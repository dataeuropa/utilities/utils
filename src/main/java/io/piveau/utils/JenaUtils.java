package io.piveau.utils;

import io.piveau.rdf.Piveau;
import io.piveau.rdf.TripleHash;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.*;
import org.apache.jena.sparql.util.Context;
import org.apache.jena.vocabulary.DCTerms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Map;

/**
 * Collection of functions around the apache jena API
 */
public class JenaUtils {
    private static final Logger log = LoggerFactory.getLogger(JenaUtils.class);

    private static final BigInteger M = new BigInteger("ffffffffffffffffffffffffffffffff", 16);

    private JenaUtils() {
    }

    public static Model read(InputStream inputStream, Lang lang, String baseUri) {
        Model model = ModelFactory.createDefaultModel();
        RDFParserBuilder builder = RDFParser.create()
                .checking(false)
                .source(inputStream)
                .lang(lang);
        if (baseUri != null) {
            builder.base(baseUri);
        }
        builder.parse(model);
        return model;
    }

    public static Model read(InputStream inputStream, String contentType) {
        return read(inputStream, mimeTypeToLang(contentType), null);
    }

    public static Model read(InputStream inputStream, String contentType, String baseUri) {
        return read(inputStream, mimeTypeToLang(contentType), baseUri);
    }

    public static Model read(byte[] content, Lang lang, String baseUri) {
        return Piveau.toModel(content, lang, baseUri);
    }

    public static Model read(byte[] content, String contentType) {
        return Piveau.toModel(content, contentType);
    }

    public static Model read(byte[] content, String contentType, String baseUri) {
        return Piveau.toModel(content, contentType, baseUri);
    }

    public static Dataset readDataset(byte[] content, String baseUri) {
        return Piveau.toDataset(content, Lang.TRIG, baseUri);
    }

    public static Dataset readDataset(InputStream inputStream, String baseUri) {
        Dataset dataset = DatasetFactory.create();
        RDFParserBuilder builder = RDFParser.create()
                .checking(false)
                .source(inputStream)
                .lang(Lang.TRIG);
        if (baseUri != null) {
            builder.base(baseUri);
        }
        builder.parse(dataset);
        return dataset;
    }

    public static String write(Dataset dataset, Lang lang) {
        return Piveau.presentAs(dataset, lang);
    }

    public static String write(Dataset dataset, RDFFormat format) {
        return Piveau.presentAs(dataset, format);
    }

    public static String write(Dataset dataset, String contentType) {
        return Piveau.presentAs(dataset, contentType);
    }

    public static String write(Model model, Lang lang) {
        return Piveau.presentAs(model, lang);
    }

    public static String write(Model model, RDFFormat format) {
        return Piveau.presentAs(model, format);
    }

    public static String write(Model model, String contentType) {
        return Piveau.presentAs(model, contentType);
    }

    public static String write(Dataset dataset, Lang lang, boolean prependXmlDeclaration) {
        return Piveau.presentAs(dataset, lang);
    }

    public static String write(Dataset dataset, RDFFormat format, boolean prependXmlDeclaration) {
        return Piveau.presentAs(dataset, format);
    }

    public static String write(Dataset dataset, String contentType, boolean prependXmlDeclaration) {
        return Piveau.presentAs(dataset, contentType);
    }

    public static String write(Model model, Lang lang, boolean prependXmlDeclaration) {
        return Piveau.presentAs(model, lang);
    }

    public static String write(Model model, RDFFormat format, boolean prependXmlDeclaration) {
        return Piveau.presentAs(model, format);
    }

    public static String write(RDFWriterBuilder writer, boolean prependXmlDeclaration) {
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            Context cxt = new Context();
            cxt.set(SysRIOT.sysRdfWriterProperties, Map.of("showXmlDeclaration", prependXmlDeclaration));
            writer.context(cxt).output(stream);
            return stream.toString();
        } catch (IOException e) {
            log.error("Failed to write model");
            return null;
        }
    }

    public static String write(Model model, String contentType, boolean prependXmlDeclaration) {
        return Piveau.presentAs(model, contentType);
    }

    public static String findIdentifier(Resource resource) {
        return findIdentifier(resource, false, false);
    }

    /*
     * Returns identifier from resource
     *
     * Based on the uriRef or the dct:identifier of the resource an identifier is returned
     */
    public static String findIdentifier(Resource resource, boolean removePrefix, boolean uriRefPrecedence) {
        if (uriRefPrecedence) {
            return identifierPrefix(resource.isURIResource() ? resource.getURI() : getIdentifierProperty(resource), removePrefix);
        } else {
            String identifier = getIdentifierProperty(resource);
            if (identifier == null && resource.isURIResource()) {
                identifier = resource.getURI();
            }
            return identifierPrefix(identifier, removePrefix);
        }

    }

    /*
     * Remove prefix of identifier
     *
     * If removePrefix is true and identifier is not null it tries to extract the part after the last slash.
     * If no slash is find or removePrefix is false, identifier is returned.
     */
    private static String identifierPrefix(String identifier, boolean removePrefix) {
        if (removePrefix && identifier != null) {
            int idx = identifier.lastIndexOf('/');
            return idx != -1 ? identifier.substring(idx + 1) : identifier;
        } else {
            return identifier;
        }
    }
    /*
     * Returns property dct:identifier of resource or null
     *
     * If dct:identifier is a literal the lexical form is returned, if it is a resource the uriRef is returned.
     * In case it is a blank node, null is returned.
     */
    private static String getIdentifierProperty(Resource resource) {
        Statement statement = resource.getProperty(DCTerms.identifier);
        if (statement != null) {
            RDFNode obj = statement.getObject();
            if (obj.isLiteral()) {
                return obj.asLiteral().getLexicalForm();
            } else if (obj.isURIResource()) {
                return obj.asResource().getURI();
            }
        }
        return null;
    }

    public static Lang mimeTypeToLang(String dataMimeType) {
        return Piveau.asRdfLang(dataMimeType);
    }

    public static RDFFormat mimeTypeToFormat(String dataMimeType) {
        return Piveau.asRdfFormat(dataMimeType);
    }

    public static String normalize(String id) {
        return Piveau.asNormalized(id);
    }

    public static String normalizeUnicode(String id) {
        return Piveau.asUnicodeNormalized(id);
    }

    public static String canonicalHash(Model model) {
        if (model == null) {
            throw new IllegalArgumentException("Model must not be null");
        }

        BigInteger totalHash = BigInteger.ZERO;
        StmtIterator it = model.listStatements();
        while (it.hasNext()) {
            Statement stm = it.nextStatement();

            BigInteger basicHash = new BigInteger(TripleHash.hash(stm.asTriple()), 16);

            if (stm.getSubject().isAnon()) {
                StmtIterator objIt = model.listStatements(null, null, stm.getSubject());
                while (objIt.hasNext()) {
                    basicHash = combineHashes(basicHash, TripleHash.hash(objIt.nextStatement().asTriple()));
                }
            }

            if (stm.getObject().isAnon()) {
                StmtIterator subjIt = stm.getObject().asResource().listProperties();
                while (subjIt.hasNext()) {
                    basicHash = combineHashes(basicHash, TripleHash.hash(subjIt.nextStatement().asTriple()));
                }
            }

            totalHash = totalHash.add(basicHash);
            totalHash = totalHash.mod(M);
        }

        return totalHash.toString(16);
    }

    private static BigInteger combineHashes(BigInteger base, String hash) {
        BigInteger sum = base.add(new BigInteger(hash, 16));
        return sum.mod(M);
    }

}
