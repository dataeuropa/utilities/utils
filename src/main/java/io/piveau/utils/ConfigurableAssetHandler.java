package io.piveau.utils;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurableAssetHandler implements Handler<RoutingContext> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final String path;
    private final WebClient client;

    public ConfigurableAssetHandler(String path, WebClient client) {
        this.path = path;
        this.client = client;
    }

    @Override
    public void handle(RoutingContext routingContext) {
        if (path.startsWith("http://") || path.startsWith("https://")) {
            client.getAbs(path).send()
                    .onSuccess(response -> routingContext.response().end(response.bodyAsBuffer()))
                    .onFailure(cause -> {
                        log.error("Serving asset from {}", path, cause);
                        routingContext.fail(cause);
                    });
        } else {
            routingContext.response().sendFile(path)
                    .onFailure(cause -> {
                        log.error("Serving asset from {}", path, cause);
                    });
        }
    }

}