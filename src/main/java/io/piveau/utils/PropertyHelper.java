package io.piveau.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public final class PropertyHelper {
    public static Map<String, List> mandatoryClassProperties = Map.of("Catalogue", List.of("Dataset", "Title", "Description", "Publisher"),
            "Dataset", List.of("Title", "Description"),
            "Distribution", List.of("AccessURL"),
            "Service", List.of("EndPointURL", "Title"));

    public static boolean isURL(String url) {
        try {
            new URL(url);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    public static String cleanPropertyName(String propertyName) {
        return propertyName
                .replace("Adms", "")
                .replace("Dct", "")
                .replace("Dcat", "")
                .replace("Foaf", "")
                .replace("Odrl", "")
                .replace("Owl", "")
                .replace("Prov", "")
                .replace("Spdx", "");
    }
}
