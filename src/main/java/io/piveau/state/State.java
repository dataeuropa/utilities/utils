package io.piveau.state;

import com.fasterxml.uuid.Generators;
import io.piveau.state.sub.AccessControlState;
import io.piveau.state.sub.SubState;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;

import java.util.*;


public class State {

    public static String CONTEXT_KEY = "state";

    private Map<String, SubState> children;
    private Map<String, Object> data;
    private String userID = null;
    private final UUID id = Generators.timeBasedEpochGenerator().generate();
    private User user;

    private final AccessControlState accessControlState;
    private RoutingContext routingContext = null;

    public UUID getID() {
        return id;
    }

    public RoutingContext getRoutingContext() {
        return routingContext;
    }

    public String getUserID() {
        return userID;
    }

    public static State fromRoutingContext(RoutingContext ctx) {
        if (ctx.get(CONTEXT_KEY) != null) {
            return ctx.get(CONTEXT_KEY);
        } else {
            return null;
        }
    }

    public State(String userID) {
        this();
        this.userID = userID;

    }

    public State() {
        accessControlState = new AccessControlState();
        if (getData().get("permissions") != null && getData().get("permissions") instanceof JsonArray) {
            accessControlState.setPermissions((JsonArray) getData().get("permissions"));
        }

        children = getChildren();
    }


    public State(RoutingContext ctx) {
        this();
        if (ctx.user() != null) {
            setUser(ctx.user());
        }
        getData().putAll(ctx.data());
        getData().put("routingContext", ctx);
        routingContext = ctx;
    }


    /* Functionality for handling user */
    public User user() {
        return user;
    }

    public boolean hasUser() {
        return user != null;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void clearUser() {
        this.user = null;
    }


    /*Functionality for handling substates*/

    public Map<String, SubState> children() {
        return getChildren();
    }


    private Map<String, SubState> getChildren() {
        if (children == null) {
            children = new HashMap<>();
            //TODO: does it make sense to create them here? 🤔
            addChild("accessControlState", accessControlState);

        }
        return children;
    }

    public SubState getChild(String name) {
        if (children == null) {
            return null;
        } else {
            return getChildren().get(name);
        }

    }

    public SubState getChild(Class<? extends SubState> impl) {
        if (children == null) {
            return null;
        } else {
            return getChildren().values().stream().filter(impl::isInstance).findFirst().orElse(null);
        }
    }

    public State addChild(String name, SubState child) {
        getChildren().put(name, child);
        child.setParent(this);
        return this;
    }


    /*functionality for handling other context data*/

    private Map<String, Object> getData() {
        if (data == null) {
            data = new HashMap<>();
        }
        return data;
    }


    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        if (data == null) {
            return null;
        } else {
            return (T) getData().get(key);
        }
    }


    @SuppressWarnings("unchecked")
    public <T> T get(String key, T defaultValue) {
        if (data == null) {
            return defaultValue;
        } else {
            Map<String, ?> data = getData();
            if (data.containsKey(key)) {
                return (T) data.get(key);
            } else {
                return defaultValue;
            }
        }
    }


    @SuppressWarnings("unchecked")
    public <T> T remove(String key) {
        if (data == null) {
            return null;
        } else {
            return (T) getData().remove(key);
        }
    }


    public Map<String, Object> data() {
        return getData();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        State state = (State) o;
        return children.equals(state.children) && Objects.equals(userID, state.userID);
    }

    @Override
    public int hashCode() {
        int result = children.hashCode();
        result = 31 * result + Objects.hashCode(userID);
        return result;
    }

    public AccessControlState getAccessControlState() {
        return accessControlState;
    }
}
