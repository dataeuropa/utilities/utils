package io.piveau.state;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import org.apache.commons.lang3.NotImplementedException;

public class StateMessageCodec implements MessageCodec<State, State> {
    @Override
    public void encodeToWire(Buffer buffer, State state) {
        throw new NotImplementedException("not needed for local messaging");
    }

    @Override
    public State decodeFromWire(int pos, Buffer buffer) {
        throw new NotImplementedException("not needed for local messaging");
    }

    @Override
    public State transform(State state) {
        // If a message is sent *locally* across the event bus.
        // This method is called to transform the message from the sent type S to the received type R.
        //Since S and R are the same type in this case, we just return it.
        return state;
    }

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public byte systemCodecID() {
        //Used to identify system codecs. Should always return -1 for a user codec.
        return -1;
    }
}
