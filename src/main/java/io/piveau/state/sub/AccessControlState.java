package io.piveau.state.sub;

import io.piveau.state.State;
import io.vertx.core.json.JsonArray;

import java.util.*;

public class AccessControlState implements SubState {

    private State parent;

    private String userId;
    private JsonArray permissions;


    private String token;

    public AccessControlState() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public JsonArray getPermissions() {
        return Objects.requireNonNullElseGet(permissions, JsonArray::new);
    }

    public void setPermissions(JsonArray permissions) {
        this.permissions = permissions;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public List<String> getDatasetIds() {
        return datasetIds;
    }

    public void setDatasetIds(List<String> datasetIds) {
        this.datasetIds = datasetIds;
    }

    public List<String> getCatalogIds() {
        return catalogIds;
    }

    public void setCatalogIds(List<String> catalogIds) {
        this.catalogIds = catalogIds;
    }

    private List<String> datasetIds = new ArrayList<>();
    private List<String> catalogIds = new ArrayList<>();


    @Override
    public State getParent() {
        return parent;
    }

    @Override
    public SubState setParent(State parent) {
        this.parent = parent;
        return this;
    }
}
