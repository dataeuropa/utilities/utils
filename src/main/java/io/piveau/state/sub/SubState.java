package io.piveau.state.sub;

import io.piveau.state.State;

public interface SubState {

    State getParent();

    SubState setParent(State parent);


}
