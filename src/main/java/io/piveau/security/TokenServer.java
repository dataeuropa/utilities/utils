package io.piveau.security;

import io.vertx.ext.auth.User;

public abstract class TokenServer {

    protected TokenServerConfig config;

    protected TokenServer(TokenServerConfig config) {
        this.config = config;
    }

    abstract protected String getTokenEndpoint();
    abstract protected String getCertsEndpoint();

    abstract protected String getIssuer();

    abstract protected boolean userHasRole(User user, String role);
    abstract protected boolean userHasPermission(User user, String resource, String scope);

}
