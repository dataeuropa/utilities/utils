package io.piveau.security;

import io.piveau.state.State;
import io.piveau.state.sub.AccessControlState;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class PermissionManager {


    private static final Logger log = LoggerFactory.getLogger(PermissionManager.class);

    public PermissionManager(State state) {
        this.permissionHierarchy = new HashMap<>();
        this.state = state;
    }

    public record Dataset(String datasetid, String catalogid, String organisationid) {
    }

    public record Catalog(String catalogid, String organisationid) {
    }

    public record Organisation(String organisationid) {
    }


    public enum PERMISSION {


        DATASET_VIEW_PUBLISHED("dataset:view_published"),
        DATASET_VIEW_DRAFT("dataset:view_draft"),
        DATASET_UPDATE("dataset:update"),
        DATASET_CREATE("dataset:create"),
        DATASET_DELETE("dataset:delete"),
        DATASET_PUBLISH("dataset:publish"),
        DATASET_UNPUBLISH("dataset:unpublish"),

        CATALOG_VIEW("catalog:view"),
        CATALOG_UPDATE("catalog:update"),
        CATALOG_CREATE("catalog:create"),
        CATALOG_DELETE("catalog:delete"),


        CATALOG_DATASETS_VIEW("catalog_datasets:view"),
        CATALOG_DATASETS_VIEW_PUBLISHED("catalog_datasets:view_published"),
        CATALOG_DATASETS_VIEW_DRAFT("catalog_datasets:view_draft"),
        CATALOG_DATASETS_UPDATE("catalog_datasets:update"),
        CATALOG_DATASETS_CREATE("catalog_datasets:create"),
        CATALOG_DATASETS_DELETE("catalog_datasets:delete");


        private final String label;

        PERMISSION(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        public static PERMISSION fromString(String text) {
            for (PERMISSION b : PERMISSION.values()) {
                if (b.label.equalsIgnoreCase(text)) {
                    return b;
                }
            }
            return null;
        }


        public boolean equalsIgnoreCase(String other) {
            return label.equalsIgnoreCase(other);
        }

        @Override
        public String toString() {
            return label;
        }
    }

    public enum ACCESS_LEVEL {
        PUBLIC,
        RESTRICTED;


        public static ACCESS_LEVEL fromUriString(String accessString) {
            return switch (accessString) {
                case "https://piveau.eu/ns/voc#public" -> PUBLIC;
                case "https://piveau.eu/ns/voc#restricted" -> RESTRICTED;
                default -> throw new IllegalArgumentException("Unknown access level: " + accessString);
            };
        }

        public static ACCESS_LEVEL fromResource(Resource accessString) {
            return fromUriString(accessString.getURI());
        }

    }


    private final Map<PERMISSION, List<Record>> permissionHierarchy;
    private Boolean permissionsComputed = false;
    private final State state;


    public List<PERMISSION> getPermissionsForDataset(String datasetid) {
        if (state == null) return null;
        AccessControlState acs = (AccessControlState) state.getChild(AccessControlState.class);
        if (acs == null) return null;
        //todo: calculate permission from access control state
        List<PERMISSION> permissions = new ArrayList<>();
        calculatePermissions(state).forEach((p, l) -> l.stream()
                .filter(Dataset.class::isInstance)
                .map(Dataset.class::cast)
                .forEach(d -> {
                    if (d.datasetid().equals(datasetid)) {
                        permissions.add(p);
                    }
                }));
        return permissions;
    }

    public List<PERMISSION> getPermissionsForCatalog(String catalogid) {
        if (state == null) return null;
        AccessControlState acs = (AccessControlState) state.getChild(AccessControlState.class);
        if (acs == null) return null;
        List<PERMISSION> permissions = new ArrayList<>();
        calculatePermissions(state).forEach((p, l) -> l.stream()
                .filter(Catalog.class::isInstance)
                .map(Catalog.class::cast)
                .forEach(c -> {
                    if (c.catalogid().equals(catalogid)) {
                        permissions.add(p);
                    }
                }));
        return permissions;
    }


    public boolean hasPermissionForDataset(String datasetid, PERMISSION permission) {
        return getPermissionsForDataset(datasetid).contains(permission);
    }

    public boolean hasPermissionForCatalog(String catalogid, PERMISSION permission) {
        return getPermissionsForCatalog(catalogid).contains(permission);
    }



    public boolean hasAllPermissionsForCatalog(String catalogid, List<PERMISSION> permissions) {

        if (state.user() == null) {
            return false;

        } else if (state.user().principal().containsKey("resources")) { //API KEY auth: if user uses an API key that allows access to all (*) or a specific catalogue
            List<String> resources = state.user().principal().getJsonArray("resources").stream()
                    .map(Object::toString).toList();
            return resources.contains("*") || resources.contains(catalogid);
            // else keycloak auth: if user has the role operator or the permissions on that catalogue
        } else return PiveauAuth.userHasRole(state.user(), "operator")
                || new HashSet<>(getPermissionsForCatalog(catalogid)).containsAll(permissions);

    }


    /**
     * Returns a list of restricted catalogs the current user can view
     * @return
     */
    public List<Catalog> getAllowedCatalogs() {
        List<Catalog> catalogs = new ArrayList<>();
        calculatePermissions(state).forEach((permission, records) -> {
            if (permission.equals(PERMISSION.DATASET_VIEW_PUBLISHED)) {
                records.forEach(record -> {
                    if (record instanceof Catalog) {
                        catalogs.add((Catalog) record);
                    }
                });
            }
        });
        return catalogs;
    }

    public Map<PERMISSION, List<Record>> calculatePermissions(State state) {

        if (permissionsComputed) {
            return permissionHierarchy;
        }
        if (state == null) return null;
        AccessControlState acs = (AccessControlState) state.getChild(AccessControlState.class);
        if (acs == null) return null;

        if (acs.getPermissions().isEmpty() && (state.getRoutingContext() != null && state.getRoutingContext().get("permissions") != null && state.getRoutingContext().get("permissions") instanceof JsonArray)) {
            acs.setPermissions(state.getRoutingContext().get("permissions"));
        }

        //Permission object looks like
/*        [ {
            "scopes": [
            "dataset:update",
                    "dataset:delete",
                    "dataset:view_draft",
                    "dataset:create"
        ],
            "rsid": "877b8b61-ed97-428f-b1b2-227a7607c2d0",
                    "rsname": "organization1/catalog2"
        }, {
            "scopes": [
            "dataset:update",
                    "dataset:delete",
                    "dataset:view_draft",
                    "dataset:create"
        ],
            "rsid": "783ab7fe-7a8f-4d9b-9cc1-71c40bc36ba2",
                    "rsname": "organization2"
        }, {
            "scopes": [
            "dataset:update",
                    "dataset:delete",
                    "dataset:view_draft",
                    "dataset:create"
        ],
            "rsid": "deed6656-f227-414e-bd17-69c56f144b33",
                    "rsname": "organization2/catalog1"
        }, {
            "scopes": [
            "dataset:view_published"
        ],
            "rsid": "e93589eb-5784-4739-ab1d-0b19497c3b41",
                    "rsname": "organization1/catalog1/dataset2"
        }, {
            "scopes": [
            "dataset:view_published"
        ],
            "rsid": "49139243-4ac2-456d-ad52-108c65f4debb",
                    "rsname": "organization1/catalog1/dataset3"
        }
]*/


        acs.getPermissions().stream()
                .filter(JsonObject.class::isInstance)
                .map(JsonObject.class::cast)
                .forEach(permission -> {

                    String resource = permission.getString("rsname", "");
                    //count number of / in resource
                    int count = (int) resource.chars().filter(c -> c == '/').count();
                    Record rs = switch (count) {
                        case 0 -> new Organisation(resource);
                        case 1 -> {
                            String[] parts = resource.split("/");
                            yield new Catalog(parts[1], parts[0]);
                        }
                        case 2 -> {
                            String[] parts2 = resource.split("/");
                            yield new Dataset(parts2[2], parts2[1], parts2[0]);
                        }
                        default -> {
                            log.warn("Invalid resource name with {} `/`: {}", count, resource);
                            yield null;
                        }
                    };

                    if (rs != null) {
                        JsonArray scopes = permission.getJsonArray("scopes", new JsonArray());
                        for (Object scope : scopes) {
                            if (PERMISSION.fromString(scope.toString()) != null) {
                                permissionHierarchy.computeIfAbsent(PERMISSION.fromString(scope.toString()), k -> new ArrayList<>()).add(rs);
                            }
                        }
                    }

                });

        permissionsComputed = true;
        return permissionHierarchy;
    }

}
