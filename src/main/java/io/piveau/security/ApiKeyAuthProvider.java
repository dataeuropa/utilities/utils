package io.piveau.security;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.authentication.AuthenticationProvider;
import io.vertx.ext.auth.authentication.Credentials;
import io.vertx.ext.auth.authentication.TokenCredentials;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApiKeyAuthProvider implements AuthenticationProvider {

    private final Map<String, List<String>> apiKeys = new HashMap<>();

    public ApiKeyAuthProvider(String apiKey) {
        apiKeys.put(apiKey, Collections.singletonList("*"));
    }

    public ApiKeyAuthProvider(Map<String, List<String>> apiKeys) {
        this.apiKeys.putAll(apiKeys);
    }

    @Override
    public void authenticate(JsonObject jsonObject, Handler<AsyncResult<User>> handler) {
        authenticate(new TokenCredentials(jsonObject), handler);
    }

    @Override
    public void authenticate(Credentials credentials, Handler<AsyncResult<User>> resultHandler) {
        TokenCredentials apiKeyCredentials = (TokenCredentials) credentials;
        String token = apiKeyCredentials.getToken();
        if (apiKeys.containsKey(token)) {
            List<String> resources = apiKeys.get(token);
            resultHandler.handle(
                    Future.succeededFuture(
                            User.create(apiKeyCredentials.toJson().put("resources", new JsonArray(resources)))));
        } else {
            resultHandler.handle(Future.failedFuture("Invalid API key"));
        }
    }

}
