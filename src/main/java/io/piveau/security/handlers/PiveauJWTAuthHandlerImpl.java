package io.piveau.security.handlers;

import io.piveau.security.*;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.authentication.TokenCredentials;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.HttpException;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.ext.web.handler.impl.HTTPAuthorizationHandler;
import io.vertx.ext.web.handler.impl.ScopedAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PiveauJWTAuthHandlerImpl extends HTTPAuthorizationHandler<JWTAuth> implements JWTAuthHandler, ScopedAuthentication<JWTAuthHandler> {

    private static final Logger log = LoggerFactory.getLogger(PiveauJWTAuthHandlerImpl.class);
    private final List<String> scopes;
    private final String delimiter;
    private KeycloakResourceHelper keycloakResourceHelper;

    public PiveauJWTAuthHandlerImpl(JWTAuth authProvider, String realm) {
        super(authProvider, Type.BEARER, realm);
        scopes = new ArrayList<>();
        this.delimiter = " ";
    }

    public PiveauJWTAuthHandlerImpl(JWTAuth authProvider, String realm, PiveauAuthConfig piveauAuthConfig, WebClient webClient) {
        super(authProvider, Type.BEARER, realm);
        scopes = new ArrayList<>();
        this.delimiter = " ";

        if (piveauAuthConfig.getTokenServerConfig() instanceof KeycloakTokenServerConfig keycloakTokenServerConfig) {
            this.keycloakResourceHelper = new KeycloakResourceHelper(webClient, keycloakTokenServerConfig);
        }

    }

    private PiveauJWTAuthHandlerImpl(PiveauJWTAuthHandlerImpl base, List<String> scopes, String delimiter) {
        super(base.authProvider, Type.BEARER, base.realm);
        this.scopes = scopes;
        this.delimiter = delimiter;
    }

    @Override
    public void authenticate(RoutingContext context, Handler<AsyncResult<User>> handler) {

        parseAuthorization(context, true, parseAuthorization -> {
            if (parseAuthorization.failed()) {
                handler.handle(Future.failedFuture(parseAuthorization.cause()));
                return;
            }

            String token = parseAuthorization.result();
            if (token == null) {
                handler.handle(Future.succeededFuture());
                return;
            }
            int segments = 0;
            for (int i = 0; i < token.length(); i++) {
                char c = token.charAt(i);
                if (c == '.') {
                    if (++segments == 3) {
                        handler.handle(Future.failedFuture(new HttpException(400, "Too many segments in token")));
                        return;
                    }
                    continue;
                }
                if (Character.isLetterOrDigit(c) || c == '-' || c == '_') {
                    continue;
                }
                // invalid character
                handler.handle(Future.failedFuture(new HttpException(400, "Invalid character in token: " + (int) c)));
                return;
            }

            authProvider.authenticate(new TokenCredentials(token), authn -> {
                if (authn.failed()) {
                    handler.handle(Future.failedFuture(new HttpException(401, authn.cause())));
                } else {
                    handler.handle(authn);
                }
            });
        });
    }

    @Override
    public JWTAuthHandler withScope(String scope) {
        List<String> updatedScopes = new ArrayList<>(this.scopes);
        updatedScopes.add(scope);
        return new PiveauJWTAuthHandlerImpl(this, updatedScopes, delimiter);
    }

    @Override
    public JWTAuthHandler withScopes(List<String> scopes) {
        return new PiveauJWTAuthHandlerImpl(this, scopes, delimiter);
    }

    @Override
    public JWTAuthHandler scopeDelimiter(String delimeter) {
        return new PiveauJWTAuthHandlerImpl(this, scopes, delimeter);
    }

    /**
     * The default behavior for post-authentication
     */
    @Override
    public void postAuthentication(RoutingContext ctx) {
        final User user = ctx.user();
        if (user == null) {
            // bad state
            ctx.next();
            return;
        }
        // the user is authenticated, however the user may not have all the required scopes
        if (!scopes.isEmpty()) {
            final JsonObject jwt = user.get("accessToken");
            if (jwt == null) {
                ctx.fail(403, new VertxException("Invalid JWT: null", true));
                return;
            }

            if (jwt.getValue("scope") == null) {
                ctx.fail(403, new VertxException("Invalid JWT: scope claim is required", true));
                return;
            }

            List<?> target;
            if (jwt.getValue("scope") instanceof String) {
                target =
                        Stream.of(jwt.getString("scope")
                                        .split(delimiter))
                                .collect(Collectors.toList());
            } else {
                target = jwt.getJsonArray("scope").getList();
            }

            if (target != null) {
                for (String scope : scopes) {
                    if (!target.contains(scope)) {
                        ctx.fail(403, new VertxException("JWT scopes != handler scopes", true));
                        return;
                    }
                }
            }
        }
        String jwt = user.get("access_token");
        if (keycloakResourceHelper != null) {
            keycloakResourceHelper.getAllPermissions(jwt).onSuccess(permissions -> {

                ctx.put("permissions", permissions);
                ctx.next();

            }).onFailure(f -> {
                log.error("Could not get permissions: ", f);
                ctx.response().setStatusCode(500).end("Could not retrieve permissions. Is your Access Token valid?");
            });
        } else {
            ctx.next();
        }

    }
}