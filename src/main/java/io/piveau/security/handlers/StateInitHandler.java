package io.piveau.security.handlers;

import io.piveau.state.State;
import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

public class StateInitHandler implements Handler<RoutingContext> {
    @Override
    public void handle(RoutingContext context) {
        State state = new State(context);
        context.put("state", state);
        context.next();
    }
}
