/*
 * Copyright (c) European Commission
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 *
 */

/*
 * Copyright (c) European Commission
 * Copyright (c) byte - Bayerische Agentur für Digitales GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.security;


import io.vertx.core.*;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpResponseExpectation;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

import java.util.ArrayList;
import java.util.List;

public class KeycloakResourceHelper {


    private final WebClient client;


    private final String groupEndpoint;
    private final String resourceEndpoint;
    private final String umaPolicyEndpoint;
    private final String tokenEndpoint;
    private final String clientsEndpoint;

    public KeycloakResourceHelper(WebClient client, KeycloakTokenServerConfig config) {
        this.client = client;
        groupEndpoint = config.getTokenServerUrl() + "/admin/realms/" + config.getRealm() + "/groups";
        resourceEndpoint = config.getTokenServerUrl() + "/realms/" + config.getRealm() + "/authz/protection/resource_set";
        umaPolicyEndpoint = config.getTokenServerUrl() + "/realms/" + config.getRealm() + "/authz/protection/uma-policy";
        tokenEndpoint = config.getTokenServerUrl() + "/realms/" + config.getRealm() + "/protocol/openid-connect/token";
        clientsEndpoint = config.getTokenServerUrl() + "/admin/realms/" + config.getRealm() + "/clients";
    }

    // Permissions
    public Future<JsonArray> getAllPermissions(String token) {
        MultiMap form = MultiMap.caseInsensitiveMultiMap();
        form.set("grant_type", "urn:ietf:params:oauth:grant-type:uma-ticket");
        form.set("response_mode", "permissions");
        form.set("audience", "piveau-hub-repo");

        return client.postAbs(tokenEndpoint)
                .bearerTokenAuthentication(token)
                .sendForm(form)
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(response -> response.body().toJsonArray());
    }

    // Group
    private JsonObject prepareGroupPayload(String groupName) {
        return new JsonObject()
                .put("name", groupName)
                .put("path", "/" + groupName)
                .put("subGroups", new JsonArray());
    }

    public Future<JsonArray> getGroups(String token) {
        return client.getAbs(groupEndpoint)
                .bearerTokenAuthentication(token)
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(response -> response.body().toJsonArray());
    }

    public Future<Boolean> existsGroup(String token, String groupName) {
        return getGroups(token)
                .map(array -> array.stream()
                        .anyMatch(e -> ((JsonObject) e).getString("name", "").equals(groupName)));
    }

    public Future<Void> createGroup(String token, String groupName) {
        JsonObject payload = prepareGroupPayload(groupName);
        return client.postAbs(groupEndpoint)
                .bearerTokenAuthentication(token)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json")
                .sendJson(payload)
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .mapEmpty();
    }

    private Future<Void> deleteGroupId(String token, String groupId) {
        return client.deleteAbs(groupEndpoint + "/" + groupId)
                .bearerTokenAuthentication(token)
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .mapEmpty();
    }

    public Future<Void> deleteGroup(String token, String groupName) {
        return getGroups(token)
                .map(array -> array.stream()
                        .map(e -> (JsonObject) e)
                        .filter(obj -> obj.getString("name", "").equals(groupName))
                        .findFirst()
                        .map(obj -> obj.getString("id", ""))
                        .orElseThrow())
                .compose(id -> deleteGroupId(token, id))
                .mapEmpty();
    }

    public Future<Boolean> createGroupIfNotExists(String token, String groupName) {
        return existsGroup(token, groupName)
                .compose(exists -> {
                    if (!exists) {
                        return createGroup(token, groupName).map(true);
                    } else {
                        return Future.succeededFuture(false);
                    }
                });
    }

    // Resource
    private JsonObject prepareResourcePayload(String resourceName, String resourceType, String displayName,
                                              String clientId, boolean ownerManagedAccess, JsonArray scopes) {
        return new JsonObject()
                .put("name", resourceName)
                .put("displayName", displayName)
                .put("type", "urn:" + clientId + ":resources:" + resourceType)
                .put("uris", new JsonArray().add("/" + resourceType + "/" + resourceName))
                .put("ownerManagedAccess", ownerManagedAccess ? "true" : "false")
                .put("resource_scopes", scopes)
                .put("scopes", scopes);
    }

    public Future<JsonArray> getResource(String token, String resourceName) {
        return client.getAbs(resourceEndpoint + "?name=" + resourceName)
                .bearerTokenAuthentication(token)
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(response -> response.body().toJsonArray());
    }

    public Future<JsonArray> getResources(String token) {
        return client.getAbs(resourceEndpoint)
                .bearerTokenAuthentication(token)
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(response -> response.body().toJsonArray());
    }

    public Future<Void> createResource(String token, String resourceName, String resourceType, String displayName,
                                       String clientId, boolean ownerManagedAccess, JsonArray scopes) {
        JsonObject payload = prepareResourcePayload(resourceName, resourceType, displayName, clientId, ownerManagedAccess, scopes);
        return client.postAbs(resourceEndpoint)
                .bearerTokenAuthentication(token)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json")
                .sendJson(payload)
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .mapEmpty();
    }

    public Future<Boolean> createResourceIfNotExist(String token, String resourceName, String resourceType, String displayName, String clientId, boolean ownerManagedAccess, JsonArray scopes) {
        Promise<Boolean> createResourcePromise = Promise.promise();
        getResource(token, resourceName).onSuccess(resources -> {
            if (resources.isEmpty()) {
                createResource(token, resourceName, resourceType, displayName, clientId, ownerManagedAccess, scopes)
                        .onSuccess(result -> createResourcePromise.complete(true))
                        .onFailure(cause -> createResourcePromise.fail(cause.getMessage()));
            } else {
                createResourcePromise.complete(false);
            }
        }).onFailure(cause -> createResourcePromise.fail(cause.getMessage()));
        return createResourcePromise.future();
    }

    private Future<Void> deleteResourceId(String token, String resourceId) {
        return client.deleteAbs(resourceEndpoint + "/" + resourceId)
                .bearerTokenAuthentication(token)
                .send()
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .mapEmpty();
    }

    public Future<Void> deleteResource(String token, String resourceName) {
        Promise<Void> deleteResourcePromise = Promise.promise();
        getResource(token, resourceName).onSuccess(resources -> {
            List<Future<Void>> futureList = new ArrayList<>();
            resources.forEach(value -> futureList.add(deleteResourceId(token, (String) value)));
            Future.all(futureList)
                    .onSuccess(futureListResult -> deleteResourcePromise.complete())
                    .onFailure(cause -> deleteResourcePromise.fail(cause.getMessage()));

        }).onFailure(cause -> deleteResourcePromise.fail(cause.getMessage()));
        return deleteResourcePromise.future();
    }

    // Uma Policy
    private JsonObject prepareUmaGroupPolicyPayload(String groupName, JsonArray scopes) {
        return new JsonObject()
                .put("name", groupName)
                .put("scopes", scopes)
                .put("groups", new JsonArray().add(groupName));
    }

    private Future<Void> createGroupPolicyId(String token, String resourceId, JsonObject payload) {
        return client.postAbs(umaPolicyEndpoint + "/" + resourceId)
                .bearerTokenAuthentication(token)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json")
                .sendJson(payload)
                .expecting(HttpResponseExpectation.SC_SUCCESS)
                .mapEmpty();
    }

    public Future<Void> createGroupPolicy(String token, String resourceName, String groupName,
                                          JsonArray scopes) {
        JsonObject payload = prepareUmaGroupPolicyPayload(groupName, scopes);
        Promise<Void> createGroupPolicyPromise = Promise.promise();
        getResource(token, resourceName).onSuccess(resources -> {
            List<Future<Void>> futureList = new ArrayList<>();
            resources.forEach(value -> futureList.add(createGroupPolicyId(token, (String) value, payload)));
            Future.all(futureList)
                    .onSuccess(futureListResult -> createGroupPolicyPromise.complete())
                    .onFailure(cause -> createGroupPolicyPromise.fail(cause.getMessage()));

        }).onFailure(cause -> createGroupPolicyPromise.fail(cause.getMessage()));
        return createGroupPolicyPromise.future();
    }


    public Future<JsonArray> getGroupPolicy(String token, String resourceId) {
        return client.getAbs(umaPolicyEndpoint + "?name=" + resourceId)
                .bearerTokenAuthentication(token)
                .send().expecting(HttpResponseExpectation.SC_SUCCESS)
                .map(response -> response.body().toJsonArray());
    }


    public Future<Boolean> createGroupPolicyIfNotExists(String token, String resourceName, String groupName,
                                                        JsonArray scopes) {
        Promise<Boolean> createGroupPolicyIfNotExistsPromise = Promise.promise();
        getGroupPolicy(token, groupName)
                .onSuccess(groupPolicies -> {
                    if (groupPolicies.isEmpty()) {
                        createGroupPolicy(token, resourceName, groupName, scopes)
                                .onFailure(createGroupPolicyIfNotExistsPromise::fail)
                                .onSuccess(a -> createGroupPolicyIfNotExistsPromise.complete(true));
                    } else {
                        createGroupPolicyIfNotExistsPromise.complete(false);
                    }
                }).onFailure(createGroupPolicyIfNotExistsPromise::fail);

        return createGroupPolicyIfNotExistsPromise.future();
    }

    public Future<JsonObject> getHubUiClient(String token) {
        return client.getAbs(clientsEndpoint)
                .bearerTokenAuthentication(token).send()
                .expecting(HttpResponseExpectation.SC_OK)
                // filter through returned json array to find object where clientID=ui and then return the value of uuid from that jsonobject
                .map(response -> response.bodyAsJsonArray().stream().filter(obj -> obj instanceof JsonObject).filter(obj -> ((JsonObject) obj).getString("clientId", "").equals("piveau-hub-ui")).map(obj -> (JsonObject) obj).findFirst().orElse(new JsonObject()));


    }

    public Future<Integer> updateHubUiClient(String token, String uuid, JsonObject uiclient) {


        return client.putAbs(clientsEndpoint + "/" + uuid)
                .bearerTokenAuthentication(token)
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/json")
                .sendJson(uiclient)
                .map(response -> {
                    if (response.statusCode() != 204) {
                        throw new VertxException("While changing hub-ui client keycloak returned status code " + response.statusCode());
                    }
                    return response.statusCode();
                })
                ;
    }


    public Future<Integer> addAsValidRedirectUrl(String token, String url) {

        return getHubUiClient(token).compose(client -> {

            JsonArray validRedirectUris = client.getJsonArray("redirectUris", new JsonArray());
            if (validRedirectUris.contains(url)) {
                return Future.succeededFuture(-200);
            }
            validRedirectUris.add(url);

            client.put("redirectUris", validRedirectUris);
            return updateHubUiClient(token, client.getString("id"), client);
        }).map(Integer::intValue);

    }

    public Future<Integer> removeAsValidRedirectUrl(String token, String url) {

        return getHubUiClient(token).compose(client -> {

            JsonArray validRedirectUris = client.getJsonArray("redirectUris", new JsonArray());
            if (!validRedirectUris.contains(url)) {
                return Future.succeededFuture(-200);
            }
            validRedirectUris.remove(url);

            client.put("redirectUris", validRedirectUris);
            return updateHubUiClient(token, client.getString("id"), client);
        }).map(Integer::intValue);

    }

}
