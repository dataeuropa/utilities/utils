package io.piveau.security;

import io.piveau.security.handlers.PiveauJWTAuthHandlerImpl;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpResponseExpectation;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.*;
import io.vertx.ext.auth.authentication.TokenCredentials;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.AuthenticationHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;

public class PiveauAuth {

    private final WebClient client;
    private final JWTAuth jwtAuth;
    private final JWTAuthHandler jwtAuthHandler;

    private static TokenServer tokenServer;

    private final PiveauAuthConfig config;

    public static Future<PiveauAuth> create(Vertx vertx, PiveauAuthConfig config) {
        return create(vertx, config, false);
    }

    public static Future<PiveauAuth> create(Vertx vertx, PiveauAuthConfig config, Boolean extendedAuth) {
        Promise<PiveauAuth> promise = Promise.promise();
        WebClient client = WebClient.create(vertx);
        TokenServer tokenServer = config.getTokenServerConfig().getTokenServer();
        if (tokenServer instanceof KeycloakTokenServer) {
            client.getAbs(tokenServer.getCertsEndpoint())
                    .send()
                    .onSuccess(response -> {
                        JsonObject kids;
                        try {
                            kids = response.bodyAsJsonObject();
                        } catch (Exception e) {
                            promise.fail(e);
                            return;
                        }

                        if (kids.containsKey("error")) {
                            promise.fail(kids.getString("error"));
                            return;
                        } else if (response.statusCode() != 200) {
                            promise.fail(response.statusMessage());
                            return;
                        }
                        JWTAuth jwtAuth = JWTAuth.create(vertx, new JWTAuthOptions()
                                .setJWTOptions(new JWTOptions().setLeeway(5)
                                        .setIssuer(config.getIssuer().isBlank() ? tokenServer.getIssuer() : config.getIssuer())
                                        .setIgnoreExpiration(true))
                                .setJwks(kids.getJsonArray("keys").stream().map(o -> (JsonObject) o)
                                        .filter(key -> !key.getString("alg").equals("RSA-OAEP")).toList()));

                        promise.complete(new PiveauAuth(client, tokenServer, jwtAuth, config, extendedAuth));
                    })
                    .onFailure(promise::fail);
        } else {
            JWTAuthOptions jwtAuthOptions = new JWTAuthOptions();
            if (!config.getCustomSecret().isBlank()) {
                jwtAuthOptions
                        .addPubSecKey(new PubSecKeyOptions()
                                .setAlgorithm("HS256")
                                .setBuffer(config.getCustomSecret()));
            }

            promise.complete(new PiveauAuth(client, tokenServer, JWTAuth.create(vertx, jwtAuthOptions), config, extendedAuth));
        }
        return promise.future();
    }

    private PiveauAuth(WebClient client, TokenServer tokenServer, JWTAuth jwtAuth, PiveauAuthConfig config) {
        this.config = config;
        this.client = client;
        this.jwtAuth = jwtAuth;
        PiveauAuth.tokenServer = tokenServer;
        jwtAuthHandler = JWTAuthHandler.create(jwtAuth);
    }

    private PiveauAuth(WebClient client, TokenServer tokenServer, JWTAuth jwtAuth, PiveauAuthConfig config, Boolean extendedAuth) {
        this.config = config;
        this.client = client;
        this.jwtAuth = jwtAuth;
        PiveauAuth.tokenServer = tokenServer;
        if (extendedAuth) {
            jwtAuthHandler = new PiveauJWTAuthHandlerImpl(jwtAuth, null, config, client);
        } else {
            jwtAuthHandler = JWTAuthHandler.create(jwtAuth);
        }
    }

    public AuthenticationHandler authHandler() {
        return jwtAuthHandler;
    }

    public Future<User> authenticate(String token) {
        return jwtAuth.authenticate(new TokenCredentials(token));
    }

    public Future<String> requestClientToken() {
        MultiMap map = MultiMap.caseInsensitiveMultiMap();
        map.add("grant_type", "client_credentials").add("client_id", config.getClientId());
        if (!config.getClientSecret().isBlank()) {
            map.add("client_secret", config.getClientSecret());
        }

        return requestToken(map, null);
    }

    public Future<String> requestUserToken(String username, String password) {
        MultiMap map = MultiMap.caseInsensitiveMultiMap();
        map.add("grant_type", "password")
                .add("username", username)
                .add("password", password)
                .add("client_id", config.getClientId());
        if (!config.getClientSecret().isBlank()) {
            map.add("client_secret", config.getClientSecret());
        }

        return requestToken(map, null);
    }

    public Future<String> requestUmaToken(String token, String audience) {
        MultiMap map = MultiMap.caseInsensitiveMultiMap()
                .add("grant_type", "urn:ietf:params:oauth:grant-type:uma-ticket")
                .add("audience", audience);
        return requestToken(map, token);
    }

    private Future<String> requestToken(MultiMap map, String token) {
        HttpRequest<Buffer> request = client.postAbs(tokenServer.getTokenEndpoint());
        if (token != null) {
            request.bearerTokenAuthentication(token);
        }
        return request
                .sendForm(map)
                .expecting(HttpResponseExpectation.SC_OK)
                .map(response -> response.bodyAsJsonObject().getString("access_token"));
    }

    public static boolean userHasRole(User user, String role) {
        return tokenServer.userHasRole(user, role);
    }

    public static boolean userHasPermission(User user, String resource, String scope) {
        return tokenServer.userHasPermission(user, resource, scope);
    }

}
