package io.piveau.security;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;

public class KeycloakTokenServer extends TokenServer {

    protected KeycloakTokenServer(TokenServerConfig config) {
        super(config);
    }

    @Override
    protected String getTokenEndpoint() {
        return config.getTokenServerUrl() + "/realms/" + config.getRealm() + "/protocol/openid-connect/token";
    }

    @Override
    protected String getCertsEndpoint() {
        return config.getTokenServerUrl() + "/realms/" + config.getRealm() + "/protocol/openid-connect/certs";
    }

    @Override
    protected String getIssuer() {
        return config.getTokenServerUrl() + "/realms/" + config.getRealm();
    }

    @Override
    protected boolean userHasRole(User user, String role) {
        return user.principal()
                .getJsonObject("realm_access", new JsonObject())
                .getJsonArray("roles", new JsonArray())
                .contains(role);
    }

    @Override
    protected boolean userHasPermission(User user, String resource, String scope) {
        JsonArray permissions = user.principal()
                .getJsonObject("authorization", new JsonObject())
                .getJsonArray("permissions", new JsonArray());
        return permissions.stream()
                .map(o -> (JsonObject) o)
                .anyMatch(permission ->
                        permission.getString("rsname", "").equals(resource) &&
                                permission.getJsonArray("scopes", new JsonArray()).contains(scope)
                );
    }

}
