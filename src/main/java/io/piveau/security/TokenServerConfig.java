package io.piveau.security;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;

public abstract class TokenServerConfig {

    private String tokenServerUrl = "";
    private String realm = "";

    static TokenServerConfig create(JsonObject config) {
        if (config.containsKey("keycloak")) {
            return new KeycloakTokenServerConfig()
                    .setTokenServerUrl(config.getJsonObject("keycloak").getString("serverUrl", ""))
                    .setRealm(config.getJsonObject("keycloak").getString("realm", ""));
        } else {
            return new TokenServerConfig() {
                @Override
                TokenServer getTokenServer() {
                    return new TokenServer(this) {
                        @Override
                        protected String getTokenEndpoint() {
                            return null;
                        }

                        @Override
                        protected String getCertsEndpoint() {
                            return null;
                        }

                        @Override
                        protected String getIssuer() {
                            return null;
                        }

                        @Override
                        protected boolean userHasRole(User user, String role) {
                            return true;
                        }

                        @Override
                        protected boolean userHasPermission(User user, String resource, String scope) {
                            return true;
                        }
                    };
                }
            };
        }
    }

    public String getTokenServerUrl() {
        return tokenServerUrl;
    }

    public TokenServerConfig setTokenServerUrl(String tokenServerUrl) {
        this.tokenServerUrl = tokenServerUrl;
        return this;
    }

    public String getRealm() {
        return realm;
    }

    public TokenServerConfig setRealm(String realm) {
        this.realm = realm;
        return this;
    }

    abstract TokenServer getTokenServer();
}
