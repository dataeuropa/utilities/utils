package io.piveau.dcatap;

import io.piveau.dcatap.classes.Catalogue;
import io.piveau.dcatap.classes.Dataset;
import io.piveau.dcatap.classes.Distribution;
import io.piveau.dcatap.classes.Service;
import io.piveau.utils.JenaUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;

import java.util.ArrayList;


public class DCATAPGraph {
    private Model model;
    private ArrayList<Catalogue> catalogueList;
    private ArrayList<Dataset> datasetsList;
    private ArrayList<Distribution> distributionList;
    private ArrayList<Service> serviceList;

    public DCATAPGraph() {
        catalogueList = new ArrayList();
        datasetsList = new ArrayList<>();
        distributionList = new ArrayList<>();
        serviceList = new ArrayList<>();
    }

    public DCATAPGraph build() {
        model = ModelFactory.createDefaultModel();
        model.setNsPrefixes(Prefixes.DCATAP_PREFIXES);

        catalogueList.forEach(catalogue -> {
            model.add(catalogue.getModel());
        });

        datasetsList.forEach(dataset -> {
            model.add(dataset.getModel());
        });

        distributionList.forEach(distribution -> {
            model.add(distribution.getModel());
        });

        serviceList.forEach(service -> {
            model.add(service.getModel());
        });


        return this;
    }


    public Catalogue createCatalogue() {
        Catalogue catalogue = new Catalogue();
        this.catalogueList.add(catalogue);
        return catalogue;
    }

    public Catalogue createCatalogue(String uri) {
        Catalogue catalogue = new Catalogue(uri);
        this.catalogueList.add(catalogue);
        return catalogue;
    }

    public DCATAPGraph addCatalogue(Catalogue catalogue) {
        catalogueList.add(catalogue);
        return this;
    }

    public Dataset createDataset() {
        Dataset dataset = new Dataset();
        this.datasetsList.add(dataset);
        return dataset;
    }

    public Dataset createDataset(String uri) {
        Dataset dataset = new Dataset(uri);
        this.datasetsList.add(dataset);
        return dataset;
    }

    public DCATAPGraph addDataset(Dataset dataset) {
        datasetsList.add(dataset);
        return this;
    }

    public Distribution createDistribution() {
        Distribution distribution = new Distribution();
        this.distributionList.add(distribution);
        return distribution;
    }

    public Distribution createDistribution(String uri) {
        Distribution distribution = new Distribution(uri);
        this.distributionList.add(distribution);
        return distribution;
    }

    public DCATAPGraph addDistribution(Distribution distribution) {
        this.distributionList.add(distribution);
        return this;
    }

    public Service createService() {
        Service service = new Service();
        this.serviceList.add(service);
        return service;
    }

    public Service createService(String uri) {
        Service service = new Service(uri);
        this.serviceList.add(service);
        return service;
    }

    public DCATAPGraph addService(Service service) {
        serviceList.add(service);
        return this;
    }


    public String asTurtle() {
        if (model == null) this.build();
        return JenaUtils.write(model, Lang.TURTLE);
    }

    public String asNTriples() {
        if (model == null) this.build();
        return JenaUtils.write(model, Lang.NTRIPLES);
    }

    public String asJSONLD() {
        if (model == null) this.build();
        return JenaUtils.write(model, Lang.JSONLD);
    }

    public String asXML() {
        if (model == null) this.build();
        return JenaUtils.write(model, Lang.RDFXML);
    }

    public Model getModel() {
        if (this.model == null) this.build();
        return model;
    }
}
