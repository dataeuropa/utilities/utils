package io.piveau.dcatap.classes;

import io.piveau.dcatap.properties.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import java.util.UUID;

public class Service extends BaseClass {

    public Service(String uri) {
        super(uri, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#DataService"));
    }

    public Service() {
        super("http://www.piveau.eu/set/service/" + UUID.randomUUID(), ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#DataService"));
    }

    /**
     * Mandatory
     */

    public Service setEndPointURL(String endPointURL) {
        DcatEndPointURL dcatEndPointURL1 = new DcatEndPointURL(endPointURL);
        this.addMandatoryProperty(dcatEndPointURL1.getClass().getSimpleName());
        this.attachProperty(dcatEndPointURL1);
        return this;
    }

    public Service setTitle(String title) {
        DctTitle dctTitle = new DctTitle(title);
        this.addMandatoryProperty(dctTitle.getClass().getSimpleName());
        this.attachProperty(dctTitle);
        return this;
    }

    public Service setTitle(String title, String titleLanguage) {
        DctTitle dctTitle = new DctTitle(title, titleLanguage);
        this.addMandatoryProperty(dctTitle.getClass().getSimpleName());
        this.attachProperty(dctTitle);
        return this;
    }

    /**
     * Recommended
     */

    public Service setEndpointDescription(String endpointDescription) {
        DcatEndPointDescription endpointDescription1Dcat = new DcatEndPointDescription(endpointDescription);
        this.attachProperty(endpointDescription1Dcat);
        return this;
    }

    public Service setServesDataset(String servesDataset) {
        DcatServesDataset dcatServesDataset1 = new DcatServesDataset(servesDataset);
        this.attachProperty(dcatServesDataset1);
        return this;
    }

    /**
     * Optional
     */

    public Service setAccessRights(String accessRight) {
        DctAccessRights dctAccessRights = new DctAccessRights(accessRight);
        this.attachProperty(dctAccessRights);
        return this;
    }

    public Service setDescription(String description) {
        DctDescription dctDescription = new DctDescription(description);
        this.attachProperty(dctDescription);
        return this;
    }

    public Service setDescription(String description, String descriptionLanguage) {
        DctDescription dctDescription = new DctDescription(description, descriptionLanguage);
        this.attachProperty(dctDescription);
        return this;
    }

    public Service setLicense(String dctLicense) {
        DctLicense license = new DctLicense(dctLicense);
        this.attachProperty(license);
        return this;
    }


    public Model getModel() {
        return model;
    }
}
