package io.piveau.dcatap.classes;

import io.piveau.dcatap.properties.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCAT;

import java.time.Instant;
import java.util.UUID;

public class Distribution extends BaseClass {

    public Distribution(String uri) {
        super(uri, DCAT.Distribution);
    }

    public Distribution() {
        super("http://www.piveau.eu/set/distribution/" + UUID.randomUUID(), DCAT.Distribution);
    }


    /**
     * Mandatory
     */

    public Distribution setAccessURL(String accessURL) {
        DcatAccessURL dcatAccessUrl = new DcatAccessURL(accessURL);
        this.addMandatoryProperty(dcatAccessUrl.getClass().getSimpleName());
        this.attachProperty(dcatAccessUrl);
        return this;
    }

    /**
     * Recommended
     */

    public Distribution setAvailability(String availability) {
        DcatapAvailability dcatapAvailability = new DcatapAvailability(availability);
        this.attachProperty(dcatapAvailability);
        return this;
    }

    public Distribution setDescription(String description) {
        DctDescription dctDescription = new DctDescription(description);
        this.attachProperty(dctDescription);
        return this;
    }

    public Distribution setDescription(String description, String descriptionLanguage) {
        DctDescription dctDescription = new DctDescription(description, descriptionLanguage);
        this.attachProperty(dctDescription);
        return this;
    }

    public Distribution setFormat(String dctFormat) {
        DctFormat dctDctFormat1 = new DctFormat(dctFormat);
        this.attachProperty(dctDctFormat1);
        return this;
    }

    public Distribution setLicense(String dctLicense) {
        DctLicense dctDctLicense1 = new DctLicense(dctLicense);
        this.attachProperty(dctDctLicense1);
        return this;
    }


    /**
     * Optional
     */

    public Distribution setAccessService(String accessService) {
        DcatAccessService dcatAccessService = new DcatAccessService(accessService);
        this.attachProperty(dcatAccessService);
        return this;
    }

    public Distribution setByteSize(String byteSize) {
        DcatByteSize dcatByteSize = new DcatByteSize(byteSize);
        this.attachProperty(dcatByteSize);
        return this;
    }

    public Distribution setCheckSum(String checkSum) {
        SpdxCheckSum spdxCheckSumObject = new SpdxCheckSum(checkSum);
        this.attachProperty(spdxCheckSumObject);
        return this;
    }

    public Distribution setCompressFormat(String compressFormat) {
        DcatCompressFormat dcatCompressFormatObject = new DcatCompressFormat(compressFormat);
        this.attachProperty(dcatCompressFormatObject);
        return this;
    }

    public Distribution setPage(String foafPage) {
        FoafPage foafPageObject = new FoafPage(foafPage);
        this.attachProperty(foafPageObject);
        return this;
    }

    public Distribution setDownloadURL(String downloadURL) {
        DcatDownloadURL dcatDownloadURLObject = new DcatDownloadURL(downloadURL);
        this.attachProperty(dcatDownloadURLObject);
        return this;
    }

    public Distribution setHasPolicy(String odrlPolicy) {
        OdrlPolicy odrlPolicyObject = new OdrlPolicy(odrlPolicy);
        this.attachProperty(odrlPolicyObject);
        return this;
    }

    public Distribution setLanguage(String dctLanguage) {
        DctLanguage dctLanguageObject = new DctLanguage(dctLanguage);
        this.attachProperty(dctLanguageObject);
        return this;
    }

    public Distribution setConformsTo(String conformsTo) {
        DctConformsTo dctConformsToObject = new DctConformsTo(conformsTo);
        this.attachProperty(dctConformsToObject);
        return this;
    }

    public Distribution setMediaType(String mediaType) {
        DcatMediaType dcatMediaTypeObject = new DcatMediaType(mediaType);
        this.attachProperty(dcatMediaTypeObject);
        return this;
    }

    public Distribution setPackageFormat(String packageFormat) {
        DcatPackageFormat dcatPackageFormatObject = new DcatPackageFormat(packageFormat);
        this.attachProperty(dcatPackageFormatObject);
        return this;
    }

    public Distribution setIssued(Instant date) {
        DctIssued dctIssued = new DctIssued(date);
        this.attachProperty(dctIssued);
        return this;
    }

    public Distribution setRights(String rightsStatement) {
        DctRightsStatement dctRightsStatement1 = new DctRightsStatement(rightsStatement);
        this.attachProperty(dctRightsStatement1);
        return this;
    }

    public Distribution setSpatialResolutionInMeters(String spatialResolutionInMeters) {
        DcatSpatialResolutionInMeters dcatSpatialResolutionInMetersObject = new DcatSpatialResolutionInMeters(spatialResolutionInMeters);
        this.attachProperty(dcatSpatialResolutionInMetersObject);
        return this;
    }

    public Distribution setStatus(String admsStatus) {
        AdmsStatus admsStatusObject = new AdmsStatus(admsStatus);
        this.attachProperty(admsStatusObject);
        return this;
    }

    public Distribution setTemporalResolution(String temporalResolution) {
        DcatTemporalResolution dcatTemporalResolutionObject = new DcatTemporalResolution(temporalResolution);
        this.attachProperty(dcatTemporalResolutionObject);
        return this;
    }

    public Distribution setTitle(String title) {
        DctTitle dctTitle = new DctTitle(title);
        this.attachProperty(dctTitle);
        return this;
    }

    public Distribution setTitle(String title, String titleLanguage) {
        DctTitle dctTitle = new DctTitle(title, titleLanguage);
        this.attachProperty(dctTitle);
        return this;
    }

    public Distribution setModified(Instant date) {
        DctModified dctModified = new DctModified(date);
        this.attachProperty(dctModified);
        return this;
    }


    /**
     * This sets a dct:identifier, which is not part of DCAT-AP, but is super convenient.
     *
     * @param dctIdentifier
     * @return
     */
    public Distribution setIdentifier(String dctIdentifier) {
        DctIdentifier dctDctIdentifier1 = new DctIdentifier(dctIdentifier);
        this.attachProperty(dctDctIdentifier1);
        return this;
    }


    public Resource getResource() {
        return resource;
    }

    public Model getModel() {
        return model;
    }

}
