package io.piveau.dcatap.classes;

import io.piveau.dcatap.properties.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCAT;

import java.time.Instant;
import java.util.UUID;

public class Catalogue extends BaseClass {

    public Catalogue() {
        super("http://www.piveau.eu/set/catalogue/" + UUID.randomUUID(), DCAT.Catalog);
    }

    public Catalogue(String uri) {
        super(uri, DCAT.Catalog);
    }

    /**
     * Mandatory
     */
    public Catalogue setDataset(String dataset) {
        /**
         *
         * Class Attributes
         */
        DcatDataset dcatDataset1 = new DcatDataset(dataset);
        this.addMandatoryProperty(dcatDataset1.getClass().getSimpleName());
        this.attachProperty(dcatDataset1);
        return this;
    }

    public Catalogue setTitle(String title) {
        DctTitle dctTitle = new DctTitle(title);
        this.addMandatoryProperty(dctTitle.getClass().getSimpleName());
        this.attachProperty(dctTitle);
        return this;
    }

    public Catalogue setTitle(String title, String titleLanguage) {
        DctTitle dctTitle = new DctTitle(title, titleLanguage);
        this.addMandatoryProperty(dctTitle.getClass().getSimpleName());
        this.attachProperty(dctTitle);
        return this;
    }

    public Catalogue setDescription(String description) {
        DctDescription dctDescription = new DctDescription(description);
        this.addMandatoryProperty(dctDescription.getClass().getSimpleName());
        this.attachProperty(dctDescription);
        return this;
    }

    public Catalogue setDescription(String description, String descriptionLanguage) {
        DctDescription dctDescription = new DctDescription(description, descriptionLanguage);
        this.addMandatoryProperty(dctDescription.getClass().getSimpleName());
        this.attachProperty(dctDescription);
        return this;
    }

    public Catalogue setPublisher(String dctPublisher) {
        DctPublisher dctPublisherObject = new DctPublisher(dctPublisher);
        this.addMandatoryProperty(dctPublisherObject.getClass().getSimpleName());
        this.attachProperty(dctPublisherObject);
        return this;
    }

    /**
     * Recommended
     */

    public Catalogue setHomepage(String foafHomepage) {
        FoafHomepage foafFoafHomepage1 = new FoafHomepage(foafHomepage);
        this.attachProperty(foafFoafHomepage1);
        return this;
    }

    public Catalogue setLanguage(String dctLanguage) {
        DctLanguage dctLanguage1 = new DctLanguage(dctLanguage);
        this.attachProperty(dctLanguage1);
        return this;
    }

    public Catalogue setLicense(String dctLicense) {
        DctLicense dctDctLicense1 = new DctLicense(dctLicense);
        this.attachProperty(dctDctLicense1);
        return this;
    }

    public Catalogue setIssued(Instant date) {
        DctIssued dctIssued = new DctIssued(date);
        this.attachProperty(dctIssued);
        return this;
    }

    public Catalogue setSpatial(String spatial) {
        DctSpatial dctSpatial1 = new DctSpatial(spatial);
        this.attachProperty(dctSpatial1);
        return this;
    }

    public Catalogue setThemeTaxonomy(String themeTaxonomy) {
        DcatThemeTaxonomy dcatThemeTaxonomy1 = new DcatThemeTaxonomy(themeTaxonomy);
        this.attachProperty(dcatThemeTaxonomy1);
        return this;
    }

    public Catalogue setModified(Instant date) {
        DctModified dctModified = new DctModified(date);
        this.attachProperty(dctModified);
        return this;
    }

    /**
     * Optional
     */

    public Catalogue setHasPart(String hasPart) {
        DctHasPart dctHasPart1 = new DctHasPart(hasPart);
        this.attachProperty(dctHasPart1);
        return this;
    }

    public Catalogue setIsPartOf(String isPartOf) {
        DctIsPartOf dctIsPartOf1 = new DctIsPartOf(isPartOf);
        this.attachProperty(dctIsPartOf1);
        return this;
    }

    public Catalogue setRights(String rightsStatement) {
        DctRightsStatement dctRightsStatement1 = new DctRightsStatement(rightsStatement);
        this.attachProperty(dctRightsStatement1);
        return this;
    }

    public Catalogue setType(String dctType) {
        DctType dctDctType1 = new DctType(dctType);
        this.attachProperty(dctDctType1);
        return this;
    }

    public Catalogue setCatalogue(String dcatCatalogue) {
        DcatCatalogue dcatCatalogue1 = new DcatCatalogue(dcatCatalogue);
        this.attachProperty(dcatCatalogue1);
        return this;
    }

    public Catalogue setCreator(String dctCreator) {
        DctCreator dctCreator1 = new DctCreator(dctCreator);
        this.attachProperty(dctCreator1);
        return this;
    }

    public Model getModel() {
        return this.model;
    }

    public Resource getResource() {
        return resource;
    }
}
