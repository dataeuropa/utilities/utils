package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.VCARD4;

public class DcatContactPoint extends BaseProperty {
    private String contactPointEmail;
    private String contactPointName;

    public DcatContactPoint(String contactPointEmail, String contactPointName) {
        super(null, DCAT.contactPoint);
        this.contactPointEmail = contactPointEmail;
        this.contactPointName = contactPointName;
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource()
                .addProperty(this.namespace, schema.getModel().createResource()
                        .addProperty(RDF.type, VCARD4.Organization)
                        .addProperty(VCARD4.fn, contactPointName)
                        .addProperty(VCARD4.hasEmail, contactPointEmail));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
