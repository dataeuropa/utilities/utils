package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import io.piveau.vocabularies.vocabulary.PROV;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDF;

import java.time.Instant;

public class ProvWasGeneratedBy extends BaseProperty {
    private Instant generatedStartDate, generatedEndDate;

    public ProvWasGeneratedBy(Instant generatedStartDate, Instant generatedEndDate) {
        super(null, PROV.wasGeneratedBy);
        this.generatedStartDate = generatedStartDate;
        this.generatedEndDate = generatedEndDate;
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(namespace, schema.getModel().createResource()
                .addProperty(RDF.type, schema.getModel().createProperty("http://www.w3.org/ns/prov#Activity"))
                .addProperty(schema.getModel().createProperty("http://www.w3.org/ns/prov#startedAtTime"), ResourceFactory.createTypedLiteral(generatedEndDate.toString(), XSDDatatype.XSDdateTime))
                .addProperty(schema.getModel().createProperty("http://www.w3.org/ns/prov#endedAtTime"), ResourceFactory.createTypedLiteral(generatedStartDate.toString(), XSDDatatype.XSDdateTime)));


    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
