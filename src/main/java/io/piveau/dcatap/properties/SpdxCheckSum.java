package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import io.piveau.vocabularies.vocabulary.SPDX;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

public class SpdxCheckSum extends BaseProperty {
    private Resource checksumAlgorithm;

    public SpdxCheckSum(String checkSum) {
        super(checkSum, SPDX.checksum);
        this.checksumAlgorithm = SPDX.checksumAlgorithm_md5;
    }

    public SpdxCheckSum(String checkSum, Resource checksumAlgorithm) {
        super(checkSum, SPDX.checksum);
        this.checksumAlgorithm = checksumAlgorithm;
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(this.namespace, schema.getModel().createResource()
                .addProperty(RDF.type, SPDX.Checksum)
                .addProperty(SPDX.algorithm, this.checksumAlgorithm)
                .addProperty(SPDX.checksumValue, this.value));
    }
}
