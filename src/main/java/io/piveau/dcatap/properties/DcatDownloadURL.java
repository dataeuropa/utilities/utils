package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCAT;

public class DcatDownloadURL extends BaseProperty {

    public DcatDownloadURL(String downloadURL) {
        super(downloadURL, DCAT.downloadURL);
    }
}
