package io.piveau.dcatap.properties;

import org.apache.jena.rdf.model.ModelFactory;

public class DcatQualifiedRelation extends BaseProperty {

    public DcatQualifiedRelation(String qualifiedRelation) {
        super(qualifiedRelation, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#qualifiedRelation"));
    }
}
