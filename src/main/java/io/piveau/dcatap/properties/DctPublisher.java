package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;

public class DctPublisher extends BaseProperty {
    private String publisherName, publisherHomepage;

    public DctPublisher(String publisherName) {
        super(publisherName, DCTerms.publisher);
    }

    public DctPublisher(String publisherName, String publisherHomepage) {
        super(publisherName, DCTerms.publisher);
        this.publisherName = publisherName;
        this.publisherHomepage = publisherHomepage;
    }

    @Override
    public void addProperty(BaseClass schema) {
        if (publisherName != null && publisherHomepage != null) this.addPropertyWithHomepage(schema);
        else super.addProperty(schema);
    }

    public void addPropertyWithHomepage(BaseClass schema) {
        schema.getResource()
                .addProperty(namespace, schema.getModel().createResource()
                        .addProperty(RDF.type, FOAF.Organization)
                        .addLiteral(FOAF.name, this.publisherName)
                        .addLiteral(FOAF.homepage, this.publisherHomepage));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
