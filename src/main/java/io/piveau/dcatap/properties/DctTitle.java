package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.DCTerms;

public class DctTitle extends BaseProperty {

    public DctTitle(String title) {
        super(title, DCTerms.title);
    }

    public DctTitle(String title, String language) {
        super(title, language, DCTerms.title);
    }

    /**
     * Override to keep default language
     *
     * @param schema
     */
    @Override
    public void addProperty(BaseClass schema) {
        this.addPropertyLanguage(schema);
    }
}
