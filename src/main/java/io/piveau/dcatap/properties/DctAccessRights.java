package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

public class DctAccessRights extends BaseProperty {

    public DctAccessRights(String accessRights) {
        super(accessRights, DCTerms.accessRights);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(DCTerms.accessRights, schema.getModel().createResource()
                .addProperty(RDF.type, namespace)
                .addProperty(RDFS.label, value));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
