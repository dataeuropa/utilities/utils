package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import io.piveau.vocabularies.vocabulary.ADMS;

public class AdmsVersionNote extends BaseProperty {

    public AdmsVersionNote(String versionNote) {
        super(versionNote, ADMS.versionNotes);
    }

    public AdmsVersionNote(String versionNote, String language) {
        super(versionNote, language, ADMS.versionNotes);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(namespace, value);
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
        schema.getResource().addProperty(namespace, value, language);
    }
}
