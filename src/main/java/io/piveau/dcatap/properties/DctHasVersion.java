package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctHasVersion extends BaseProperty {

    public DctHasVersion(String hasVersion) {
        super(hasVersion, DCTerms.hasVersion);
    }
}
