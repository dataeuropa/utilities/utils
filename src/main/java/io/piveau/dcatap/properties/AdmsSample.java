package io.piveau.dcatap.properties;

import io.piveau.vocabularies.vocabulary.ADMS;

public class AdmsSample extends BaseProperty {

    public AdmsSample(String sample) {
        super(sample, ADMS.sample);
    }
}
