package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCAT;

public class DcatThemeTaxonomy extends BaseProperty {

    public DcatThemeTaxonomy(String themeTaxonomy) {
        super(themeTaxonomy, DCAT.themeTaxonomy);
    }
}
