package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.DCTerms;

import java.time.Instant;

public class DctIssued extends BaseProperty {

    public DctIssued(Instant date) {
        super(date.toString(), DCTerms.issued);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(this.namespace,
                ResourceFactory.createTypedLiteral(value, XSDDatatype.XSDdateTime));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
        this.addProperty(schema);
    }
}
