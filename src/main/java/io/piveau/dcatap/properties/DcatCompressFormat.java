package io.piveau.dcatap.properties;

import org.apache.jena.rdf.model.ModelFactory;

public class DcatCompressFormat extends BaseProperty {

    public DcatCompressFormat(String compressFormat) {
        super(compressFormat, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#compressFormat"));
    }
}
