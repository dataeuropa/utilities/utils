package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.SKOS;

public class DcatapAvailability extends BaseProperty {

    public DcatapAvailability(String availability) {
        super(availability, ModelFactory.createDefaultModel().createProperty("http://data.europa.eu/r5r/availability"));
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(this.namespace, schema.getModel().createResource()
                .addProperty(RDF.type, SKOS.Concept)
                .addProperty(SKOS.prefLabel, this.value));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
