package io.piveau.dcatap.properties;

import org.apache.jena.vocabulary.DCTerms;

public class DctLanguage extends BaseProperty {

    public DctLanguage(String dctLanguage) {
        super(dctLanguage, DCTerms.language);
    }
}
