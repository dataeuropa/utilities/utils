package io.piveau.dcatap.properties;

import io.piveau.vocabularies.vocabulary.ADMS;

public class AdmsStatus extends BaseProperty {

    public AdmsStatus(String admsStatus) {
        super(admsStatus, ADMS.status);
    }

}
