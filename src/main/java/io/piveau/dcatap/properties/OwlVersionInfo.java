package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.OWL;

public class OwlVersionInfo extends BaseProperty {

    public OwlVersionInfo(String versionInfo) {
        super(versionInfo, OWL.versionInfo);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(this.namespace, value);
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
        schema.getResource().addProperty(this.namespace, value, language);
    }
}
