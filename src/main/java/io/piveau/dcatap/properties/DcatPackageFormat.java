package io.piveau.dcatap.properties;

import org.apache.jena.rdf.model.ModelFactory;

public class DcatPackageFormat extends BaseProperty {

    public DcatPackageFormat(String packageFormat) {
        super(packageFormat, ModelFactory.createDefaultModel().createProperty("http://www.w3.org/ns/dcat#packageFormat"));
    }
}
