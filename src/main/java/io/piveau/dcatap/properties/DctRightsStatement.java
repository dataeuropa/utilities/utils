package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

public class DctRightsStatement extends BaseProperty {

    public DctRightsStatement(String rightValue) {
        super(rightValue, DCTerms.rights);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(namespace, schema.getModel().createResource()
                .addProperty(RDF.type, DCTerms.RightsStatement)
                .addProperty(RDFS.label, value));

    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
        schema.getResource().addProperty(namespace, schema.getModel().createResource()
                .addProperty(RDF.type, DCTerms.RightsStatement)
                .addProperty(RDFS.label, value));

    }
}
