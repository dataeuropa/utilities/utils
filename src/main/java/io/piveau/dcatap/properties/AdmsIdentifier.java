package io.piveau.dcatap.properties;

import io.piveau.vocabularies.vocabulary.ADMS;
import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.datatypes.TypeMapper;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.SKOS;

public class AdmsIdentifier extends BaseProperty {
    String admsIdentifierNotation, admsIdentifierNotationType, admsIdentifierLabel;

    public AdmsIdentifier(String admsIdentifierNotation, String admsIdentifierNotationType, String admsIdentifierLabel) {
        super(null, ADMS.identifier);
        this.admsIdentifierNotation = admsIdentifierNotation;
        this.admsIdentifierNotationType = admsIdentifierNotationType;
        this.admsIdentifierLabel = admsIdentifierLabel;
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(namespace, schema.getModel().createResource()
                .addProperty(RDF.type, namespace)
                .addProperty(SKOS.notation, this.admsIdentifierNotation, TypeMapper.getInstance().getSafeTypeByName(this.admsIdentifierNotationType))
                .addProperty(RDFS.label, this.admsIdentifierLabel));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
    }
}
