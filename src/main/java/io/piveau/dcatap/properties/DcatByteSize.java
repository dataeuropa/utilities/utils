package io.piveau.dcatap.properties;

import io.piveau.dcatap.classes.BaseClass;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.DCAT;

public class DcatByteSize extends BaseProperty {

    public DcatByteSize(String byteSize) {
        super(byteSize, DCAT.byteSize);
    }

    @Override
    public void addProperty(BaseClass schema) {
        schema.getResource().addProperty(namespace,
                ResourceFactory.createTypedLiteral(value, XSDDatatype.XSDdecimal));
    }

    @Override
    public void addPropertyLanguage(BaseClass schema) {
        this.addProperty(schema);
    }
}
