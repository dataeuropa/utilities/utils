package io.piveau.utils

internal val semVerRegEx =
    Regex("""^(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)(?:-((?:0|[1-9][0-9]*|[0-9]*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9][0-9]*|[0-9]*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?${'$'}""")

@ExperimentalUnsignedTypes
class SemVer @JvmOverloads constructor(version: String = "0.0.0") {

    var major: UInt
    var minor: UInt
    var patch: UInt

    var preRelease: String = ""
        set(value) {
            if (stringifySemVer(major, minor, patch, value, buildMetadata).isSemVer()) field =
                value else throw IllegalArgumentException()
        }

    var buildMetadata: String = ""
        set(value) {
            if (stringifySemVer(major, minor, patch, preRelease, value).isSemVer()) field =
                value else throw IllegalArgumentException()
        }

    init {
        val matches = semVerRegEx.matchEntire(version)?.destructured ?: throw IllegalArgumentException()
        major = matches.component1().toUInt()
        minor = matches.component2().toUInt()
        patch = matches.component3().toUInt()
        preRelease = matches.component4()
        buildMetadata = matches.component5()
    }

    operator fun compareTo(other: SemVer): Int {
        if (this == other) return 0
        if (major == other.major && minor == other.minor && patch == other.patch && preRelease.isNotBlank() && other.preRelease.isBlank()) return -1
        if (major == other.major && minor == other.minor && patch == other.patch && preRelease.isNotBlank() && other.preRelease.isNotBlank()) return PreRelease(
            preRelease
        ).compareTo(
            PreRelease(other.preRelease)
        )

        if (major < other.major) return -1
        if (minor < other.minor && major <= other.major) return -1
        if (patch < other.patch && minor <= other.minor && major <= other.major) return -1

        return 1
    }

    override fun toString(): String = stringifySemVer(major, minor, patch, preRelease, buildMetadata)

    override fun hashCode(): Int = toString().hashCode()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SemVer

        return (major == other.major && minor == other.minor && patch == other.patch && preRelease == other.preRelease)
    }

}

internal class PreRelease(preRelease: String) {
    val tokens = preRelease.split(".")
    operator fun compareTo(other: PreRelease): Int {
        tokens.zip(other.tokens).forEach {
            if (it.first.isNumeric() && it.second.isNumeric()) {
                val compared = it.first.toInt().compareTo(it.second.toInt())
                if (compared != 0) return compared
            } else {
                val compared = it.first.compareTo(it.second)
                if (compared != 0) return compared
            }
        }
        return if (tokens.size < other.tokens.size) -1 else 0
    }
}

@ExperimentalUnsignedTypes
internal fun stringifySemVer(major: UInt, minor: UInt, patch: UInt, preRelease: String, buildMetadata: String): String {
    var str = "$major.$minor.$patch"
    if (preRelease.isNotBlank()) str += "-$preRelease"
    if (buildMetadata.isNotBlank()) str += "+$buildMetadata"
    return str
}

internal fun String.isNumeric() = matches("""^\d+$""".toRegex())
fun String.isSemVer() = matches(semVerRegEx)
