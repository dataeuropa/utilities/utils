package io.piveau.utils

import io.vertx.core.http.HttpHeaders
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonArray
import io.vertx.ext.web.handler.CorsHandler
import io.vertx.ext.web.openapi.RouterBuilder
import kotlin.streams.toList

/**
 * Create a standard CorsHandler suitable for most piveau services. The constructor needs an Array with
 * allowed Domain names that can appear in the Origin Header of a request.
 *
 * A list of the following Headers is allowed by default, but can be changed by changing the `allowedHeaders`:
 * "x-requested-with", "Access-Control-Allow-Origin","Access-Control-Allow-Headers",  "Origin", "Content-Type", "Accept", "Authorization"
 *
 * A list of the following Methods is allowed by default, but can be changed by changing the `allowedMethods`:
 * GET, DELETE, OPTIONS, POST, PUT
 *
 * As a convenience method `addRootHandler` can be used to directly add a root handler to a router builder object
 *
 */
class CorsHelper(corsDomains: JsonArray) {


    constructor(corsDomains: JsonArray, allowedHeaders: Set<String>, allowedMethods: Set<HttpMethod>) : this(corsDomains) {
        this.allowedHeaders = allowedHeaders
        this.allowedMethods = allowedMethods

    }

    private val domainList: List<String?> = corsDomains
        .stream()
        .map { it -> (it as? String)?.replace(".", "\\.") }
        .filter { it != null }
        .toList()


    val corsString = "^(https?:\\/\\/(?:.+\\.)?(?:${ domainList.joinToString("|") })(?::\\d{1,5})?)$"


    /**
     *
     */
    var allowedHeaders = setOf<String>("x-requested-with",
        HttpHeaders.AUTHORIZATION.toString(),
        HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS.toString(),
        HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN.toString(),
        HttpHeaders.ORIGIN.toString(),
        HttpHeaders.CONTENT_TYPE.toString(),
        HttpHeaders.ACCEPT.toString()
    )
    set(value) {
        field = value
        handler.allowedHeaders(value)
    }

    var allowedMethods = setOf<HttpMethod>(HttpMethod.GET, HttpMethod.DELETE, HttpMethod.OPTIONS, HttpMethod.POST, HttpMethod.PUT)
    set(value) {
        field = value
        handler.allowedMethods(value)
    }




     val handler:CorsHandler = CorsHandler.create(corsString).allowedHeaders(allowedHeaders).allowedMethods(allowedMethods)






    /**
     *Append the generated CorsHandler to the routerBuilder as RootHandler so the CorsHandler will be called on every request
     *
     * @param routerBuilder
     * @return  a reference to the RouterBuilder
     */
    fun addRootHandler(routerBuilder: RouterBuilder): RouterBuilder {
        routerBuilder.rootHandler(handler)

        return routerBuilder
    }




}

/**
 * Add a Cors handler as root handler
 *
 * @param corsDomains a JsonArray of allowed Domains
 * @return self
 */
fun RouterBuilder.corsHandlerAsRootHandler(corsDomains: JsonArray):RouterBuilder {
    return rootHandler(CorsHelper(corsDomains).handler)
}