@file:JvmName("Piveau")
@file:JvmMultifileClass

package io.piveau.utils

import io.piveau.log.PiveauLogger
import io.piveau.rdf.*
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpHeaders
import io.vertx.ext.shell.cli.Completion
import io.vertx.ext.web.client.HttpRequest
import io.vertx.ext.web.client.HttpResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.apache.jena.rdf.model.Model
import org.sqids.Sqids
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.math.BigInteger
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

/**
 * The piveau context for a specific service and module.
 *
 * A service is a piveau service and a module is a functional group inside the service. E.g. the hub is a service and the
 * indexing is a module inside the hub service.
 *
 * Currently, the context is just a wrapper around a [PiveauLogger]
 *
 * The context can be extended to also cover a specific resource via [extend]. In an not extended piveau context the resource
 * name is "self".
 *
 * @see io.piveau.log.PiveauLogger
 * @property serviceName The name or identifier of the service
 * @property moduleName The name or identifier of the module
 * @constructor Creates a piveau context for a module within a service
 */
class PiveauContext(val serviceName: String, val moduleName: String) {
    private var resource = "self"

    private constructor(serviceName: String, moduleName: String, resource: String) : this(serviceName, moduleName) {
        this.resource = resource
    }

    /**
     * Extending the piveau context with a resource
     *
     * @param resource The name or identifier of the resource
     * @return A new [PiveauContext] representing the same service and module but also the resource
     */
    fun extend(resource: String) = PiveauContext(serviceName, moduleName, resource)

    private val log: PiveauLogger by lazy {
        PiveauLogger(
            serviceName,
            moduleName,
            resource
        )
    }

    /**
     * Access to the context related logger
     *
     * @return The [PiveauLogger]
     */
    fun log() = log

}

private val M = BigInteger("ffffffffffffffffffffffffffffffff", 16)

private fun combineHashes(base: BigInteger, hash: String): BigInteger = base.add(BigInteger(hash, 16)).mod(M)

/**
 * Generates a canonical hash value for a [org.apache.jena.rdf.model.Model]
 *
 * @receiver The model to calculate the hash value for
 * @return Hash value as MD5 hex [String]
 */
fun Model.canonicalHash(): String {
    var totalHash = BigInteger.ZERO
    listStatements().forEachRemaining { statement ->
        var basicHash = BigInteger(statement.tripleHash(), 16)
        val (subj, _, obj) = statement
        if (subj.isAnon) {
            listStatements(null, null, subj).forEachRemaining {
                basicHash = combineHashes(basicHash, it.tripleHash())
            }
        }
        if (obj.isAnon) {
            obj.asResource().listProperties().forEachRemaining {
                basicHash = combineHashes(basicHash, it.tripleHash())
            }
        }
        totalHash += basicHash
        totalHash.mod(M)
    }
    return totalHash.toString(16)
}

fun Completion.candidatesCompletion(pool: List<String>) = candidatesCompletion(lineTokens().last().value(), pool)
fun Completion.candidatesCompletion(input: String, pool: List<String>) {
    if (input.isNotBlank()) {
        val candidates = pool.filter { name -> name.startsWith(input) }.toList().sorted()
        when (candidates.size) {
            0 -> complete("", false)
            1 -> complete(candidates[0].substring(input.length), true)
            else -> {
                if (Completion.findLongestCommonPrefix(candidates) != input) {
                    complete(Completion.findLongestCommonPrefix(candidates).substring(input.length), false)
                } else {
                    complete(candidates)
                }
            }
        }
    } else {
        complete(pool)
    }
}

fun HttpRequest<Buffer>.sendBufferDigestAuth(
    username: String,
    password: String,
    buffer: Buffer
): Future<HttpResponse<Buffer>> {
    return Future.future { promise ->
        sendBuffer(buffer)
            .compose {
                when (it.statusCode()) {
                    401 -> {
                        if (it.headers().contains("WWW-Authenticate")) {
                            putHeader(
                                HttpHeaders.AUTHORIZATION.toString(),
                                DigestAuth.authenticate(
                                    it.getHeader("WWW-Authenticate"),
                                    uri(),
                                    method().toString(),
                                    username,
                                    password
                                )
                            ).sendBuffer(buffer)
                        } else {
                            Future.succeededFuture(it)
                        }
                    }

                    else -> Future.succeededFuture(it)
                }
            }
            .onComplete(promise)
    }
}

fun HttpRequest<Buffer>.sendDigestAuth(
    username: String,
    password: String
): Future<HttpResponse<Buffer>> {
    return Future.future { promise ->
        send()
            .compose {
                when (it.statusCode()) {
                    401 -> {
                        if (it.headers().contains("WWW-Authenticate")) {
                            putHeader(
                                HttpHeaders.AUTHORIZATION.toString(),
                                DigestAuth.authenticate(
                                    it.getHeader("WWW-Authenticate"),
                                    uri(),
                                    method().toString(),
                                    username,
                                    password
                                )
                            ).send()
                        } else {
                            Future.succeededFuture(it)
                        }
                    }

                    else -> Future.succeededFuture(it)
                }
            }
            .onComplete(promise)
    }
}

suspend fun gunzip(source: String, target: String) {
    withContext(Dispatchers.IO) {
        GZIPInputStream(FileInputStream(source)).use { input ->
            val outputFile = File(target)
            FileOutputStream(outputFile).use { output ->
                input.copyTo(output)
            }
        }
    }
}

suspend fun gzip(source: String, target: String) {
    withContext(Dispatchers.IO) {
        FileInputStream(source).use { input ->
            val outputFile = File(target)
            GZIPOutputStream(FileOutputStream(outputFile)).use { output ->
                input.copyTo(output)
            }
        }
    }
}

fun sqidsWithTime(counter: Long): String = Sqids.builder().build().encode(listOf(System.currentTimeMillis(), counter))


