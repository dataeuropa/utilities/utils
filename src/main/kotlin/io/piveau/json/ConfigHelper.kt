package io.piveau.json

import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject

/**
 * Wrapper for a JSON config object
 */
class ConfigHelper private constructor (private val config: JsonObject) {
    companion object {
        /**
         * Create a [ConfigHelper] for a config
         *
         * @param config JsonObject containing config values
         * @return [ConfigHelper] for [config]
         */
        @JvmStatic
        fun forConfig(config: JsonObject): ConfigHelper =
            ConfigHelper(config)
    }

    @Deprecated("Renamed.", ReplaceWith("forceJsonObject(key)"))
    fun getJson(key: String): JsonObject = config.asJsonObject(key)

    /**
     * Get the value from config as [JsonObject]
     *
     * @param key The key for the JSON object
     * @return [JsonObject] of [key]
     */
    fun forceJsonObject(key: String): JsonObject = config.asJsonObject(key)

    /**
     * Get the value from config as [JsonArray]
     *
     * @param key The key for the JSON array
     * @return [JsonArray] of [key]
     */
    fun forceJsonArray(key: String): JsonArray = config.asJsonArray(key)

}
