@file:JvmName("JsonExtensions")
package io.piveau.json

import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject

fun JsonArray.addIfNotEmpty(value: String?): JsonArray = if (value?.isNotEmpty() == true) add(value) else this
fun JsonArray.addIfNotBlank(value: String?): JsonArray = if (value?.isNotBlank() == true) add(value) else this
fun JsonArray.addIfNotEmpty(value: JsonObject): JsonArray = if (value.isEmpty) this else add(value)
fun JsonArray.addIfNotNull(value: Any?): JsonArray = value?.let { add(value) } ?: this

fun JsonObject.withJsonObject(key: String): JsonObject = getJsonObject(key) ?: JsonObject().apply { this@withJsonObject.put(key, this) }
fun JsonObject.withJsonArray(key: String): JsonArray = getJsonArray(key) ?: JsonArray().apply { this@withJsonArray.put(key, this) }

fun JsonObject.putIfNotEmpty(key: String, value: JsonObject): JsonObject = if (value.isEmpty) this else put(key, value)
fun JsonObject.putIfNotEmpty(key: String, value: String?): JsonObject = if (value?.isNotEmpty() == true) put(key, value) else this
fun JsonObject.putIfNotBlank(key: String, value: String?): JsonObject = if (value?.isNotBlank() == true) put(key, value) else this
fun JsonObject.putIfNotNull(key: String, value: Any?): JsonObject = value?.let { put(key, value) } ?: this
fun JsonObject.putPair(pair: Pair<String, JsonObject?>): JsonObject = putIfNotNull(pair.first, pair.second)
fun JsonObject.putPairIfNotNull(pair: Pair<String, JsonObject?>?): JsonObject = pair?.let { putIfNotNull(pair.first, pair.second) } ?: this

fun JsonObject.asJsonObject(key: String): JsonObject = when(val value = this.getValue(key)) {
    is JsonObject -> value
    is String -> JsonObject(value)
    else -> JsonObject()
}

fun JsonObject.asJsonArray(key: String): JsonArray = when(val value = this.getValue(key)) {
    is JsonArray -> value
    is String -> JsonArray(value)
    else -> JsonArray()
}
