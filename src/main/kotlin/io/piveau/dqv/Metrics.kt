@file:JvmName("PiveauMetrics")

package io.piveau.dqv

import io.piveau.rdf.extractAsModel
import io.piveau.vocabularies.vocabulary.DQV
import io.piveau.vocabularies.vocabulary.PROV
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.datatypes.xsd.XSDDatatype
import org.apache.jena.query.Dataset
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.OA
import java.time.Instant

// Get all named graphs containing a dqv:QualityMetadata resource
fun Dataset.listMetricsModels(): List<Model> {
    return listNames().asSequence().map { name -> getNamedModel(name) }
        .filter { it.containsResource(DQV.QualityMetadata) }.toList()
}

fun Dataset.createMetricsGraph(name: String): Model = addNamedModel(name, ModelFactory.createDefaultModel().apply {
    createResource(name, DQV.QualityMetadata).addProperty(
        PROV.generatedAtTime,
        createTypedLiteral(Instant.now(), XSDDatatype.XSDdateTime)
    )
}).getNamedModel(name)

fun Model.removeMeasurement(target: Resource, metric: Resource): Model {

    val computedOn = getResource(target.uri)

    val iterator =
        computedOn.listProperties(DQV.hasQualityMeasurement)
            .filterKeep {
                it.resource.hasProperty(DQV.isMeasurementOf, metric)
            }.mapWith { it.resource }

    while (iterator.hasNext()) {
        val resource = iterator.removeNext()
        resource.removeProperties()
        remove(computedOn, DQV.hasQualityMeasurement, resource)
    }
    return this
}

fun Model.removeMeasurement(target: Resource, metric: Resource, value: Resource): Model {

    val computedOn = getResource(target.uri)

    val iterator =
        computedOn.listProperties(DQV.hasQualityMeasurement)
            .filterKeep {
                it.resource.hasProperty(DQV.isMeasurementOf, metric) && it.resource.hasProperty(DQV.computedOn, value)
            }.mapWith { it.resource }

    while (iterator.hasNext()) {
        val resource = iterator.removeNext()
        resource.removeProperties()
        remove(computedOn, DQV.hasQualityMeasurement, resource)
    }
    return this
}

fun Model.addMeasurement(target: Resource, metric: Resource, value: Any): Resource =
    addMeasurement(target, target, metric, value)

fun Model.addMeasurement(target: Resource, computedOn: Resource, metric: Resource, value: Any): Resource =
    createResource(DQV.QualityMeasurement).apply {
        addProperty(DQV.isMeasurementOf, metric)
        addProperty(DQV.computedOn, computedOn)
        addProperty(PROV.generatedAtTime, model.createTypedLiteral(Instant.now(), XSDDatatype.XSDdateTime))
        addProperty(DQV.value, createTypedLiteral(value))
    }.also { add(target, DQV.hasQualityMeasurement, it) }

fun Model.replaceMeasurement(target: Resource, metric: Resource, value: Any) =
    removeMeasurement(target, metric).addMeasurement(target, metric, value)

fun Model.removeAnnotation(target: Resource, motivatedBy: Resource): Model {

    val hasTarget = getResource(target.uri)

    val iterator =
        hasTarget.listProperties(DQV.hasQualityAnnotation)
            .filterKeep {
                it.resource.hasProperty(OA.motivatedBy, motivatedBy)
            }.mapWith { it.resource }

    while (iterator.hasNext()) {
        val resource = iterator.removeNext()
        val model = resource.extractAsModel()
        remove(model)
//        remove(hasTarget, DQV.hasQualityAnnotation, resource)
    }
    return this
}

fun Model.addAnnotation(target: Resource, motivatedBy: Resource, body: Resource, dimension: Resource? = PV.interoperability): Resource =
    createResource(DQV.QualityAnnotation).apply {
        addProperty(OA.motivatedBy, motivatedBy)
        addProperty(OA.hasTarget, target)
        addProperty(PROV.generatedAtTime, model.createTypedLiteral(Instant.now(), XSDDatatype.XSDdateTime))
        addProperty(OA.hasBody, body)
        if (dimension != null) {
            addProperty(DQV.inDimension, dimension)
        }
    }.also { add(target, DQV.hasQualityAnnotation, it) }

fun Model.replaceAnnotation(target: Resource, motivatedBy: Resource, body: Resource): Resource =
    removeAnnotation(target, motivatedBy).addAnnotation(target, motivatedBy, body)