package io.piveau.log

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Logger for the piveau context.
 *
 * The piveau logger name is `piveau.${name}`. The high level message format is `[${component}] [${resource}] <message>`.
 * `name`, `component` and `resource` are set via the constructor.
 *
 * The logging functions of this class are very close (if not identical) to the SLF4J API.
 *
 * @param name The service name. It will be prefixed with `piveau.` to serve as logger name
 * @param component The component name of the sub component inside the service with [name]
 * @param resource The resource name
 * @see io.piveau.utils.PiveauContext
 * @constructor Creates a piveau logger for a resource within a module of a service.
 */
class PiveauLogger(name: String, component: String, resource: String) {

    private val log: Logger = LoggerFactory.getLogger("piveau.$name")
    private val prefix: String = "[$component] [$resource] "

    fun error(message: String) = log.error(prefix + message)

    fun error(format: String, argument1: Any) = log.error(prefix + format, argument1)

    fun error(format: String, argument1: Any, argument2: Any) = log.error(prefix + format, argument1, argument2)

    fun error(format: String, vararg arguments: Any) = log.error(prefix + format, *arguments)

    fun error(format: String, e: Throwable) = log.error(prefix + format, e)

    fun warn(message: String) = log.warn(prefix + message)

    fun warn(format: String, argument1: Any) = log.warn(prefix + format, argument1)

    fun warn(format: String, argument1: Any, argument2: Any) = log.warn(prefix + format, argument1, argument2)

    fun warn(format: String, vararg arguments: Any) = log.warn(prefix + format, *arguments)

    fun warn(format: String, e: Throwable) = log.warn(prefix + format, e)

    fun info(message: String) = log.info(prefix + message)

    fun info(format: String, argument1: Any) = log.info(prefix + format, argument1)

    fun info(format: String, argument1: Any, argument2: Any) = log.info(prefix + format, argument1, argument2)

    fun info(format: String, vararg arguments: Any) = log.info(prefix + format, *arguments)

    fun info(format: String, e: Throwable) = log.info(prefix + format, e)

    fun debug(message: String) = log.debug(prefix + message)

    fun debug(format: String, argument1: Any) = log.debug(prefix + format, argument1)

    fun debug(format: String, argument1: Any, argument2: Any) = log.debug(prefix + format, argument1, argument2)

    fun debug(format: String, vararg arguments: Any) = log.debug(prefix + format, *arguments)

    fun debug(format: String, e: Throwable) = log.debug(prefix + format, e)

    fun trace(message: String) = log.trace(prefix + message)

    fun trace(format: String, argument1: Any) = log.trace(prefix + format, argument1)

    fun trace(format: String, argument1: Any, argument2: Any) = log.trace(prefix + format, argument1, argument2)

    fun trace(format: String, vararg arguments: Any) = log.trace(prefix + format, *arguments)

    fun trace(format: String, e: Throwable) = log.trace(prefix + format, e)

    val isTraceEnabled
        get() = log.isTraceEnabled

    val isDebugEnabled
        get() = log.isDebugEnabled

    val isInfoEnabled
        get() = log.isInfoEnabled

    val isWarnEnabled
        get() = log.isWarnEnabled

    val isErrorEnabled
        get() = log.isErrorEnabled

}