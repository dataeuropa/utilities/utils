package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource

object ADMS {
    const val NS = "http://www.w3.org/ns/adms#"
    const val NS_LEGACY = "http://purl.org/adms/"

    const val NS_SCHEME_ASSET_TYPE = "${NS_LEGACY}assettype/"
    const val NS_SCHEME_INTEROPERABILITY_LEVEL = "${NS_LEGACY}interoperabilitylevel/"
    const val NS_SCHEME_LICENCE_TYPE = "${NS_LEGACY}licencetype/"
    const val NS_SCHEME_PUBLISHER_TYPE = "${NS_LEGACY}publishertype/"
    const val NS_SCHEME_REPRESENTATION_TECHNIQUE = "${NS_LEGACY}representationtechnique/"
    const val NS_SCHEME_STATUS = "${NS_LEGACY}status/"

    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val Identifier: Resource = m.createResource("${NS}Identifier")

    @JvmField
    val status: Property = m.createProperty(NS, "status")
    @JvmField
    val identifier: Property = m.createProperty(NS, "identifier")
    @JvmField
    val sample: Property = m.createProperty(NS, "sample")
    @JvmField
    val versionNotes: Property = m.createProperty(NS, "versionNotes")
}
