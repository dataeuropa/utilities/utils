package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource

object EDP {
    const val NS = "https://europeandataportal.eu/voc#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val TransInProcess: Resource = m.createResource("${NS}TransInProcess")
    @JvmField
    val TransCompleted: Resource = m.createResource("${NS}TransCompleted")

    @JvmField
    val MetricsLatest: Resource = m.createResource("${NS}MetricsLatest")
    @JvmField
    val MetricsHistory: Resource = m.createResource("${NS}MetricsHistory")

    @JvmField
    val licensingAssistant = m.createProperty("${NS}licensingAssistant")

    @JvmField
    val transIssued = m.createProperty("${NS}transIssued")
    @JvmField
    val transReceived = m.createProperty("${NS}transReceived")
    @JvmField
    val transStatus = m.createProperty("${NS}transStatus")
    @JvmField
    val originalLanguage = m.createProperty("${NS}originalLanguage")

    @JvmField
    val harvestEndpoint = m.createProperty("${NS}harvestEndpoint")

    @JvmField
    val isMachineReadable = m.createProperty("${NS}isMachineReadable")
    @JvmField
    val isNonProprietary = m.createProperty("${NS}isNonProprietary")

    @JvmField
    val visibility = m.createProperty("${NS}visibility")

    @JvmField
    val hidden = m.createResource("${NS}hidden")

}
