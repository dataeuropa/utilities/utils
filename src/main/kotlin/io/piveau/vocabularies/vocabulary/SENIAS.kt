package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property

object SENIAS {
    const val NS = "http://senias.de/ns/sis#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val deadline: Property = m.createProperty(NS, "deadline")

}
