package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource

object PV {
    const val NS = "https://piveau.eu/ns/voc#"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val originId: Property = m.createProperty(NS, "originalId")

    // DQV

    // Dimensions
    @JvmField
    val accessibility: Resource = m.createResource("${NS}accessibility")

    @JvmField
    val contextuality: Resource = m.createResource("${NS}contextuality")

    @JvmField
    val findability: Resource = m.createResource("${NS}findability")

    @JvmField
    val interoperability: Resource = m.createResource("${NS}interoperability")

    @JvmField
    val reusability: Resource = m.createResource("${NS}reusability")

    // Metrics
    @JvmField
    val accessRightsAvailability: Resource = m.createResource("${NS}accessRightsAvailability")

    @JvmField
    val accessRightsVocabularyAlignment: Resource = m.createResource("${NS}accessRightsVocabularyAlignment")

    @JvmField
    val accessUrlStatusCode: Resource = m.createResource("${NS}accessUrlStatusCode")

    @JvmField
    val byteSizeAvailability: Resource = m.createResource("${NS}byteSizeAvailability")

    @JvmField
    val categoryAvailability: Resource = m.createResource("${NS}categoryAvailability")

    @JvmField
    val contactPointAvailability: Resource = m.createResource("${NS}contactPointAvailability")

    @JvmField
    val dateIssuedAvailability: Resource = m.createResource("${NS}dateIssuedAvailability")

    @JvmField
    val dateModifiedAvailability: Resource = m.createResource("${NS}dateModifiedAvailability")

    @JvmField
    val dcatApCompliance: Resource = m.createResource("${NS}dcatApCompliance")

    @JvmField
    val downloadUrlAvailability: Resource = m.createResource("${NS}downloadUrlAvailability")

    @JvmField
    val downloadUrlStatusCode: Resource = m.createResource("${NS}downloadUrlStatusCode")

    @JvmField
    val formatAvailability: Resource = m.createResource("${NS}formatAvailability")

    @JvmField
    val formatMatch: Resource = m.createResource("${NS}formatMatch")

    @JvmField
    val formatMediaTypeMachineInterpretable: Resource = m.createResource("${NS}formatMediaTypeMachineInterpretable")

    @JvmField
    val formatMediaTypeNonProprietary: Resource = m.createResource("${NS}formatMediaTypeNonProprietary")

    @JvmField
    val formatMediaTypeVocabularyAlignment: Resource = m.createResource("${NS}formatMediaTypeVocabularyAlignment")

    @JvmField
    val keywordAvailability: Resource = m.createResource("${NS}keywordAvailability")

    @JvmField
    val knownLicence: Resource = m.createResource("${NS}knownLicence")

    @JvmField
    val licenceAvailability: Resource = m.createResource("${NS}licenceAvailability")

    @JvmField
    val mediaTypeAvailability: Resource = m.createResource("${NS}mediaTypeAvailability")

    @JvmField
    val publisherAvailability: Resource = m.createResource("${NS}publisherAvailability")

    @JvmField
    val rightsAvailability: Resource = m.createResource("${NS}rightsAvailability")

    @JvmField
    val spatialAvailability: Resource = m.createResource("${NS}spatialAvailability")

    @JvmField
    val syntaxValid: Resource = m.createResource("${NS}syntaxValid")

    @JvmField
    val temporalAvailability: Resource = m.createResource("${NS}temporalAvailability")

    @JvmField
    val accessibilityScoring: Resource = m.createResource("${NS}accessibilityScoring")

    @JvmField
    val findabilityScoring: Resource = m.createResource("${NS}findabilityScoring")

    @JvmField
    val interoperabilityScoring: Resource = m.createResource("${NS}interoperabilityScoring")

    @JvmField
    val reusabilityScoring: Resource = m.createResource("${NS}reusabilityScoring")

    @JvmField
    val contextualityScoring: Resource = m.createResource("${NS}contextualityScoring")

    @JvmField
    val scoring: Resource = m.createResource("${NS}scoring")

    @JvmField
    val scored: Resource = m.createResource("${NS}scored")

    @JvmField
    val atLeastFourStars: Resource = m.createResource("${NS}atLeastFourStars")

    @JvmField
    val trueScore: Property = m.createProperty(NS, "trueScore")

    @JvmField
    val falseScore: Property = m.createProperty(NS, "falseScore")

    // Five star
    @JvmField
    val zeroStars: Resource = m.createResource("${NS}zeroStars")

    @JvmField
    val oneStar: Resource = m.createResource("${NS}oneStar")

    @JvmField
    val twoStars: Resource = m.createResource("${NS}twoStars")

    @JvmField
    val threeStars: Resource = m.createResource("${NS}threeStars")

    @JvmField
    val fourStars: Resource = m.createResource("${NS}fourStars")

    @JvmField
    val fiveStars: Resource = m.createResource("${NS}fiveStars")

    //CSV validations

    //errors
    @JvmField
    val dateFormat: Resource = m.createResource("${NS}dateFormat")

    @JvmField
    val numberFormat: Resource = m.createResource("${NS}numberFormat")

    @JvmField
    val delimiter: Resource = m.createResource("${NS}delimiter")

    @JvmField
    val nullValues: Resource = m.createResource("${NS}nullValues")

    @JvmField
    val wrongContentType: Resource = m.createResource("${NS}wrongContentType")

    @JvmField
    val raggedRows: Resource = m.createResource("${NS}raggedRows")

    @JvmField
    val blankRows: Resource = m.createResource("${NS}blankRows")

    @JvmField
    val invalidEncoding: Resource = m.createResource("${NS}invalidEncoding")

    @JvmField
    val notFound: Resource = m.createResource("${NS}notFound")

    @JvmField
    val strayQuote: Resource = m.createResource("${NS}strayQuote")

    @JvmField
    val whitespace: Resource = m.createResource("${NS}whitespace")

    @JvmField
    val lineBreaks: Resource = m.createResource("${NS}lineBreaks")

    //warnings
    @JvmField
    val noEncoding: Resource = m.createResource("${NS}noEncoding")

    @JvmField
    val encoding: Resource = m.createResource("${NS}encoding")

    @JvmField
    val noContentType: Resource = m.createResource("${NS}noContentType")

    @JvmField
    val excel: Resource = m.createResource("${NS}excel")

    @JvmField
    val checkOptions: Resource = m.createResource("${NS}checkOptions")

    @JvmField
    val inconsistentValues: Resource = m.createResource("${NS}inconsistentValues")

    @JvmField
    val emptyColumnName: Resource = m.createResource("${NS}emptyColumnName")

    @JvmField
    val duplicateColumnName: Resource = m.createResource("${NS}duplicateColumnName")

    @JvmField
    val titleRow: Resource = m.createResource("${NS}titleRow")

    //info
    @JvmField
    val nonrfcLineBreaks: Resource = m.createResource("${NS}nonrfcLineBreaks")

    @JvmField
    val assumedHeader: Resource = m.createResource("${NS}assumedHeader")

    @JvmField
    val passed: Property = m.createProperty(NS, "passed")

    @JvmField
    val rowNumber: Property = m.createProperty(NS, "rowNumber")

    @JvmField
    val columnNumber: Property = m.createProperty(NS, "columnNumber")

    @JvmField
    val csvIndicator: Resource = m.createResource("${NS}CsvIndicator")

    @JvmField
    val csvError: Resource = m.createResource("${NS}CsvError")

    @JvmField
    val csvWarning: Resource = m.createResource("${NS}CsvWarning")

    @JvmField
    val csvInformation: Resource = m.createResource("${NS}CsvInformation")

    //mapping
    @JvmField
    val mappingClass: Property = m.createProperty(NS, "mappingClass")

    @JvmField
    val mappingName: Property = m.createProperty(NS, "mappingName")

    @JvmField
    val mappingProperty: Property = m.createProperty(NS, "mappingProperty")

    @JvmField
    val mappingLink: Property = m.createProperty(NS, "mappingLink")


    //Catalogue descriptions
    @JvmField
    val catalogueBackground: Property = m.createProperty(NS, "catalogueBackground")

    @JvmField
    val catalogueProfileImage: Property = m.createProperty(NS, "catalogueProfileImage")

    @JvmField
    val catalogueFavIcon: Property = m.createProperty(NS, "catalogueFavIcon")

    @JvmField
    val catalogueLogo: Property = m.createProperty(NS, "catalogueLogo")

    @JvmField
    val catalogueInterestingDatasets: Property = m.createProperty(NS, "catalogueInterestingDatasets")

    @JvmField
    val visualisationSelection: Property = m.createProperty(NS, "visualisationSelection")

    @JvmField
    val visualisationData: Property = m.createProperty(NS, "visualisationData")

    @JvmField
    val openDataPresence: Property = m.createProperty(NS, "openDataPresence")


    // access control fields

    @JvmField
    val publication_status: Property = m.createProperty(NS, "publicationStatus")

    @JvmField
    val draft: Resource = m.createResource("${NS}Draft")

    @JvmField
    val published: Resource = m.createResource("${NS}Published")

    @JvmField
    val access_level: Property = m.createProperty(NS, "accessLevel")

    @JvmField
    val public: Resource = m.createResource("${NS}public")

    @JvmField
    val restricted: Resource = m.createResource("${NS}restricted")

    @JvmField
    val isOpenDataPresence: Property = m.createProperty(NS, "isOpenDataPresence")
}
