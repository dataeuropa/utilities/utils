package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource

object STATDCATAP {
    const val NS = "http://data.europa.eu/s1n/"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val attribute = m.createProperty(NS, "attribute")

    @JvmField
    val dimension = m.createProperty(NS, "dimension")

    @JvmField
    val numSeries = m.createProperty(NS, "numSeries")

    @JvmField
    val hasQualityAnnotation = m.createProperty(NS, "hasQualityAnnotation")

    @JvmField
    val statUnitMeasure = m.createProperty(NS, "statUnitMeasure")

}
