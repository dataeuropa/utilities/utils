package io.piveau.vocabularies.vocabulary

import org.apache.jena.rdf.model.ModelFactory

object DCATAP {
    const val NS = "http://data.europa.eu/r5r/"
    private val m = ModelFactory.createDefaultModel()

    @JvmField
    val availability = m.createProperty(NS, "availability")

    // SHACL
    @JvmField
    val CatalogShape = m.createResource("${NS}Catalog_Shape")

    @JvmField
    val DatasetShape = m.createResource("${NS}Dataset_Shape")
}
