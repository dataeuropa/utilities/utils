@file:JvmName("ConceptSchemes")

package io.piveau.vocabularies

import io.piveau.vocabularies.vocabulary.*
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.predicate.ResponsePredicate
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.*
import org.apache.jena.ontology.Individual
import org.apache.jena.ontology.OntDocumentManager
import org.apache.jena.ontology.OntModel
import org.apache.jena.ontology.OntModelSpec
import org.apache.jena.rdf.model.InfModel
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.reasoner.ReasonerFactory
import org.apache.jena.reasoner.ReasonerRegistry
import org.apache.jena.reasoner.rulesys.RDFSRuleReasonerFactory
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFParser
import org.apache.jena.vocabulary.*
import org.slf4j.LoggerFactory
import java.io.ByteArrayInputStream
import java.io.InputStream

lateinit var client: WebClient
lateinit var vertxApp: Vertx

fun initRemotes(vertx: Vertx, remotes: Boolean = false, prefetch: Boolean = true): Future<Void> {
        vertxApp = vertx
        client = WebClient.create(vertx)
        if (remotes) {
            enableRemotes()
        }
        return vertx.executeBlocking { promise ->
            val log = LoggerFactory.getLogger("io.piveau.vocabularies.ConceptSchemes")
            if (prefetch) {
                log.info("Loading ${if (remotes) "from remote" else ""}...")
                DataTheme.ontology.also { log.debug("Data themes loaded") }
                FileType.ontology.also { log.debug("File types loaded") }
                AccessRight.ontology.also { log.debug("Access rights loaded") }
                CorporateBodies.ontology.also { log.debug("Corporate bodies loaded") }
                Languages.ontology.also { log.debug("Languages loaded") }
                License.ontology.also { log.debug("Licenses loaded") }
                Countries.ontology.also { log.debug("Countries loaded") }
                Continents.ontology.also { log.debug("Continents loaded") }
                Places.ontology.also { log.debug("Places loaded") }
                Frequency.ontology.also { log.debug("Frequencies loaded") }
                PlannedAvailability.ontology.also { log.debug("Planned availabilities loaded") }
                DatasetType.ontology.also { log.debug("Dataset type loaded") }
                DatasetStatus.ontology.also { log.debug("Dataset status loaded") }
                DistributionType.ontology.also { log.debug("Distribution type loaded") }
                DocumentationType.ontology.also { log.debug("Documentation type loaded") }
                EuroVoc.ontology.also { log.debug("EuroVoc loaded") }
            }
            log.info("Loading done")
            promise.complete()
        }
    }

@Deprecated("Name is ambiguous", ReplaceWith("initRemotes(vertx, remotes, prefetch)"))
fun init(vertx: Vertx, config: JsonObject = JsonObject()) =
    initRemotes(vertx, config.getBoolean("enableRemotes", false))

@Deprecated("Use initRemotes", ReplaceWith("initRemotes(vertx)"))
fun initVocabularies(vertx: Vertx, config: JsonObject = JsonObject()) {
    initRemotes(vertx, config.getBoolean("enableRemotes", false), config.getBoolean("prefetch", true))
}

class Concept(val resource: Resource, val scheme: ConceptScheme) {
    val identifier: String =
        resource.getProperty(DC_11.identifier)?.literal?.string ?: (resource.uri?.substringAfterLast("/") ?: "")

    val exactMatch: String = resource.getProperty(SKOS.exactMatch)?.resource?.uri ?: ""

    val prefLabels: Map<String, String>
        get() = resource.listProperties(SKOS.prefLabel).asSequence()
            .associate { it.language to it.literal.lexicalForm }

    val individual: Resource?
        get() = scheme.getConcept(resource)?.resource

    val altLabels: Map<String, String>
        get() = resource.listProperties(SKOS.altLabel).asSequence()
            .associate { it.language to it.literal.lexicalForm }

    fun label(lang: String): String? = prefLabel(lang) ?: altLabel(lang)

    fun prefLabel(lang: String): String? = resource.getProperty(SKOS.prefLabel, lang)?.literal?.string
        ?: resource.getProperty(SKOS.prefLabel)?.literal?.lexicalForm

    fun altLabel(lang: String): String? = resource.getProperty(SKOS.altLabel, lang)?.literal?.string
        ?: resource.getProperty(SKOS.altLabel)?.literal?.lexicalForm
}

internal val skosSchema: Model = RDFDataMgr.loadModel("https://www.w3.org/2009/08/skos-reference/skos.rdf").apply {
    RDFDataMgr.read(this, "https://www.w3.org/2009/08/skos-reference/skos-xl.rdf")
}

internal val reasoner = ReasonerRegistry.getRDFSSimpleReasoner().apply {
    setParameter(ReasonerVocabulary.PROPsetRDFSLevel, ReasonerVocabulary.RDFS_SIMPLE)
}

open class ConceptScheme(val uriRef: String, resource: String, remote: String? = null) {

    val ontology: InfModel by lazy {
        runBlocking {
            val conceptModel = ModelFactory.createDefaultModel().apply {
                remote?.let { fetchVocabulary(remote, resource) } ?: readXmlResource(resource)
            }
            ModelFactory.createInfModel(reasoner, skosSchema, conceptModel)
//            ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF).apply {
//                remote?.let { fetchVocabulary(remote, resource) } ?: readXmlResource(resource)
//                OntDocumentManager.getInstance().loadImport(this, "https://www.w3.org/2009/08/skos-reference/skos.rdf")
//                OntDocumentManager.getInstance().loadImport(this, "https://www.w3.org/2009/08/skos-reference/skos-xl.rdf")
//            }
        }
    }

    open val id: String
        get() = uriRef.substringAfterLast("/")

    protected val scheme: Resource
        get() = ontology.getResource(uriRef)

    val concepts: List<Concept>
        get() = ontology.listSubjectsWithProperty(RDF.type, SKOS.Concept)
            .mapWith { Concept(it.asResource(), this) }
            .toList()

    fun containsConcept(resource: Resource) = getConcept(resource) != null

    fun getConcept(resource: Resource): Concept? {
        return if (resource.isURIResource) {
            val ind = ontology.getResource(resource.uri)
            if (ind?.hasProperty(RDF.type, SKOS.Concept) == true) Concept(ind, this) else null
        } else null
    }

}

var dataThemeRemote: String? = null
var fileTypeRemote: String? = null
var accessRightRemote: String? = null
var corporateBodiesRemote: String? = null
var languagesRemote: String? = null
var licenseRemote: String? = null
var countriesRemote: String? = null
var continentsRemote: String? = null
var placesRemote: String? = null
var frequencyRemote: String? = null
var datasetTypeRemote: String? = null
var datasetStatusRemote: String? = null
var distributionTypeRemote: String? = null
var documentationTypeRemote: String? = null
var euroVocRemote: String? = null
var plannedAvailabilityRemote: String? = null

// Disabled remotes misses some required infos. E.g., the remote version of countries does not contain iso codes
fun enableRemotes() {
    dataThemeRemote = "http://publications.europa.eu/resource/distribution/data-theme/rdf/skos_core/data-theme-skos.rdf"
    fileTypeRemote = "http://publications.europa.eu/resource/distribution/file-type/rdf/skos_core/filetypes-skos.rdf"
    accessRightRemote =
        "http://publications.europa.eu/resource/distribution/access-right/rdf/skos_core/access-right-skos.rdf"
    corporateBodiesRemote =
        "http://publications.europa.eu/resource/distribution/corporate-body/rdf/skos_ap_act/corporatebodies-skos-ap-act.rdf"
    languagesRemote = "http://publications.europa.eu/resource/distribution/language/rdf/skos_core/languages-skos.rdf"
    licenseRemote = "http://publications.europa.eu/resource/distribution/licence/rdf/skos_core/licences-skos.rdf"
//    countriesRemote =
//        "http://publications.europa.eu/resource/distribution/country/rdf/skos_ap_act/countries-skos-ap-act.rdf"
    continentsRemote =
        "http://publications.europa.eu/resource/distribution/continent/rdf/skos_ap_act/continents-skos-ap-act.rdf"
    placesRemote = "http://publications.europa.eu/resource/distribution/place/rdf/skos_ap_act/places-skos-ap-act.rdf"
    frequencyRemote = "http://publications.europa.eu/resource/distribution/frequency/rdf/skos_core/frequencies-skos.rdf"
    datasetTypeRemote =
        "http://publications.europa.eu/resource/distribution/dataset-type/rdf/skos_core/dataset-types-skos.rdf"
    datasetStatusRemote =
        "http://publications.europa.eu/resource/distribution/dataset-status/rdf/skos_core/datasetstatus-skos.rdf"
    plannedAvailabilityRemote =
        "http://publications.europa.eu/resource/distribution/planned-availability/rdf/skos_core/planned-availability-skos.rdf"
    distributionTypeRemote =
        "http://publications.europa.eu/resource/distribution/distribution-type/rdf/skos_core/distribution-types-skos.rdf"
    documentationTypeRemote =
        "http://publications.europa.eu/resource/distribution/documentation-type/rdf/skos_core/documentation-types-skos.rdf"
//    euroVocRemote = "http://publications.europa.eu/resource/distribution/eurovoc/rdf/skos_ap_act/eurovoc-skos-ap-act.rdf"

    enableIanaRemotes()
}

object EuroVoc : AuthorityTable(
    "http://publications.europa.eu/resource/authority/eurovoc",
    "eurovoc-skos-ap-act-stripped.rdf",
    euroVocRemote
)

object DataTheme :
    AuthorityTable("http://publications.europa.eu/resource/authority/data-theme", "data-theme-skos.rdf", dataThemeRemote)

object FileType :
    AuthorityTable("http://publications.europa.eu/resource/authority/file-type", "filetypes-skos.rdf", fileTypeRemote) {
    init {
        ontology.readXmlResource("machine-readable-format.rdf")
        ontology.readXmlResource("non-proprietary-format.rdf")
        ontology.readXmlResource("piveau-filetypes-skos.rdf")
    }

    @JvmStatic
    fun isMachineReadable(fileType: Concept): Boolean =
        fileType.resource.getProperty(EDP.isMachineReadable)?.boolean ?: false

    @JvmStatic
    fun isNonProprietary(fileType: Concept): Boolean =
        fileType.resource.getProperty(EDP.isNonProprietary)?.boolean ?: false
}

object DatasetType : AuthorityTable(
    "http://publications.europa.eu/resource/authority/dataset-type",
    "dataset-types-skos.rdf",
    datasetTypeRemote
)

object DatasetStatus : AuthorityTable(
    "http://publications.europa.eu/resource/authority/dataset-status",
    "datasetstatus-skos.rdf",
    datasetStatusRemote
)

object DistributionType : AuthorityTable(
    "http://publications.europa.eu/resource/authority/distribution-type",
    "distribution-types-skos.rdf",
    distributionTypeRemote
)

object DocumentationType : AuthorityTable(
    "http://publications.europa.eu/resource/authority/documentation-type",
    "documentation-types-skos.rdf",
    documentationTypeRemote
)

object AccessRight : AuthorityTable(
    "http://publications.europa.eu/resource/authority/access-right",
    "access-right-skos.rdf",
    accessRightRemote
)

object CorporateBodies : AuthorityTable(
    "http://publications.europa.eu/resource/authority/corporate-body",
    "corporatebodies-skos-ap-act.rdf",
    corporateBodiesRemote
)

object PlannedAvailability : AuthorityTable(
    "http://publications.europa.eu/resource/authority/planned-availability",
    "planned-availability-skos.rdf",
    plannedAvailabilityRemote
)

object Frequency :
    AuthorityTable("http://publications.europa.eu/resource/authority/frequency", "frequencies-skos.rdf", frequencyRemote)

object ADMSAssetType : ConceptScheme(ADMS.NS_SCHEME_ASSET_TYPE, "ADMS_SKOS_v1.00.rdf")
object ADMSStatus : ConceptScheme(ADMS.NS_SCHEME_STATUS, "ADMS_SKOS_v1.00.rdf")

object Countries : AuthorityTable(
    "http://publications.europa.eu/resource/authority/country",
    "countries-skos-ap-act.rdf",
    countriesRemote
) {
    init {
        ontology.readXmlResource("piveau-countries-skos.rdf")
    }

    private val iso31661alpha2Resource =
        ontology.createResource("http://publications.europa.eu/resource/authority/notation-type/ISO_3166_1_ALPHA_2")

    fun iso31661alpha2(concept: Concept): String? {
        var isoAlpha2Code: String? = null
        concept.resource.listProperties(EUVOC.xlNotation).forEachRemaining {
            it.resource?.getPropertyResourceValue(DCTerms.type)?.run {
                if (this == iso31661alpha2Resource) {
                    isoAlpha2Code = it.getProperty(EUVOC.xlCodification).string.lowercase()
                }
            }
        }
        return isoAlpha2Code
    }

}

object Continents : AuthorityTable(
    "http://publications.europa.eu/resource/authority/continent",
    "continents-skos.rdf",
    continentsRemote
) {
    init {
        ontology.readXmlResource("piveau-continents-skos.rdf")
    }
}

object Places :
    AuthorityTable("http://publications.europa.eu/resource/authority/place", "places-skos-ap-act.rdf", placesRemote) {
    fun countryOf(concept: Concept): Concept? =
        concept.resource.getProperty(GEOSPARQL.sfWithin)?.let {
            getConcept(it.resource)
        }
}

object Languages :
    AuthorityTable("http://publications.europa.eu/resource/authority/language", "languages-skos.rdf", languagesRemote) {
    fun iso6391Code(language: Concept): String? =
        ontology.listObjectsOfProperty(language.resource, AT.opMappedCode).toList().find {
            it.asResource()?.getProperty(DC_11.source)?.`object`?.asLiteral()?.string == "iso-639-1"
        }?.asResource()?.getProperty(AT.legacyCode)?.`object`?.asLiteral()?.string

    fun tedCode(language: Concept): String? =
        ontology.listObjectsOfProperty(language.resource, AT.opMappedCode).toList().find {
            it.asResource()?.getProperty(DC_11.source)?.`object`?.asLiteral()?.string == "TED"
        }?.asResource()?.getProperty(AT.legacyCode)?.`object`?.asLiteral()?.string
}

object License :
    AuthorityTable("http://publications.europa.eu/resource/authority/licence", "licences-skos.rdf", licenseRemote) {
    init {
        ontology.readXmlResource("piveau-licences-skos.rdf")
    }

    fun exactMatch(value: String): Concept? =
        ontology.listSubjectsWithProperty(SKOS.exactMatch, ontology.createResource(value)).nextOptional().orElse(null)?.let {
            Concept(it, this)
        }

    fun altLabel(value: String, lang: String = "en"): Concept? =
        ontology.listSubjectsWithProperty(SKOS.altLabel, value, lang).nextOptional().orElse(null)?.let {
            Concept(it, this)
        }

    fun licenseAssistant(concept: Concept): String? =
        concept.resource.getProperty(EDP.licensingAssistant)?.resource?.uri

    fun isOpen(concept: Concept): Boolean =
        concept.resource.getProperty(OSI.isOpen)?.boolean ?: false
}

object PiveauScoring : ConceptScheme("https://piveau.eu/ns/scoring#scoring", "piveau-scoring-skos.rdf") {
    fun abstract(score: Int): Resource = when (score) {
        in 0..119 -> ontology.getResource(PS.bad.uri)
        in 120..220 -> ontology.getResource(PS.sufficient.uri)
        in 221..350 -> ontology.getResource(PS.good.uri)
        else -> ontology.getResource(PS.excellent.uri)
    }
}

suspend fun Model.fetchVocabulary(uri: String, resource: String) {
    val sendPromise = Promise.promise<HttpResponse<Buffer>>()
    client.getAbs(uri).expect(ResponsePredicate.SC_OK).send(sendPromise)
    sendPromise.future().coAwait()
    if (sendPromise.future().failed()) {
        removeAll()
        readXmlResource(resource)
        return
    } else {
        RDFParser.create()
            .source(ByteArrayInputStream(sendPromise.future().result().bodyAsString().toByteArray()))
            .lang(Lang.RDFXML)
            .parse(this@fetchVocabulary)
    }

    val futures = mutableListOf<Future<Void>>()

    val iterator = listSubjectsWithProperty(SKOS.inScheme, createResource(uri))
    while (iterator.hasNext()) {
        val promise = Promise.promise<Void>()
        client.getAbs(iterator.nextResource().uri).expect(ResponsePredicate.SC_OK).send {
            if (it.succeeded()) {
                RDFParser.create()
                    .source(ByteArrayInputStream(it.result().bodyAsString().toByteArray()))
                    .lang(Lang.RDFXML)
                    .parse(this@fetchVocabulary)
                promise.complete()
            } else {
                promise.fail(it.cause())
            }
        }
        futures.add(promise.future())
    }
    Future.all(futures.toList()).onComplete {
        if (it.failed()) {
            removeAll()
            readXmlResource(resource)
        }
    }.coAwait()
}

fun Model.readTurtleResource(resource: String): Model =
    apply { RDFDataMgr.read(this, resource.loadResource(), Lang.TURTLE) }

fun Model.readXmlResource(resource: String): Model =
    apply { RDFDataMgr.read(this, resource.loadResource(), Lang.RDFXML) }

fun String.loadResource(): InputStream? = object {}.javaClass.classLoader.getResourceAsStream(this)

fun Resource.isA(resource: Resource): Boolean = listProperties(RDF.type).toSet().any { it.`object` == resource }
