package io.piveau.dcatap

import io.piveau.rdf.asNormalized
import io.vertx.core.json.JsonObject
import org.apache.jena.query.Dataset
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory

class DCATAPUriRef internal constructor(val id: String, private val context: String, private val graphContext: String) {

    val uri
        get() = when {
            context.isBlank() -> throw IllegalStateException("No explicit context set")
            else -> DCATAPUriSchema.baseUri + context + id
        }

    val uriRef: URIRef
        get() = uri

    val resource: Resource = when {
        context.isBlank() -> ResourceFactory.createResource()
        else -> ResourceFactory.createResource(uri)
    }

    val graphName
        get() = when {
            graphContext.isBlank() -> throw IllegalStateException("No explicit graph name context set")
            else -> DCATAPUriSchema.baseUri + graphContext + id
        }

    val graphNameRef: URIRef
        get() = graphName

    val datasetGraphName: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.datasetGraphContext + id

    val catalogueGraphName: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.catalogueGraphContext + id

    val metricsGraphName: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.metricsGraphContext + id

    val historicMetricsGraphName: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.historicMetricsGraphContext + id

    val datasetUriRef: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.datasetContext + id

    val distributionUriRef: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.distributionContext + id

    val recordUriRef: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.recordContext + id

    val catalogueUriRef: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.catalogueContext + id

    val metricsUriRef: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.metricsContext + id

    val historicMetricsUriRef: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.historicMetricsContext + id

    val measurementUriRef: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.measurementContext + id

    val annotationUriRef: String
        get() = DCATAPUriSchema.baseUri + DCATAPUriSchema.annotationContext + id

    fun isValid() = context.isNotBlank()

    fun inModel(model: Model): Resource = resource.inModel(model)

    override fun equals(other: Any?): Boolean = (other is DCATAPUriRef) && uri == other.uri
    override fun hashCode(): Int = uri.hashCode()

}

object DCATAPUriSchema {

    const val DEFAULT_BASE_URI = "https://piveau.io/"

    @JvmStatic
    var config: JsonObject? = null

    @JvmStatic
    val baseUri: String by lazy { config?.getString("baseUri", DEFAULT_BASE_URI) ?: DEFAULT_BASE_URI }

    @JvmStatic
    val datasetContext: String by lazy { config?.getString("datasetContext", "set/data/") ?: "set/data/" }

    @JvmStatic
    val distributionContext: String by lazy {
        config?.getString("distributionContext", "set/distribution/") ?: "set/distribution/"
    }

    @JvmStatic
    val recordContext: String by lazy { config?.getString("recordContext", "set/record/") ?: "set/record/" }

    @JvmStatic
    val catalogueContext: String by lazy { config?.getString("catalogueContext", "id/catalogue/") ?: "id/catalogue/" }

    @JvmStatic
    val metricsContext by lazy { config?.getString("metricsContext", "id/metrics/") ?: "id/metrics/" }

    @JvmStatic
    val measurementContext by lazy { config?.getString("measurementContext", "id/measurement/") ?: "id/measurement/" }

    @JvmStatic
    val annotationContext by lazy { config?.getString("annotationContext", "id/annotation/") ?: "id/annotation/" }

    @JvmStatic
    val historicMetricsContext by lazy {
        config?.getString("historyMetricsContext", "id/historic_metrics/") ?: "id/historic_metrics/"
    }

    @JvmStatic
    val datasetGraphContext: String
        get() = datasetContext

    @JvmStatic
    val catalogueGraphContext: String
        get() = catalogueContext

    @JvmStatic
    val metricsGraphContext: String
        get() = metricsContext

    @JvmStatic
    val historicMetricsGraphContext: String
        get() = historicMetricsContext

    @JvmStatic
    @JvmOverloads
    fun applyFor(id: String, context: String = ""): DCATAPUriRef =
        createFor(id.asNormalized(), context)

    @JvmStatic
    @JvmOverloads
    fun createFor(id: String, context: String = ""): DCATAPUriRef =
        DCATAPUriRef(id, context, context)

    @JvmStatic
    fun createForCatalogue(id: String): DCATAPUriRef =
        DCATAPUriRef(id, catalogueContext, catalogueGraphContext)

    @JvmStatic
    fun createForDataset(id: String): DCATAPUriRef =
        DCATAPUriRef(id, datasetContext, datasetGraphContext)

    @JvmStatic
    fun createForAnnotation(id: String, datasetID: String): DCATAPUriRef =
        DCATAPUriRef("$datasetID/$id", annotationContext, metricsGraphContext)

    @JvmStatic
    fun createForMeasurement(id: String, datasetID: String): DCATAPUriRef =
        DCATAPUriRef("$datasetID/$id", measurementContext, metricsGraphContext)

    @JvmStatic
    fun parseUriRef(uriRef: String): DCATAPUriRef {
        return when {
            isCatalogueUriRef(uriRef) -> DCATAPUriRef(
                uriRef.substringAfter(catalogueContext),
                catalogueContext,
                catalogueGraphContext
            )

            isDatasetUriRef(uriRef) -> DCATAPUriRef(
                uriRef.substringAfter(datasetContext),
                datasetContext,
                datasetGraphContext
            )

            isRecordUriRef(uriRef) -> DCATAPUriRef(
                uriRef.substringAfter(recordContext),
                recordContext,
                datasetGraphContext
            )

            isDistributionUriRef(uriRef) -> DCATAPUriRef(
                uriRef.substringAfter(distributionContext),
                distributionContext,
                datasetGraphContext
            )

            isMetricsUriRef(uriRef) -> DCATAPUriRef(
                uriRef.substringAfter(metricsContext),
                metricsContext,
                metricsGraphContext
            )

            isHistoricMetricsUriRef(uriRef) -> DCATAPUriRef(
                uriRef.substringAfter(historicMetricsContext),
                historicMetricsContext,
                historicMetricsGraphContext
            )

            isAnnotationUriRef(uriRef) -> DCATAPUriRef(
                uriRef.substringAfter(annotationContext),
                annotationContext,
                metricsGraphContext
            )

            isMeasurementUriRef(uriRef) -> DCATAPUriRef(
                uriRef.substringAfter(measurementContext),
                measurementContext,
                metricsGraphContext
            )

            else -> {
                DCATAPUriRef("", "", "")
            }
        }
    }

    @JvmStatic
    fun isCatalogueUriRef(uriRef: String) = uriRef.startsWith(baseUri + catalogueContext)

    @JvmStatic
    fun isDatasetUriRef(uriRef: String) = uriRef.startsWith(baseUri + datasetContext)

    @JvmStatic
    fun isRecordUriRef(uriRef: String) = uriRef.startsWith(baseUri + recordContext)

    @JvmStatic
    fun isDistributionUriRef(uriRef: String) = uriRef.startsWith(baseUri + distributionContext)

    @JvmStatic
    fun isMetricsUriRef(uriRef: String) = uriRef.startsWith(baseUri + metricsContext)

    @JvmStatic
    fun isHistoricMetricsUriRef(uriRef: String) = uriRef.startsWith(baseUri + historicMetricsContext)

    @JvmStatic
    fun isMeasurementUriRef(uriRef: String) = uriRef.startsWith(baseUri + measurementContext)

    @JvmStatic
    fun isAnnotationUriRef(uriRef: String) = uriRef.startsWith(baseUri + annotationContext)

}

fun Model.dcatapResource(uriRef: DCATAPUriRef) = when {
    containsResource(uriRef.resource) -> createResource(uriRef.uri)
    else -> null
}

fun Dataset.dcatapGraph(uriRef: DCATAPUriRef) = when {
    containsNamedModel(uriRef.graphName) -> getNamedModel(uriRef.graphName)
    else -> null
}
