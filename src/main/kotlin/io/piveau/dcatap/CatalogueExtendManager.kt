package io.piveau.dcatap

import io.piveau.security.PermissionManager.ACCESS_LEVEL
import io.piveau.vocabularies.vocabulary.PV
import io.vertx.core.Future
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.flow.*
import org.apache.jena.rdf.model.Model
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.RDF
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicReference

class CatalogueExtendManager internal constructor(
    private val tripleStore: TripleStore,
    private val catalogueManager: CatalogueManager
) {

    val log = LoggerFactory.getLogger(javaClass)

    fun exist(catalogueId: String) = catalogueManager.exist(catalogueId)

    fun ensureExists(catalogueId: String): Future<Unit> = catalogueManager.ensureExists(catalogueId)

    fun isProtected(catalogueId: String): Future<Boolean> = DCATAPUriSchema.createForCatalogue(catalogueId).let {
        tripleStore.ask("ASK WHERE { GRAPH <${it.graphNameRef}> { <${it.graphNameRef}> <${PV.public}>  \"false\"^^<http://www.w3.org/2001/XMLSchema#boolean>.} }")
    }

    fun listUris(
        restricted: FilterType = FilterType.INCLUDE,
        allowed: List<String> = listOf()
    ): Future<List<Pair<DCATAPUriRef, ACCESS_LEVEL>>> {

        val restrictedQuery = when (restricted) {
            FilterType.INCLUDE -> "OPTIONAL { ?g <${PV.public}> ?r. }"
            FilterType.EXCLUDE -> " ?g <${PV.public}> \"true\"^^<http://www.w3.org/2001/XMLSchema#boolean>. "
            FilterType.ONLY -> " ?g <${PV.public}> \"false\"^^<http://www.w3.org/2001/XMLSchema#boolean>. "
        }

        if (restricted == FilterType.INCLUDE) {
            return tripleStore.select("SELECT DISTINCT ?g ?r WHERE { GRAPH ?g { ?g a <${DCAT.Catalog}>. $restrictedQuery } }")
                .map { resultSet ->
                    resultSet.asSequence().map {
                        if (it.get("r") == null || !it.get("r").isLiteral) {

                            Pair(
                                DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                                ACCESS_LEVEL.PUBLIC
                            )
                        } else {
                            Pair(
                                DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                                if (it.getLiteral("r").boolean) ACCESS_LEVEL.PUBLIC else ACCESS_LEVEL.RESTRICTED
                            )
                        }
                    }
                        .filter { it.first.isValid() }
                        .filter { it.second == ACCESS_LEVEL.PUBLIC || allowed.contains(it.first.id) }
                        .toList()
                }
        } else {
            return tripleStore.select("SELECT DISTINCT ?g WHERE { GRAPH ?g { ?g a <${DCAT.Catalog}>. $restrictedQuery } }")
                .map { resultSet ->
                    resultSet.asSequence().map {
                        Pair(
                            DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                            if (restricted == FilterType.EXCLUDE) ACCESS_LEVEL.PUBLIC else ACCESS_LEVEL.RESTRICTED
                        )
                    }
                        .filter { it.first.isValid() }
                        .filter { it.second == ACCESS_LEVEL.PUBLIC || allowed.contains(it.first.id) }
                        .toList()
                }
        }

    }

    @JvmOverloads
    fun list(
        offset: Int = 0, limit: Int = 100,
        restricted: FilterType = FilterType.INCLUDE,
        allowed: List<String> = listOf()
    ): Future<List<Model>> {
        val futuresReference = AtomicReference<List<Future<Model>>>()


        val restrictedQuery = when (restricted) {
            FilterType.INCLUDE -> "OPTIONAL { ?g <${PV.access_level}> ?r. }"
            FilterType.EXCLUDE -> " ?g <${PV.access_level}> <${PV.public}>. "
            FilterType.ONLY -> " ?g <${PV.access_level}> <${PV.restricted}>. "
        }

        // Take care. If more than max order by limit catalogues exists, this query will not work (usually 10.000)
        return tripleStore.select("SELECT DISTINCT ?g WHERE { GRAPH ?g { ?g a <${DCAT.Catalog}>. $restrictedQuery } } ORDER BY ?g OFFSET $offset LIMIT $limit")
            .compose { result ->
                val futures = result.asSequence()
                    .map {
                        if (it.get("r") == null || !it.get("r").isLiteral) {

                            Pair(
                                DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                                if (restricted == FilterType.ONLY) ACCESS_LEVEL.RESTRICTED else ACCESS_LEVEL.PUBLIC
                            )
                        } else {
                            Pair(
                                DCATAPUriSchema.parseUriRef(it.getResource("g").uri),
                                if (it.getLiteral("r").boolean) ACCESS_LEVEL.PUBLIC else ACCESS_LEVEL.RESTRICTED
                            )
                        }
                    }
                    .filter { it.first.isValid() }
                    .filter { it.second == ACCESS_LEVEL.PUBLIC || allowed.contains(it.first.id) }
                    .map { getStrippedGraph(it.first.uriRef) }
                    .toList()
                futuresReference.set(futures)
                Future.join(futures)
            }
            .map { it.list<Model>().filterNotNull() }
            .otherwise {
                futuresReference.get().filter { it.succeeded() }.map { it.result() }
            }
    }

    fun subCatalogues(
        catalogueId: String,
        restricted: FilterType = FilterType.INCLUDE
    ): Future<List<DCATAPUriRef>> {
        val schema = DCATAPUriSchema.createForCatalogue(catalogueId)

        val restrictedQuery = when (restricted) {
            FilterType.INCLUDE -> ""
            FilterType.EXCLUDE -> " GRAPH ?sc { ?sc  <${PV.access_level}> <${PV.public}>. }  "
            FilterType.ONLY -> " GRAPH ?sc { ?sc  <${PV.access_level}> <${PV.restricted}>. } "
        }

        return tripleStore.select("SELECT ?sc WHERE { GRAPH <${schema.graphNameRef}> { <${schema.uriRef}> <${DCTerms.hasPart}> ?sc } $restrictedQuery")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("sc").uri) }
                    .filter { it.isValid() }
                    .toList()
            }
    }

    fun allRecords(
        catalogueId: String,
        restricted: FilterType = FilterType.INCLUDE,
        drafts: FilterType = FilterType.EXCLUDE,
        allowed: List<String> = listOf()
    ): Future<List<DCATAPUriRef>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query =
            "SELECT ?r WHERE { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.record}> ?r } }"

        return tripleStore.recursiveSelect(query, 0, 5000, mutableListOf())
            .map { list ->
                list.flatMap { it.asSequence() }
                    .map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("r").uri) }
                    .filter { it.isValid() }
            }
    }

    @JvmOverloads
    fun records(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<DCATAPUriRef>> =
        catalogueManager.records(catalogueId, offset, limit)

    fun allDatasets(catalogueId: String): Future<List<DCATAPUriRef>> = catalogueManager.allDatasets(catalogueId)

    fun countDatasets(catalogueId: String): Future<Int> = catalogueManager.countDatasets(catalogueId)

    @JvmOverloads
    fun datasets(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<DCATAPUriRef>> =
        catalogueManager.datasets(catalogueId, offset, limit)

    @JvmOverloads
    fun listDatasets(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<Model>> =
        catalogueManager.listDatasets(catalogueId, offset, limit)

    fun addDatasetEntry(
        catalogueGraphName: String,
        catalogueUriRef: String,
        datasetUriRef: String,
        recordUriRef: String
    ): Future<Unit> {
        return tripleStore.update(
            "INSERT DATA { GRAPH <$catalogueGraphName> { <$catalogueUriRef> <http://www.w3.org/ns/dcat#record> <$recordUriRef> ; <http://www.w3.org/ns/dcat#dataset> <$datasetUriRef> . } }"
        )
    }

    fun removeDatasetEntry(catalogueUriRef: URIRef, datasetUriRef: URIRef): Future<Unit> {
        val catalogueDCATAPUriRef = DCATAPUriSchema.parseUriRef(catalogueUriRef)
        val datasetDCATAPUriRef = DCATAPUriSchema.parseUriRef(datasetUriRef)
        return tripleStore.update(
            "DELETE DATA { GRAPH <${catalogueDCATAPUriRef.graphNameRef}> { <$catalogueUriRef> <http://www.w3.org/ns/dcat#record> <${datasetDCATAPUriRef.recordUriRef}> ; <http://www.w3.org/ns/dcat#dataset> <$datasetUriRef> . } }"
        )
    }

    fun allDatasetIdentifiers(catalogueId: String): Future<List<String>> =
        catalogueManager.allDatasetIdentifiers(catalogueId)

    fun datasetIdentifiers(catalogueId: String, offset: Int = 0, limit: Int = 100): Future<List<String>> =
        catalogueManager.datasetIdentifiers(catalogueId, offset, limit)

    fun delete(catalogueId: String) = deleteGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun deleteGraph(name: URIRef) = tripleStore.deleteGraph(name)

    fun get(catalogueId: String) = getGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun getRecordsStripped(catalogueId: String): Future<Model> =
        getStrippedGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun getRecordsStrippedGraph(name: URIRef) =
        tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o MINUS { ?s <${DCAT.record}> ?o } } }")

    fun getStripped(catalogueId: String): Future<Model> =
        getStrippedGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef)

    fun getStrippedGraph(name: URIRef) =
        tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o MINUS { ?s <${DCAT.dataset}> ?o} MINUS { ?s <${DCAT.record}> ?o } } }")

    fun getGraph(name: URIRef) = tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o } }")

    fun set(catalogueId: String, model: Model) =
        setGraph(DCATAPUriSchema.createForCatalogue(catalogueId).graphNameRef, model)

    fun setGraph(name: URIRef, model: Model) = tripleStore.setGraph(name, model, false)

    fun datasetsAsFlow(catalogueId: String): Flow<Pair<DCATAPUriRef, Model>> = flow {
        emitAll(
            allDatasets(catalogueId).coAwait()
                .asFlow()
                .transform {
                    try {
                        val model = tripleStore.datasetManager.getGraph(it.graphNameRef).coAwait()

                        if (model.contains(it.resource, RDF.type, DCAT.Dataset)) {
                            emit(it to model)
                        } else {
                            log.error("Named graph for ${it.uriRef} does not contain a corresponding dcat:Dataset resource")
                        }
                    } catch (e: Exception) {
                        log.error("Fetching model", e)
                    }
                }
        )
    }

}
