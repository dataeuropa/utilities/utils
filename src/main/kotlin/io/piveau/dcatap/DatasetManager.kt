package io.piveau.dcatap

import io.piveau.vocabularies.vocabulary.EDP
import io.vertx.core.Future
import org.apache.jena.rdf.model.*
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import java.util.concurrent.atomic.AtomicReference

const val DATASET_NOT_FOUND = "Dataset Not Found"
const val DISTRIBUTION_NOT_FOUND = "Distribution Not Found"
const val CATALOGUE_NOT_FOUND = "Catalogue Not Found"

class DatasetManager internal constructor(private val tripleStore: TripleStore) {

    fun exist(uriRef: DCATAPUriRef) =
        tripleStore.ask("ASK WHERE { GRAPH <${uriRef.graphNameRef}> { <${uriRef.uriRef}> a <${DCAT.Dataset}> } }")

    fun ensureExists(uriRef: DCATAPUriRef): Future<Unit> = exist(uriRef)
        .compose {
            if (it) Future.succeededFuture() else Future.failedFuture(TripleStoreException(404, DATASET_NOT_FOUND))
        }

    fun existGraph(graphName: URIRef): Future<Boolean> =
        tripleStore.ask("ASK WHERE { GRAPH <$graphName> { ?s ?p ?o } }")

    @JvmOverloads
    fun list(offset: Int = 0, limit: Int = 100): Future<List<DCATAPUriRef>> =
        tripleStore.select("SELECT ?dataset WHERE { GRAPH ?catalogue { ?cat a <${DCAT.Catalog}> ; <${DCAT.dataset}> ?dataset FILTER NOT EXISTS { ?cat <${EDP.visibility}> <${EDP.hidden}> } } } OFFSET $offset LIMIT $limit")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> DCATAPUriSchema.parseUriRef(qs.getResource("dataset").uri) }
                    .filter { it.isValid() }
                    .toList()
            }

    fun count(): Future<Int> =
        tripleStore.select("SELECT (count(?dataset) AS ?count) WHERE { GRAPH ?catalogue { ?cat a <${DCAT.Catalog}> ; <${DCAT.dataset}> ?dataset FILTER NOT EXISTS { ?cat <${EDP.visibility}> <${EDP.hidden}> } } }")
            .map { resultSet ->
                resultSet.asSequence().map { qs -> qs.getLiteral("count").int }
                    .toList().first()
            }

    @JvmOverloads
    fun listModels(offset: Int = 0, limit: Int = 100): Future<List<Model>> {
        val futuresReference = AtomicReference<List<Future<Model>>>()
        return tripleStore.select("SELECT ?dataset WHERE { GRAPH ?catalogue { ?cat a <${DCAT.Catalog}> ; <${DCAT.dataset}> ?dataset FILTER NOT EXISTS { ?cat <${EDP.visibility}> <${EDP.hidden}> } } } OFFSET $offset LIMIT $limit")
            .compose { result ->
                val futures = result.asSequence()
                    .map { DCATAPUriSchema.parseUriRef(it.getResource("dataset").uri) }
                    .filter { it.isValid() }
                    .map { getGraph(it.uriRef) }
                    .toList()
                futuresReference.set(futures)
                Future.join(futures)
            }
            .map { it.list<Model>().filterNotNull() }
            .otherwise {
                futuresReference.get().filter { it.succeeded() }.map { it.result() }
            }
    }

    fun identify(originId: String, catalogueId: String): Future<Pair<Resource, Resource>> {
        val catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId)
        val query = """
                SELECT ?dataset ?record
                WHERE
                {
                    GRAPH <${catalogueUriRef.graphNameRef}> 
                    {
                        <${catalogueUriRef.uriRef}> <${DCAT.dataset}> ?dataset 
                    } 
                    
                    GRAPH ?dataset
                    {
                        ?record <${DCTerms.identifier}> "$originId" ; 
                                <${FOAF.primaryTopic}> ?dataset 
                    }
                }
                """

        return tripleStore.select(query)
            .map {
                if (it.hasNext()) {
                    it.next().run {
                        Pair<Resource, Resource>(getResource("dataset"), getResource("record"))
                    }
                } else {
                    throw TripleStoreException(404, DATASET_NOT_FOUND)
                }
            }
    }

    fun catalogue(datasetUriRef: URIRef): Future<Resource> {
        val query = "SELECT ?catalogue WHERE { GRAPH ?catalogue { ?catalogue <${DCAT.dataset}> <$datasetUriRef> } }"

        return tripleStore.select(query)
            .map {
                if (it.hasNext()) {
                    it.next().getResource("catalogue")
                } else {
                    throw TripleStoreException(404, CATALOGUE_NOT_FOUND)
                }
            }
    }

    fun originIdentifier(datasetUriRef: URIRef): Future<String> =
        tripleStore.select("SELECT ?identifier WHERE { GRAPH <$datasetUriRef> { ?r a <${DCAT.CatalogRecord}> ; <${DCTerms.identifier}> ?identifier } }")
            .map {
                if (it.hasNext()) {
                    it.next().getLiteral("identifier").lexicalForm
                } else {
                    throw TripleStoreException(404, DATASET_NOT_FOUND)
                }
            }

    fun distributions(datasetUriRef: URIRef): Future<List<DCATAPUriRef>> =
        tripleStore.select("SELECT ?dist WHERE { GRAPH ?g { <$datasetUriRef> <${DCAT.distribution}> ?dist } }")
            .map { resultSet ->
                val result = mutableListOf<DCATAPUriRef>()
                while (resultSet.hasNext()) {
                    val uriRef = DCATAPUriSchema.parseUriRef(resultSet.next().getResource("dist").uri)
                    if (uriRef.isValid()) {
                        result.add(uriRef)
                    }
                }
                result
            }

    fun identifyDistribution(distributionUriRef: URIRef): Future<DCATAPUriRef> =
        tripleStore.select("SELECT ?dataset WHERE { GRAPH ?g { ?dataset a <${DCAT.Dataset}> ; <${DCAT.distribution}> <$distributionUriRef> } }")
            .map {
                if (it.hasNext()) {
                    val uriRef = DCATAPUriSchema.parseUriRef(it.next().getResource("dataset").uri)
                    if (uriRef.isValid()) {
                        uriRef
                    } else {
                        throw TripleStoreException(404, DISTRIBUTION_NOT_FOUND)
                    }
                } else {
                    throw TripleStoreException(404, DISTRIBUTION_NOT_FOUND)
                }
            }

    fun delete(originId: String, catalogueId: String): Future<Unit> =
        identify(originId, catalogueId).compose {
            tripleStore.deleteGraph(it.first.uri).mapEmpty()
        }

    fun deleteGraph(name: URIRef) = tripleStore.deleteGraph(name)

    fun get(originId: String, catalogueId: String): Future<Model> =
        identify(originId, catalogueId).compose {
            getGraph(DCATAPUriSchema.parseUriRef(it.first.uri).graphNameRef)
        }

    fun getGraph(name: URIRef) =
        tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o } }")

    fun set(originId: String, catalogueId: String, model: Model): Future<String> =
        Future.failedFuture(NotImplementedError())

    fun setGraph(name: URIRef, model: Model, clearGeoData: Boolean): Future<String> =
        tripleStore.setGraph(name, model, clearGeoData)

}