package io.piveau.dcatap

import org.apache.jena.rdf.model.Model

class MetricsManager internal constructor(private val tripleStore: TripleStore) {

    fun getGraph(name: URIRef) = tripleStore.construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$name> { ?s ?p ?o } }")

    fun setGraph(name: URIRef, model: Model) = tripleStore.setGraph(name, model, false)

}
