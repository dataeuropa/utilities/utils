@file:JvmName(name = "LanguageTag")
package io.piveau.dcatap

// returns triple <machine-translated, actual-lang, original-lang>
fun parseLangTag(tag: String, default: String): Triple<Boolean, String, String?> {
    val elements = tag.split("-".toRegex(), 5)
    return when (elements.size) {
        0 -> Triple(false, default, null)
        1, 2 -> Triple(false, elements[0].ifBlank { default }, null)
        3 -> Triple(false, elements[0].ifBlank { default }, elements[2])
        else -> Triple(elements[3] == "t0", elements[0].ifBlank { default }, elements[2])
    }
}
