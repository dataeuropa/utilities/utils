package io.piveau.dcatap

import io.piveau.json.putIfNotEmpty
import io.piveau.json.putIfNotBlank
import io.piveau.rdf.*
import io.piveau.vocabularies.*
import io.piveau.vocabularies.vocabulary.EDP
import io.vertx.core.json.JsonObject
import org.apache.jena.datatypes.BaseDatatype
import org.apache.jena.datatypes.RDFDatatype
import org.apache.jena.datatypes.xsd.XSDDatatype
import org.apache.jena.rdf.model.*
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.*
import javax.xml.datatype.Duration

object StandardVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): String? = null

    override fun visitLiteral(literal: Literal): String? {
        val type = literal.datatypeURI
        return if (type == XSDDatatype.XSDduration.uri) {
            literal.lexicalForm
            /*
            ToDo
            We need convert the Duration string into a valid ISO 8601. Before we need to clarify how the
            frontend parses this kind of string.

            val dur = java.time.Duration.ofSeconds(literal.lexicalForm.toDouble().toLong())
            */
        } else {
            val value = literal.value
            when (value) {
                is BaseDatatype.TypedValue -> (literal.value as BaseDatatype.TypedValue).lexicalValue
                else -> literal.lexicalForm
            }
        }
    }

    override fun visitURI(resource: Resource, uriRef: String): String = uriRef
}

object StandardTypedVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any? = null
    override fun visitLiteral(literal: Literal): Any? = when (literal.value) {
        is BaseDatatype.TypedValue -> (literal.value as BaseDatatype.TypedValue).lexicalValue
        else -> literal.value
    }

    override fun visitURI(resource: Resource, uriRef: String): Any = uriRef
}

object LabeledVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): String? = label(resource)
    override fun visitLiteral(literal: Literal): String? = literal.lexicalForm
    override fun visitURI(resource: Resource, uriRef: String): String? = label(resource)

    private fun label(resource: Resource): String? = resource.listProperties().toList().firstOrNull {
        listOf(
            SKOS.prefLabel,
            SKOS.altLabel,
            RDFS.label
        ).contains(it.predicate)
    }?.`object`?.asLiteral()?.lexicalForm
}

object DocumentVisitor : RDFVisitor {
    val schemaUrlProperty = ModelFactory.createDefaultModel().createProperty("http://schema.org/url")

    override fun visitBlank(resource: Resource, id: AnonId): Any? =
        resource.getProperty(schemaUrlProperty)?.literal?.lexicalForm?.let {
            JsonObject().putIfNotBlank("resource", it).putIfNotBlank("label", label(resource))
        }

    override fun visitLiteral(literal: Literal): Any =
        JsonObject().putIfNotBlank("resource", literal.lexicalForm)

    override fun visitURI(resource: Resource, uriRef: String): Any =
        JsonObject().putIfNotBlank("resource", uriRef).putIfNotBlank("label", label(resource))

    private fun label(resource: Resource): String? = resource.listProperties().toList().firstOrNull {
        listOf(
            FOAF.name,
            DCTerms.title,
            RDFS.label
        ).contains(it.predicate)
    }?.`object`?.asLiteral()?.lexicalForm
}

object ConformsToVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any =
        JsonObject().putIfNotBlank("label", resource.visitWith(LabeledVisitor) as String?)

    override fun visitLiteral(literal: Literal): Any = JsonObject().putIfNotBlank("label", literal.lexicalForm)

    override fun visitURI(resource: Resource, uriRef: String): Any =
        JsonObject().put("resource", uriRef).putIfNotBlank("label", resource.visitWith(LabeledVisitor) as String?)
}

object VCARDVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any =
        resource.getProperty(VCARD4.value)?.string ?: resource.getProperty(VCARD4.hasValue)?.resource?.uri ?: ""

    override fun visitLiteral(literal: Literal): Any = literal.value
    override fun visitURI(resource: Resource, uriRef: String): Any = uriRef
}

object VCARDAddressVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): String = readAddress(resource)
    override fun visitLiteral(literal: Literal): String = literal.lexicalForm
    override fun visitURI(resource: Resource, uriRef: String): String = readAddress(resource)

    private fun readAddress(resource: Resource): String {
        val result : MutableList<String> = mutableListOf()

        resource.getProperty(VCARD4.street_address)?.`object`?.visitWith(StandardVisitor)?.let { result.add(it.toString()) }
        resource.getProperty(VCARD4.locality)?.`object`?.visitWith(StandardVisitor)?.let { result.add(it.toString()) }
        resource.getProperty(VCARD4.postal_code)?.`object`?.visitWith(StandardVisitor)?.let { result.add(it.toString()) }
        resource.getProperty(VCARD4.country_name)?.`object`?.visitWith(StandardVisitor)?.let { result.add(it.toString()) }

        return result.joinToString(", ")
    }

}

object VCARDValueVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): String =
        resource.getProperty(VCARD4.hasValue)?.resource?.uri ?: ""
    override fun visitLiteral(literal: Literal): String = literal.lexicalForm
    override fun visitURI(resource: Resource, uriRef: String): String =
        resource.getProperty(VCARD4.hasValue)?.resource?.uri ?: ""
}

object ThemeVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any = JsonObject()

    override fun visitLiteral(literal: Literal): Any = DataTheme.findConcept(literal.lexicalForm.uppercase())?.let {
        JsonObject()
            .putIfNotBlank("id", it.identifier)
            .putIfNotEmpty("label", JsonObject().putIfNotBlank("en", it.label("en")))
            .putIfNotBlank("resource", it.resource.uri)
    } ?: JsonObject()

    override fun visitURI(resource: Resource, uriRef: String): Any = DataTheme.getConcept(resource)?.let {
        JsonObject()
            .putIfNotBlank("id", it.identifier)
            .putIfNotEmpty("label", JsonObject().putIfNotBlank("en", it.label("en")))
            .putIfNotBlank("resource", it.resource.uri)
    } ?: JsonObject()
}

object SkosVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any = JsonObject()
    override fun visitLiteral(literal: Literal): Any = JsonObject()
    override fun visitURI(resource: Resource, uriRef: String): Any =
        JsonObject()
            .putIfNotBlank("id", normalize(uriRef))
            .putIfNotEmpty("label", JsonObject().putIfNotBlank("en", uriRef))
            .putIfNotBlank("resource", uriRef)
}

object ProvenanceVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any =
        JsonObject().putIfNotBlank("label", resource.visitWith(LabeledVisitor) as String?)

    override fun visitLiteral(literal: Literal): Any = JsonObject().putIfNotBlank("label", literal.lexicalForm)

    override fun visitURI(resource: Resource, uriRef: String): Any =
        JsonObject().putIfNotBlank("resource", uriRef).putIfNotBlank("label", resource.visitWith(LabeledVisitor) as String?)
}

object MediaTypeVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any? = when {
        resource.isA(resource.model.createResource("${DCTerms.NS}IMT")) -> {
            resource.getProperty(RDF.value)?.`object`?.asLiteral()?.lexicalForm ?: ""
        }
        else -> ""
    }

    override fun visitLiteral(literal: Literal): Any? = when (literal.value) {
        is BaseDatatype.TypedValue -> (literal.value as BaseDatatype.TypedValue).lexicalValue
        else -> literal.value
    }

    override fun visitURI(resource: Resource, uriRef: String): Any? {
        return if (IanaType.isMediaType(resource)) {
            IanaType.getMediaType(resource)
        } else {
            uriRef
        }
    }
}

object FormatVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any =
        JsonObject().apply {
            resource.visitWith(LabeledVisitor)?.let { it as String
                putIfNotBlank("id", it.uppercase())
                putIfNotBlank("label", it.uppercase())
            }
        }

    override fun visitLiteral(literal: Literal): Any = FileType.findConcept(literal.lexicalForm.uppercase())?.let {
        JsonObject().putIfNotBlank("id", it.identifier).putIfNotBlank("label", it.label("en")).putIfNotBlank("resource", it.resource.uri)
    } ?: JsonObject().putIfNotBlank("id", literal.lexicalForm).putIfNotBlank("label", literal.lexicalForm)

    override fun visitURI(resource: Resource, uriRef: String): Any = FileType.getConcept(resource)?.let {
        JsonObject().putIfNotBlank("id", it.identifier).putIfNotBlank("label", it.label("en")).putIfNotBlank("resource", it.resource.uri)
    } ?: FileType.findConcept(uriRef.substringAfterLast('/', uriRef).uppercase())?.let {
        JsonObject().putIfNotBlank("id", it.identifier).putIfNotBlank("label", it.label("en")).putIfNotBlank("resource", it.resource.uri)
    } ?: JsonObject()
}

object LicenseVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any = JsonObject().apply {
        resource.listProperties().forEachRemaining { (_, predicate, obj) ->
            when (predicate) {
                DCTerms.identifier -> putIfNotBlank("id", obj.visitWith(StandardVisitor) as String?)
                DC_11.identifier -> putIfNotBlank("id", obj.visitWith(StandardVisitor) as String?)
                SKOS.prefLabel -> putIfNotBlank("description", obj.visitWith(StandardVisitor) as String?)
                SKOS.altLabel -> putIfNotBlank("label", obj.visitWith(StandardVisitor) as String?)
                DCTerms.title -> putIfNotBlank("label", obj.visitWith(StandardVisitor) as String?)
                SKOS.exactMatch -> putIfNotBlank("resource", obj.visitWith(StandardVisitor) as String?)
                EDP.licensingAssistant -> putIfNotBlank("la_url", obj.visitWith(StandardVisitor) as String?)
            }
        }
    }

    override fun visitLiteral(literal: Literal): Any =
        License.exactMatch(literal.lexicalForm)?.let {
            JsonObject()
                .putIfNotBlank("id", it.identifier)
                .putIfNotBlank("label", it.label("en"))
                .putIfNotBlank("description", it.label("en"))
                .putIfNotBlank("resource", it.exactMatch)
                .putIfNotBlank("la_url", License.licenseAssistant(it))
        } ?: License.findConcept(literal.lexicalForm.uppercase().run {
            // may be it's better to use altLabel directly?
            replace("-", "").replace('.', '_').replace("\\s+".toRegex(), "_")
        })?.let {
            JsonObject()
                .putIfNotBlank("id", it.identifier)
                .putIfNotBlank("label", it.label("en"))
                .putIfNotBlank("description", it.label("en"))
                .putIfNotBlank("resource", it.exactMatch)
                .putIfNotBlank("la_url", License.licenseAssistant(it))
        } ?: JsonObject()
            .putIfNotBlank("id", literal.lexicalForm.asNormalized())
            .putIfNotBlank("label", literal.lexicalForm)
            .putIfNotBlank("description", literal.lexicalForm)

    override fun visitURI(resource: Resource, uriRef: String): Any = License.getConcept(resource)?.let {
        JsonObject()
            .putIfNotBlank("id", it.identifier)
            .putIfNotBlank("label", it.label("en"))
            .putIfNotBlank("description", it.label("en"))
            .putIfNotBlank("resource", it.exactMatch)
            .putIfNotBlank("la_url", License.licenseAssistant(it))
    } ?: License.exactMatch(uriRef)?.let {
        JsonObject()
            .putIfNotBlank("id", it.identifier)
            .putIfNotBlank("label", it.label("en"))
            .putIfNotBlank("description", it.label("en"))
            .putIfNotBlank("resource", it.exactMatch)
            .putIfNotBlank("la_url", License.licenseAssistant(it))
    } ?: JsonObject()
        .putIfNotBlank("resource", uriRef)
        .putIfNotBlank("id", uriRef)
        .putIfNotBlank("label", uriRef)
}

object LanguageVisitor : RDFVisitor {
    override fun visitBlank(resource: Resource, id: AnonId): Any? =
        resource.visitWith(LabeledVisitor)?.let {
            Languages.findConcept(it as String)?.let { concept ->
                JsonObject()
                    .putIfNotBlank("id", Languages.iso6391Code(concept))
                    .putIfNotBlank("label", concept.label("en"))
                    .putIfNotBlank("resource", concept.resource.uri)
            }
        }

    override fun visitLiteral(literal: Literal): Any? =
        Languages.findConcept(literal.lexicalForm.uppercase())?.let {
            JsonObject()
                .putIfNotBlank("id", Languages.iso6391Code(it))
                .putIfNotBlank("label", it.label("en"))
                .putIfNotBlank("resource", it.resource.uri)
        } ?: JsonObject()
            .putIfNotBlank("id", literal.lexicalForm)
            .putIfNotBlank("label", literal.lexicalForm)

    override fun visitURI(resource: Resource, uriRef: String): Any? =
        Languages.getConcept(resource)?.let {
            JsonObject()
                .putIfNotBlank("id", Languages.iso6391Code(it))
                .putIfNotBlank("label", it.label("en"))
                .putIfNotBlank("resource", it.resource.uri)
        } ?: Languages.findConcept(uriRef.substringAfterLast('/'))?.let {
            JsonObject()
                .putIfNotBlank("id", Languages.iso6391Code(it))
                .putIfNotBlank("label", it.label("en"))
                .putIfNotBlank("resource", it.resource.uri)
        }
}
