package io.piveau.dcatap

import io.piveau.rdf.*
import io.piveau.utils.sendBufferDigestAuth
import io.piveau.utils.sendDigestAuth
import io.vertx.circuitbreaker.CircuitBreaker
import io.vertx.circuitbreaker.CircuitBreakerOptions
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import org.apache.jena.atlas.json.JsonParseException
import org.apache.jena.query.ResultSet
import org.apache.jena.query.ResultSetFactory
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Statement
import org.apache.jena.riot.Lang
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger

typealias URIRef = String

const val NOTFOUND = "Not found"

class TripleStore @JvmOverloads constructor(
    vertx: Vertx,
    config: JsonObject = JsonObject(),
    client: WebClient? = null,
    var name: String = "triple-store-${count.getAndIncrement()}"
) {

    private val log: Logger = LoggerFactory.getLogger(javaClass)

    private val client: WebClient = client ?: WebClient.create(
        vertx, WebClientOptions()
            .setMaxPoolSize(50)
            .setMaxHeaderSize(40960)
    )

    private val breaker = CircuitBreaker.create(
        "triplestore-breaker", vertx, CircuitBreakerOptions()
            .setMaxRetries(10)
            .setTimeout(config.getLong("circuitBreakerTimeout", -1))
    ).retryPolicy { t, c ->
        if (log.isTraceEnabled) {
            log.trace("$name: Retry number $c")
            t?.let { log.trace("Breaker retry cause", it) }
        }
        c * 5000L
    }

    private val address = config.getString("address", "http://virtuoso:8890")

    private val queryEndpoint = config.getString("queryEndpoint", "/sparql")
    private val queryAuthEndpoint = config.getString("queryAuthEndpoint", "/sparql-auth")
    private val graphEndpoint = config.getString("graphEndpoint", "/sparql-graph-crud")
    private val graphAuthEndpoint = config.getString("graphAuthEndpoint", "/sparql-graph-crud-auth")

    private val chunksize = config.getInteger("chunksize", 5000)
    private val anytime = config.getInteger("anytime", 180000).toString()

    private val transferFormat = config.getString("transferFormat", "application/n-triples").asRdfLang()

    private val queryRequest = this.client.getAbs("$address$queryEndpoint")
        .putHeader(ACCEPT_HEADER, "application/sparql-results+json")

    private val graphRequest = this.client.getAbs("$address$graphEndpoint")
        .putHeader(ACCEPT_HEADER, Lang.NTRIPLES.headerString)

    private val username = config.getString("username", "dba")
    private val password = config.getString("password", "dba")

    val datasetManager by lazy { DatasetManager(this) }
    val datasetExtendManager by lazy { DatasetExtendManager(this, datasetManager) }
    val catalogueManager by lazy { CatalogueManager(this) }
    val catalogueExtendManager by lazy { CatalogueExtendManager(this, catalogueManager) }
    val metricsManager by lazy { MetricsManager(this) }

    /**
     * Get a named graph as [Model].
     * @return a [Future] providing the model of the named graph.
     */
    fun getGraph(graphName: URIRef): Future<Model> =
        construct("CONSTRUCT { ?s ?p ?o } WHERE { GRAPH <$graphName> { ?s ?p ?o } }")

    @JvmOverloads
    fun setGraph(graphName: URIRef, model: Model, clearGeoData: Boolean = false): Future<String> {
        if (clearGeoData) {
            model.listStatements()
                .filterKeep { it.`object`.isLiteral && geoFormats.contains(it.literal.datatypeURI) }
                .toList().forEach {
                    val literal =
                        model.createTypedLiteral(it.literal.lexicalForm, mapGeoDatatype(it.literal.datatypeURI))
                    model.add(it.subject, it.predicate, literal)
                    model.remove(it)
                }
        }
        if (model.size() > chunksize) model.skolemize()
        val chunks = model.listStatements().toList().chunked(chunksize).toMutableList()
        val start = chunks.removeFirst()
        return putGraph(graphName, ModelFactory.createDefaultModel().add(start))
            .compose { status ->
                if (chunks.isNotEmpty()) {
                    postChunks(graphName, chunks)
                } else {
                    Future.succeededFuture(status)
                }
            }
    }

    fun putGraph(graphName: URIRef, model: Model): Future<String> =
        breaker.execute { breakerPromise ->
            client.putAbs("$address$graphAuthEndpoint")
                .putHeader(
                    CONTENT_TYPE_HEADER,
                    transferFormat.headerString
                )
                .addQueryParam("graph", graphName)
                .addQueryParam("timeout", anytime)
                .sendBufferDigestAuth(
                    username,
                    password,
                    Buffer.buffer(model.presentAs(transferFormat))
                ).onSuccess {
                    when (it.statusCode()) {
                        503 -> breakerPromise.fail(TripleStoreException(503, it.statusMessage()))
                        else -> breakerPromise.complete(it)
                    }
                }.onFailure { cause ->
                    breakerPromise.fail(TripleStoreException(500, cause = cause))
                }
        }.map {
            when (it.statusCode()) {
                200, 204 -> "updated"
                201 -> "created"
                206 -> throw TripleStoreException(206, partialResultMessage(it))
                in 202..299 -> it.statusMessage()
                else -> throw TripleStoreException(it.statusCode(), it.statusMessage())
            }
        }

    fun postGraph(graphName: URIRef, model: Model): Future<String> =
        breaker.execute { breakerPromise ->
            client.postAbs("$address$graphAuthEndpoint")
                .putHeader(
                    CONTENT_TYPE_HEADER,
                    transferFormat.headerString
                )
                .addQueryParam("graph", graphName)
                .addQueryParam("timeout", anytime)
                .sendBufferDigestAuth(
                    username,
                    password,
                    Buffer.buffer(model.presentAs(transferFormat))
                ).onSuccess {
                    when (it.statusCode()) {
                        503 -> breakerPromise.fail(TripleStoreException(503, it.statusMessage()))
                        else -> breakerPromise.complete(it)
                    }
                }.onFailure { cause -> breakerPromise.fail(TripleStoreException(500, cause = cause)) }
        }.map {
            when (it.statusCode()) {
                200, 204 -> "updated"
                201 -> "created"
                206 -> throw TripleStoreException(206, partialResultMessage(it))
                in 202..299 -> it.statusMessage()
                else -> throw TripleStoreException(it.statusCode(), it.statusMessage())
            }
        }

    fun postChunks(graphName: URIRef, chunks: List<List<Statement>>): Future<String> {
        if (chunks.isEmpty()) {
            return Future.failedFuture("Chunk list is empty")
        }
        val model = ModelFactory.createDefaultModel().add(chunks.first())
        return postGraph(graphName, model)
            .compose {
                if (chunks.size > 1) {
                    postChunks(graphName, chunks.subList(1, chunks.size))
                } else {
                    Future.succeededFuture(it)
                }
            }
    }

    fun deleteGraph(graphName: URIRef): Future<Unit> =
        breaker.execute { breakerPromise ->
            client.deleteAbs("$address$graphAuthEndpoint")
                .addQueryParam("graph", graphName)
                .addQueryParam("timeout", anytime)
                .sendDigestAuth(
                    username,
                    password
                ).onSuccess {
                    when (it.statusCode()) {
                        503 -> breakerPromise.fail(TripleStoreException(503, it.statusMessage()))
                        else -> breakerPromise.complete(it)
                    }
                }.onFailure { cause -> breakerPromise.fail(TripleStoreException(500, cause = cause)) }
        }.map {
            when (it.statusCode()) {
                206 -> throw TripleStoreException(206, partialResultMessage(it))
                in 200..299 -> Unit
                else -> throw TripleStoreException(it.statusCode(), it.statusMessage())
            }
        }

    fun headGraph(graphName: URIRef) =
        breaker.execute { breakerPromise ->
            client.headAbs("$address$graphAuthEndpoint")
                .addQueryParam("graph", graphName)
                .addQueryParam("timeout", anytime)
                .sendDigestAuth(username, password)
                .onSuccess {
                    when (it.statusCode()) {
                        503 -> breakerPromise.fail(TripleStoreException(503, it.statusMessage()))
                        else -> breakerPromise.complete(it)
                    }
                }.onFailure { cause -> breakerPromise.fail(TripleStoreException(500, cause = cause)) }
        }.map {
            when (it.statusCode()) {
                206 -> throw TripleStoreException(206, partialResultMessage(it))
                in 200..299 -> true
                404 -> false
                else -> throw TripleStoreException(it.statusCode(), it.statusMessage())
            }
        }

    fun select(query: String): Future<ResultSet> =
        breaker.execute { breakerPromise ->
            queryRequest.copy()
                .addQueryParam("query", query)
                .addQueryParam("timeout", anytime)
//                .sendDigestAuth(username, password)
                .send()
                .onSuccess {
                    when (it.statusCode()) {
                        503 -> breakerPromise.fail(TripleStoreException(503, it.statusMessage()))
                        else -> breakerPromise.complete(it)
                    }
                }
                .onFailure { cause -> breakerPromise.fail(TripleStoreException(500, cause = cause)) }
        }.map {
            when (it.statusCode()) {
                206 -> throw TripleStoreException(206, partialResultMessage(it))
                in 200..299 -> {
                    try {
                        ResultSetFactory.fromJSON(it.body().bytes.inputStream())
                    } catch (e: JsonParseException) {
                        throw TripleStoreException(500, cause = e)
                    }
                }

                500 -> {
                    val reason = it.bodyAsString()?.let { body ->
                        if (body.isNotBlank()) {
                            log.error(body)
                            body.substringBefore("\\n")
                        } else {
                            it.statusMessage()
                        }
                    } ?: it.statusMessage()
                    throw TripleStoreException(500, reason)
                }

                else -> throw TripleStoreException(500, it.statusMessage())
            }
        }

    fun update(query: String): Future<Unit> =
        breaker.execute { breakerPromise ->
            client.postAbs("$address$queryAuthEndpoint")
                .putHeader(HttpHeaders.CONTENT_TYPE.toString(), "application/sparql-update")
                .addQueryParam("timeout", anytime)
                .sendBufferDigestAuth(
                    username,
                    password,
                    Buffer.buffer(query)
                )
                .onSuccess {
                    when (it.statusCode()) {
                        503 -> breakerPromise.fail(TripleStoreException(503, it.statusMessage()))
                        else -> breakerPromise.complete(it)
                    }
                }.onFailure { cause -> breakerPromise.fail(TripleStoreException(500, cause = cause)) }
        }.map {
            when (it.statusCode()) {
                206 -> throw TripleStoreException(206, partialResultMessage(it))
                in 200..299 -> Unit
                else -> throw TripleStoreException(it.statusCode(), it.statusMessage(), query)
            }
        }

    fun ask(query: String): Future<Boolean> =
        breaker.execute { breakerPromise ->
            queryRequest.copy()
                .putHeader(ACCEPT_HEADER, "application/sparql-results+json")
                .addQueryParam("query", query)
                .addQueryParam("timeout", anytime)
//                .sendDigestAuth(username, password)
                .send()
                .onSuccess {
                    when (it.statusCode()) {
                        503 -> breakerPromise.fail(TripleStoreException(503, it.statusMessage()))
                        else -> breakerPromise.complete(it)
                    }
                }.onFailure { cause -> breakerPromise.fail(TripleStoreException(500, cause = cause)) }
        }.map {
            when (it.statusCode()) {
                206 -> throw TripleStoreException(206, partialResultMessage(it))
                in 200..299 ->
                    it.body()?.toJsonObject()?.getBoolean("boolean") ?: throw TripleStoreException(
                        500,
                        "JSON Result does not contain the requested value",
                        query = query
                    )

                else -> throw TripleStoreException(500, it.statusMessage())
            }
        }

    fun construct(query: String): Future<Model> =
        queryRecursive(query, 0, ModelFactory.createDefaultModel(), chunksize)

    fun recursiveSelect(
        query: String,
        offset: Int,
        limit: Int,
        resultSets: MutableList<ResultSet>
    ): Future<List<ResultSet>> = select("$query OFFSET $offset LIMIT $limit")
        .compose {
            if (it.hasNext()) {
                resultSets.add(it)
                recursiveSelect(query, offset + limit, limit, resultSets)
            } else {
                Future.succeededFuture(resultSets)
            }
        }

    private fun queryRecursive(query: String, offset: Int, model: Model, limit: Int = 50000): Future<Model> =
        breaker.execute {
            client.getAbs("$address$queryEndpoint")
                .putHeader(ACCEPT_HEADER, RDFMimeTypes.NTRIPLES)
                .addQueryParam("query", "$query OFFSET $offset LIMIT $limit")
                .addQueryParam("timeout", anytime)
//                .sendDigestAuth(username, password)
                .send()
                .onSuccess { response ->
                    when (response.statusCode()) {
                        503 -> it.fail(TripleStoreException(503, response.statusMessage()))
                        else -> it.complete(response)
                    }
                }
                .onFailure(it::fail)
        }.compose {
            when (it.statusCode()) {
                206 -> throw TripleStoreException(206, partialResultMessage(it))
                in 200..299 -> {
                    val m = it.body()?.bytes?.toModel(Lang.NTRIPLES) ?: ModelFactory.createDefaultModel()
                    if (m.isEmpty) {
                        Future.succeededFuture(model)
                    } else {
                        model.add(m)
                        queryRecursive(query, offset + limit, model)
                    }
                }

                else -> throw TripleStoreException(500, it.statusMessage())
            }
        }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TripleStore) return false
        if (address != other.address) return false
        if (username != other.username) return false
        if (password != other.password) return false
        if (queryEndpoint != other.queryEndpoint) return false
        if (queryAuthEndpoint != other.queryAuthEndpoint) return false
        if (graphEndpoint != other.graphEndpoint) return false
        if (graphAuthEndpoint != other.graphAuthEndpoint) return false
        if (anytime != other.anytime) return false
        if (transferFormat != other.transferFormat) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + username.hashCode()
        result = 31 * result + password.hashCode()
        result = 31 * result + graphEndpoint.hashCode()
        result = 31 * result + graphAuthEndpoint.hashCode()
        result = 31 * result + queryEndpoint.hashCode()
        result = 31 * result + queryAuthEndpoint.hashCode()
        result = 31 * result + anytime.hashCode()
        result = 31 * result + transferFormat.hashCode()
        result = 31 * result + name.hashCode()
        return result
    }

    companion object {
        private val ACCEPT_HEADER = HttpHeaders.ACCEPT.toString()
        private val CONTENT_TYPE_HEADER = HttpHeaders.CONTENT_TYPE.toString()

        val count = AtomicInteger(0)

        private val geoFormats = listOf(
            "http://www.opengis.net/ont/geosparql#wktLiteral",
            "http://www.opengis.net/ont/geosparql#gmlLiteral",
            "http://www.openlinksw.com/schemas/virtrdf#Geometry",
            "https://www.iana.org/assignments/media-types/application/vnd.geo+json",
            "https://www.iana.org/assignments/media-types/application/geo+json"
        )
    }
}

internal fun partialResultMessage(response: HttpResponse<Buffer>) = when (response.getHeader("x-sql-state") ?: "") {
    "S1TAT" -> response.getHeader("x-sparql-anytime")
        ?: response.getHeader("x-sql-message")
        ?: response.statusMessage()

    else -> response.statusMessage()
}
