package io.piveau.vertx

import io.piveau.telemetry.PiveauTelemetry
import io.vertx.core.Launcher
import io.vertx.core.VertxOptions
import io.vertx.core.impl.launcher.commands.RunCommand
import io.vertx.tracing.opentelemetry.OpenTelemetryOptions
import java.util.jar.Manifest

class PiveauVertxLauncher : Launcher() {

    override fun beforeStartingVertx(options: VertxOptions) {
        val openTelemetry = PiveauTelemetry.init()
        options.setTracingOptions(OpenTelemetryOptions(openTelemetry))
    }

    override fun getMainVerticle(): String {
        val manifests = RunCommand::class.java.getClassLoader().getResources("META-INF/MANIFEST.MF")
        var verticle = "io.piveau.MainClass"
        manifests.asIterator().forEachRemaining { url ->
            url.openStream().use { stream ->
                Manifest(stream).mainAttributes
                    .getValue("Main-Verticle")?.let {
                        verticle = it
                        return@forEachRemaining
                    }
            }
        }
        return verticle
    }

}

fun main(args: Array<String>) = PiveauVertxLauncher().dispatch(args)
