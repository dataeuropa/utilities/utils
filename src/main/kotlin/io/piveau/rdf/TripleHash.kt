@file:JvmName(name = "TripleHash")
package io.piveau.rdf

import org.apache.commons.codec.digest.DigestUtils
import org.apache.jena.graph.Triple

/**
 * Generates a canonical hash value for the triple
 *
 * @receiver A triple as part of a graph to calculate a cumulated hash value.
 * @return Hash value as MD5 hex [String]
 */
fun Triple.hash(): String {
    val (subject, predicate, obj) = this
    return DigestUtils.md5Hex("$subject$predicate$obj")
}

/**
 * Destructuring declaration for the subject of a triple
 *
 * If subject is a blank node it returns "Magic_S"
 *
 * @return The subject of the triple as [String] or "Magic_S"
 */
operator fun Triple.component1() = if (subject.isBlank) "Magic_S" else subject.toString()
/**
 * Destructuring declaration for the predicate of a triple
 *
 * @return The predicate of the triple as [String]
 */
operator fun Triple.component2() = predicate.toString()
/**
 * Destructuring declaration for the object of a triple
 *
 * If object is a blank node it returns "Magic_O"
 *
 * @return The object of the triple as [String] or "Magic_O"
 */
operator fun Triple.component3() = if (`object`.isBlank) "Magic_O" else `object`.toString()
