package io.piveau.rdf

import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.Literal
import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.Envelope
import org.locationtech.jts.geom.GeometryFactory
import org.locationtech.jts.io.ParseException
import org.locationtech.jts.io.WKTReader
import org.locationtech.jts.io.geojson.GeoJsonWriter
import org.locationtech.jts.io.gml2.GMLReader
import org.slf4j.LoggerFactory
import org.w3c.dom.Node
import org.xml.sax.InputSource
import java.io.StringReader
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

const val geoJsonMediaType = "https://www.iana.org/assignments/media-types/application/vnd.geo+json"
const val geoVndJsonMediaType = "https://www.iana.org/assignments/media-types/application/geo+json"
const val virtRdfGeometryDatatype = "http://www.openlinksw.com/schemas/virtrdf#Geometry"
const val wktDatatype = "http://www.opengis.net/ont/geosparql#wktLiteral"
const val gmlDatatype = "http://www.opengis.net/ont/geosparql#gmlLiteral"
const val geoJsonDatatype = "http://www.opengis.net/ont/geosparql#geoJSONLiteral"

const val geoJsonMediaTypeReplacement = "https://replacement.io/assignments/media-types/application/vnd.geo+json"
const val geoVndJsonMediaTypeReplacement = "https://replacement.io/assignments/media-types/application/geo+json"
const val virtRdfGeometryDatatypeReplacement = "http://replacement.io/schemas/virtrdf#Geometry"
const val wktDatatypeReplacement = "http://replacement.io/ont/geosparql#wktLiteral"
const val gmlDatatypeReplacement = "http://replacement.io/ont/geosparql#gmlLiteral"
const val geoJsonDatatypeReplacement = "http://replacement.io/ont/geosparql#geoJSONLiteral"

private val logger = LoggerFactory.getLogger("GeoParser")

fun mapGeoDatatype(dataType: String) = when(dataType) {
    geoJsonMediaType -> geoJsonMediaTypeReplacement
    geoVndJsonMediaType -> geoVndJsonMediaTypeReplacement
    geoJsonDatatype -> geoJsonDatatypeReplacement
    virtRdfGeometryDatatype -> virtRdfGeometryDatatypeReplacement
    wktDatatype -> wktDatatypeReplacement
    gmlDatatype -> gmlDatatypeReplacement
    geoJsonMediaTypeReplacement -> geoJsonMediaType
    geoVndJsonMediaTypeReplacement -> geoVndJsonMediaType
    geoJsonDatatypeReplacement -> geoJsonDatatype
    virtRdfGeometryDatatypeReplacement -> virtRdfGeometryDatatype
    wktDatatypeReplacement -> wktDatatype
    gmlDatatypeReplacement -> gmlDatatype
    else -> dataType
}

fun parseGeoLiteral(value: Literal): JsonObject = when (value.datatypeURI) {
    geoVndJsonMediaType, geoVndJsonMediaTypeReplacement,
    geoJsonMediaType, geoJsonMediaTypeReplacement,
    geoJsonDatatype, geoJsonDatatypeReplacement -> JsonObject(value.string ?: "{}")
    virtRdfGeometryDatatype, virtRdfGeometryDatatypeReplacement,
    wktDatatype, wktDatatypeReplacement -> try {
        val geometry = WKTReader().read(value.string ?: "{}")
        JsonObject(GeoJsonWriter().apply {
            setEncodeCRS(false)
        }.write(geometry))
    } catch (e: ParseException) {
        logger.error("Parsing {}", value.lexicalForm, e)
        JsonObject()
    }
    gmlDatatype, gmlDatatypeReplacement -> {
        try {
            val geometry = GMLReader().read(value.string ?: "{}", null)
            JsonObject(GeoJsonWriter().apply {
                setEncodeCRS(false)
            }.write(geometry))
        } catch (e: Exception) {
            val builder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
            val document = builder.parse(InputSource(StringReader(value.string ?: "{}")))
            val xPath = XPathFactory.newInstance().newXPath()
            val node1 = (xPath.evaluate(
                "/Envelope/upperCorner/text()",
                document,
                XPathConstants.NODE
            ) as? Node)?.nodeValue
            val node2 = (xPath.evaluate(
                "/Envelope/lowerCorner/text()",
                document,
                XPathConstants.NODE
            ) as? Node)?.nodeValue
            if (node1 != null && node2 != null) {
                val node1Splitted =
                    node1.trim().split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val node2Splitted =
                    node2.trim().split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                /**
                 * Latitude - Second Parameter - Y
                 * Longitude - First Parameter - X
                 */
                if (node1Splitted.size > 1 && node2Splitted.size > 1) {
                    try {
                        val envelope = Envelope(
                            Coordinate(node2Splitted[1].toDouble(), node2Splitted[0].toDouble()),
                            Coordinate(node1Splitted[1].toDouble(), node1Splitted[0].toDouble())
                        )
                        val geometry = GeometryFactory().toGeometry(envelope)
                        JsonObject(GeoJsonWriter().apply {
                            setEncodeCRS(false)
                        }.write(geometry))
                    } catch (e: Exception) {
                        logger.error("Parsing {}", value.lexicalForm, e)
                        JsonObject()
                    }
                } else {
                    logger.error("Parsing {}", value.lexicalForm)
                    JsonObject()
                }
            } else {
                JsonObject()
            }
        }
    }
    else -> JsonObject().also { logger.error("Unknown data type: {}", value.datatypeURI) }
}
