@file:JvmName("Piveau")
@file:JvmMultifileClass

package io.piveau.rdf

import io.piveau.dcatap.DCATAPUriRef
import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.DCATAPUriSchema.parseUriRef
import io.piveau.dcatap.setNsPrefixesFiltered
import io.piveau.jena.ModelExtract
import io.piveau.jena.StatementBoundaryBase
import io.piveau.vocabularies.loadResource
import org.apache.jena.query.Dataset
import org.apache.jena.query.DatasetFactory
import org.apache.jena.rdf.model.*
import org.apache.jena.riot.*
import org.apache.jena.riot.system.ErrorHandler
import org.apache.jena.sparql.util.Context
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.util.ResourceUtils
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.RDF
import java.io.ByteArrayOutputStream
import java.nio.file.Path
import java.text.Normalizer


private val cxt = Context().also { ctx ->
    ctx[SysRIOT.sysRdfWriterProperties] = mapOf<String, Any>("showXmlDeclaration" to "true")
}

fun Resource.selectFrom(resources: List<Resource>): Resource? =
    listProperties(RDF.type).toList().map { it.`object`.asResource() }.firstOrNull { resources.contains(it) }

fun Resource.getIdentifierProperty(): String? = getProperty(DCTerms.identifier)?.let { stmt ->
    stmt.`object`.let {
        when {
            it.isLiteral -> it.asLiteral().lexicalForm
            it.isURIResource -> it.asResource().uri
            else -> null
        }
    }
}

@JvmOverloads
fun Resource.identify(removePrefix: Boolean = false, uriRefPrecedence: Boolean = true): String? {

    val uriRef = if (isURIResource) {
        if (removePrefix) uri.substringAfterLast("/") else uri
    } else null

    return if (uriRefPrecedence && uriRef != null) {
        uriRef
    } else {
        getIdentifierProperty() ?: uriRef
    }
}

private val stopPredicates4Datasets = listOf(
    DCAT.dataset,
    FOAF.primaryTopic,
    FOAF.isPrimaryTopicOf,
    DCTerms.source,
    DCTerms.hasVersion,
    DCTerms.isVersionOf,
    DCTerms.isPartOf,
    DCTerms.hasPart,
    DCAT.servesDataset
)

private val stopPredicates4Catalogues = listOf(
    DCAT.dataset,
    DCAT.record,
    DCAT.service,
    DCAT.catalog,
    DCTerms.hasPart,
    DCTerms.isPartOf
)

private object DatasetExtractor : ModelExtract(object : StatementBoundaryBase() {
    override fun stopAt(s: Statement): Boolean = s.predicate in stopPredicates4Datasets
})

private object CatalogueExtractor : ModelExtract(object : StatementBoundaryBase() {
    override fun stopAt(s: Statement): Boolean = s.predicate in stopPredicates4Catalogues
})

private object ModelExtractor : ModelExtract(object : StatementBoundaryBase() {
    override fun stopAt(s: Statement): Boolean = s.predicate in stopPredicates4Datasets
})

@JvmOverloads
fun Resource.extractDatasetAsModel(model: Model = this.model): Model? = DatasetExtractor.extract(this, model).apply {
    setNsPrefixesFiltered()
}

@JvmOverloads
fun Resource.extractCatalogueAsModel(model: Model = this.model): Model? =
    CatalogueExtractor.extract(this, model).apply {
        setNsPrefixesFiltered()
    }

@JvmOverloads
@Deprecated(
    "Replace with more specific function if possible and available",
    ReplaceWith("Resource.extractDatasetAsModel(model)")
)
fun Resource.extractAsModel(model: Model = this.model): Model? = ModelExtractor.extract(this, model).apply {
    setNsPrefixesFiltered()
}

fun Resource.rename(uriRef: String): Resource {
    val store = mutableMapOf<Property, String>().apply {
        model.listStatements(this@rename, null, this@rename).let {
            while (it.hasNext()) {
                it.next().apply {
                    it.remove()
                    put(predicate, `object`.asResource().uri)
                }
            }
        }
    }

    return ResourceUtils.renameResource(this, uriRef).apply {
        store.forEach { addProperty(it.key, model.createResource(it.value)) }
    }
}

fun Path.toModel(lang: Lang, errorHandler: ErrorHandler): Model = ModelFactory.createDefaultModel().apply {
    RDFParser.create().checking(false).source(this@toModel).lang(lang)
        .errorHandler(errorHandler)
        .parse(this)
}

fun Path.toModel(lang: Lang): Model = ModelFactory.createDefaultModel().apply {
    RDFParser.create().checking(false).source(this@toModel).lang(lang).parse(this)
}

fun Path.toDataset(lang: Lang, errorHandler: ErrorHandler): Dataset = DatasetFactory.create().apply {
    RDFParser.create().checking(false).source(this@toDataset).lang(lang)
        .errorHandler(errorHandler)
        .parse(this)
}

fun Path.toDataset(lang: Lang): Dataset = DatasetFactory.create().apply {
    RDFParser.create().checking(false).source(this@toDataset).lang(lang).parse(this)
}

fun ByteArray.toModel(lang: Lang, errorHandler: ErrorHandler): Model = ModelFactory.createDefaultModel().apply {
    RDFParser.create().checking(false).source(inputStream()).lang(lang)
        .errorHandler(errorHandler)
        .parse(this) // this is model
}

@JvmOverloads
fun ByteArray.toModel(lang: Lang, baseUri: String? = null): Model = ModelFactory.createDefaultModel().apply {
    val builder = RDFParser.create().checking(false).source(inputStream()).lang(lang)
    baseUri?.apply {
        builder.base(this) // this is baseUri
    }
    builder.parse(this) // this is model
}

@JvmOverloads
fun ByteArray.toModel(contentType: String, baseUri: String? = null): Model =
    toModel(contentType.asRdfLang(), baseUri)

@JvmOverloads
fun ByteArray.toDataset(lang: Lang, baseUri: String? = null): Dataset = DatasetFactory.create().apply {
    val builder: RDFParserBuilder = RDFParser.create().checking(false).source(inputStream()).lang(lang)
    baseUri?.apply {
        builder.base(this) // this is baseUri
    }
    builder.parse(this) // this is dataset
}

fun Model.findTypedSubject(subjectType: Resource): String {
    val datasets = listResourcesWithProperty(RDF.type, subjectType).toList()
    return if (datasets.isNotEmpty()) {
        val uriRef = parseUriRef(datasets[0].uri)
        if (uriRef.isValid()) {
            uriRef.uriRef
        } else {
            ""
        }
    } else {
        ""
    }
}

fun Model.findSubjectAsResource(subjectType: Resource): Resource? {
    val subject = listResourcesWithProperty(RDF.type, subjectType).toList()
    return subject.firstOrNull()
}

fun Model.findSubjectAsString(subjectType: Resource): String? = findSubjectAsResource(subjectType)?.uri

fun Model.findSubjectAsDCATAPUriRef(subjectType: Resource): DCATAPUriRef? {
    return findSubjectAsString(subjectType)?.let {
        val uriRef = parseUriRef(it)
        if (uriRef.isValid()) uriRef else null
    }
}

fun Model.findCatalogueAsResource() = findSubjectAsResource(DCAT.Catalog)
fun Model.findCatalogueAsString() = findSubjectAsString(DCAT.Catalog)
fun Model.findCatalogueAsDCATAPUriRef() = findSubjectAsDCATAPUriRef(DCAT.Catalog)

fun Model.findDatasetAsResource() = findSubjectAsResource(DCAT.Dataset)
fun Model.findDatasetAsString() = findSubjectAsString(DCAT.Dataset)
fun Model.findDatasetAsDCATAPUriRef() = findSubjectAsDCATAPUriRef(DCAT.Dataset)

fun Model.findRecordAsResource() = findSubjectAsResource(DCAT.CatalogRecord)
fun Model.findRecordAsString() = findSubjectAsString(DCAT.CatalogRecord)
fun Model.findRecordAsDCATAPUriRef() = findSubjectAsDCATAPUriRef(DCAT.CatalogRecord)

fun Model.findDistributionAsResource() = findSubjectAsResource(DCAT.Distribution)
fun Model.findDistributionAsString() = findSubjectAsString(DCAT.Distribution)
fun Model.findDistributionAsDCATAPUriRef() = findSubjectAsDCATAPUriRef(DCAT.Distribution)


fun Model.presentAsN3() = presentAs(Lang.N3)

fun Model.presentAsTurtle() = presentAs(Lang.TURTLE)

fun Model.presentAsRdfXml() = presentAs(Lang.RDFXML)

fun Model.presentAsNTriples() = presentAs(Lang.NTRIPLES)

fun Model.presentAsJsonLd() = presentAs(Lang.JSONLD)

@JvmOverloads
fun Model.presentAs(format: RDFFormat = RDFFormat.JSONLD_PRETTY): String {
    setNsPrefixesFiltered()
    ByteArrayOutputStream().use {
        RDFWriter.create().source(this).format(format).context(cxt).output(it)
        return it.toString()
    }
}

fun Model.presentAs(lang: Lang): String {
    setNsPrefixesFiltered()
    ByteArrayOutputStream().use {
        RDFWriter.create().source(this).lang(lang).context(cxt).output(it)
        return it.toString()
    }
}

fun Model.presentAs(contentType: String): String = presentAs(contentType.asRdfFormat())

@Deprecated("Name is misleading", replaceWith = ReplaceWith("Model.presentAs(lang)"))
fun Model.asString(lang: Lang): String = presentAs(lang)

@JvmOverloads
@Deprecated("Name is misleading", replaceWith = ReplaceWith("Model.presentAs(format)"))
fun Model.asString(format: RDFFormat = RDFFormat.JSONLD_PRETTY): String = presentAs(format)

@Deprecated("Name is misleading", replaceWith = ReplaceWith("Model.presentAs(contentType)"))
fun Model.asString(contentType: String): String = presentAs(contentType.asRdfFormat())

@JvmOverloads
fun Dataset.presentAs(format: RDFFormat = RDFFormat.JSONLD_PRETTY): String {
    setNsPrefixesFiltered()
    ByteArrayOutputStream().use {
        RDFWriter.create().source(this).format(format).context(cxt).output(it)
        return it.toString()
    }
}

fun Dataset.presentAs(lang: Lang): String {
    setNsPrefixesFiltered()
    ByteArrayOutputStream().use {
        RDFWriter.create().source(this).lang(lang).context(cxt).output(it)
        return it.toString()
    }
}

fun Dataset.presentAs(contentType: String): String = presentAs(contentType.asRdfFormat())

@Deprecated("Name is misleading", replaceWith = ReplaceWith("Dataset.presentAs(lang)"))
fun Dataset.asString(lang: Lang): String = presentAs(lang)

@JvmOverloads
@Deprecated("Name is misleading", replaceWith = ReplaceWith("Dataset.presentAs(format)"))
fun Dataset.asString(format: RDFFormat = RDFFormat.JSONLD_PRETTY): String = presentAs(format)

@Deprecated("Name is misleading", replaceWith = ReplaceWith("Dataset.presentAs(contentType)"))
fun Dataset.asString(contentType: String): String = presentAs(contentType.asRdfFormat())

fun Dataset.readTrigResource(resource: String): Dataset =
    apply { RDFDataMgr.read(this, resource.loadResource(), Lang.TRIG) }

@JvmOverloads
fun List<Model>.presentAs(contentType: String = RDFMimeTypes.JSONLD, subjectType: Resource = DCAT.Dataset): String =
    when {
        isQuadFormat(contentType) -> {
            val dataset = DatasetFactory.create()
            forEach { model ->
                val subject = model.findTypedSubject(subjectType)
                if (subject.isNotBlank()) {
                    dataset.addNamedModel(subject, model)
                } else {
                    dataset.defaultModel.add(model)
                }
            }
            dataset.presentAs(contentType)
        }

        else -> {
            val model = ModelFactory.createDefaultModel()
            forEach { model.add(it) }
            model.presentAs(contentType)
        }
    }

fun Model.skolemize() {
    skolemize(DCATAPUriSchema.baseUri + ".well-known/genid/")
}

fun Model.skolemize(baseUri: String) {
    skolemize { anonId -> baseUri + anonId.labelString }
}

fun Model.skolemize(skolemUriFunction: (AnonId) -> String) {
    val resIterator = listSubjects()
    while (resIterator.hasNext()) {
        val resource = resIterator.nextResource()
        if (resource != null && resource.isAnon) {
            val skolemURI: String = skolemUriFunction(resource.id)
            ResourceUtils.renameResource(resource, skolemURI)
        }
    }
}

fun String.asUnicodeNormalized(): String = Normalizer.normalize(this, Normalizer.Form.NFKD)
    .replace("%".toRegex(), "")
    .replace("/".toRegex(), "-")
    .replace("\\s".toRegex(), "-")
    .replace("-+".toRegex(), "-")
    .lowercase()

fun String.asNormalized(): String = Normalizer.normalize(this, Normalizer.Form.NFKD)
    .replace("%".toRegex(), "")
    .replace("/".toRegex(), "-")
    .replace("\\W".toRegex(), "-")
    .replace("-+".toRegex(), "-")
    .lowercase()

fun String.asRdfLang(): Lang = when (substringBefore(';', this).trim()) {
    RDFMimeTypes.RDFXML -> Lang.RDFXML
    RDFMimeTypes.JSONLD, "application/json" -> Lang.JSONLD
    RDFMimeTypes.TURTLE -> Lang.TURTLE
    RDFMimeTypes.N3 -> Lang.N3
    RDFMimeTypes.TRIG -> Lang.TRIG
    RDFMimeTypes.TRIX -> Lang.TRIX
    RDFMimeTypes.NTRIPLES -> Lang.NTRIPLES
    RDFMimeTypes.NQUADS -> Lang.NQUADS
    else -> Lang.RDFNULL
}

fun String.asRdfFormat(): RDFFormat = when (substringBefore(';', this).trim()) {
    RDFMimeTypes.RDFXML -> RDFFormat.RDFXML_PRETTY
    RDFMimeTypes.JSONLD -> RDFFormat.JSONLD_PRETTY
    RDFMimeTypes.TURTLE -> RDFFormat.TURTLE_PRETTY
    RDFMimeTypes.N3 -> RDFFormat.TRIG_PRETTY
    RDFMimeTypes.TRIG -> RDFFormat.TRIG_PRETTY
    RDFMimeTypes.TRIX -> RDFFormat.TRIX
    RDFMimeTypes.NTRIPLES -> RDFFormat.NTRIPLES_UTF8
    RDFMimeTypes.NQUADS -> RDFFormat.NQUADS_UTF8
    else -> RDFFormat.RDFNULL
}

val String.isRDF: Boolean
    get() = substringBefore(';', this).trim() in listOf(
        RDFMimeTypes.RDFXML,
        RDFMimeTypes.JSONLD,
        RDFMimeTypes.TURTLE,
        RDFMimeTypes.N3,
        RDFMimeTypes.TRIG,
        RDFMimeTypes.TRIX,
        RDFMimeTypes.NTRIPLES,
        RDFMimeTypes.NQUADS
    )

object RDFMimeTypes {
    const val RDFXML = "application/rdf+xml"
    const val JSONLD = "application/ld+json"
    const val TURTLE = "text/turtle"
    const val N3 = "text/n3"
    const val TRIG = "application/trig"
    const val TRIX = "application/trix"
    const val NTRIPLES = "application/n-triples"
    const val NQUADS = "application/n-quads"
}

fun isQuadFormat(format: String) = when (format) {
    RDFMimeTypes.JSONLD,
    RDFMimeTypes.NQUADS,
    RDFMimeTypes.TRIG,
    RDFMimeTypes.TRIX -> true

    else -> false
}

operator fun Statement.component1(): Resource = subject
operator fun Statement.component2(): Property = predicate
operator fun Statement.component3(): RDFNode = `object`

/**
 * Generates a canonical hash value for the statement
 * @return Hash value as MD5 hex [String]
 */
fun Statement.tripleHash() = asTriple().hash()
