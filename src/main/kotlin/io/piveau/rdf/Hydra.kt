package io.piveau.rdf

import io.piveau.vocabularies.vocabulary.HYDRA
import org.apache.http.client.utils.URIBuilder
import org.apache.http.client.utils.URLEncodedUtils
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.RDF
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset

fun findHydraPaging(model: Model, base: String? = null) = HydraPaging.findPaging(model, base)

class HydraPaging private constructor(
    private val paging: Resource?,
    val total: Int,
    private val base: String?
) {

    companion object {
        @JvmStatic
        @JvmOverloads
        fun findPaging(model: Model, base: String? = null): HydraPaging {
            var hydra = model.listResourcesWithProperty(RDF.type, HYDRA.PartialCollectionView)
            if (!hydra.hasNext()) {
                hydra = model.listResourcesWithProperty(RDF.type, HYDRA.PagedCollection)
            }
            return if (hydra.hasNext()) {
                val paging = hydra.next()
                HydraPaging(
                    paging,
                    paging.getProperty(HYDRA.totalItems)?.int ?: 0,
                    base
                )
            } else {
                HydraPaging(null, 0, base)
            }
        }
    }

    val next: String?
        get() =
            (paging?.getProperty(HYDRA.next) ?: paging?.getProperty(HYDRA.nextPage))?.let {
                handleBroken(
                    when (val obj = it.`object`) {
                        is Resource -> obj.uri
                        is Literal -> obj.string
                        else -> null
                    }
                )
            }

    private fun handleBroken(next: String?): String? = base?.let {
        try {
            val baseUrl = URL(base)
            val nextUrl = URL(next)
            // deal here with base
            "${baseUrl.protocol}://${baseUrl.authority}${baseUrl.path}?${nextUrl.query}"
        } catch (e: MalformedURLException) {
            next
        }
    } ?: next

}

// build Hydra rdf
class HydraCollection private constructor(
    val model: Model,
    val id: String
) {

    companion object {
        @JvmStatic
        @JvmOverloads
        fun build(
            base: String,
            total: Int,
            offset: Int = 0,
            limit: Int = 100,
            usePagedCollection: Boolean = false
        ): HydraCollection {

            val model = ModelFactory.createDefaultModel()
            val baseUrl = URL(base)

            val builder = URIBuilder()
            builder.scheme = baseUrl.protocol
            builder.host = baseUrl.host
            builder.port = baseUrl.port
            builder.path = baseUrl.path
            URLEncodedUtils.parse(baseUrl.query, Charset.forName("UTF-8")).iterator().asSequence()
                .filter { arg -> arg.name != "limit" }
                .filter { arg -> arg.name != "offset" }
                .filter { arg -> arg.name != "usePagedCollection" }
                .forEach { a -> builder.addParameter(a.name, a.value) }

            builder.addParameter("limit", limit.toString())

            // Include usePagedCollection parameter if true
            if (usePagedCollection) {
                builder.addParameter("usePagedCollection", "true")
            }

            val current = URIBuilder(builder.toString())
            current.addParameter("offset", offset.toString())

            val hydra = model.createResource(current.toString())

            if (usePagedCollection) {
                hydra.addProperty(RDF.type, HYDRA.PagedCollection)
                // Add 'previous' link for PagedCollection
//                if (offset > 0) {
//                    val prevOffset = if (offset > limit) offset - limit else 0
//                    val prev = URIBuilder(builder.toString()).addParameter("offset", prevOffset.toString())
//                    hydra.addProperty(HYDRA.previous, prev.toString())
//                }
            } else {
                hydra.addProperty(RDF.type, HYDRA.PartialCollectionView)
            }

            hydra.addLiteral(HYDRA.totalItems, total)

            if (offset + limit < total) {

                val next = URIBuilder(builder.toString()).addParameter("offset", (offset + limit).toString())
                hydra.addProperty(HYDRA.next, next.toString())
            }
            val first = URIBuilder(builder.toString()).addParameter("offset", 0.toString())
            hydra.addProperty(HYDRA.first, first.toString())

            val last = URIBuilder(builder.toString()).addParameter("offset", (total - limit).toString()).toString()
            hydra.addProperty(HYDRA.last, last)

            val prev = if (offset > 0) {
                var off = if (offset > limit) offset - limit else 0
                if (off < 0) off = 0

                URIBuilder(builder.toString()).addParameter("offset", off.toString())
            } else {
                null
            }
            if (prev != null) {
                hydra.addProperty(HYDRA.previous, prev.toString())
            }

            return HydraCollection(model, current.toString())
        }
    }
}