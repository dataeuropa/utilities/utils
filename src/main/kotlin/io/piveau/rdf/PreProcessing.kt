@file:JvmName("PreProcessing")

package io.piveau.rdf

import org.apache.jena.graph.Node
import org.apache.jena.graph.NodeFactory
import org.apache.jena.graph.Node_URI
import org.apache.jena.graph.Triple
import org.apache.jena.riot.RDFParser
import org.apache.jena.riot.system.StreamRDF
import org.apache.jena.riot.system.StreamRDFLib
import org.apache.jena.riot.system.StreamRDFWrapper
import org.eclipse.rdf4j.model.*
import org.eclipse.rdf4j.model.impl.LinkedHashModel
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.eclipse.rdf4j.rio.RDFFormat
import org.eclipse.rdf4j.rio.RDFParseException
import org.eclipse.rdf4j.rio.Rio
import org.eclipse.rdf4j.rio.helpers.BasicParserSettings
import org.eclipse.rdf4j.rio.helpers.StatementCollector
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.BufferedInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream

private val factory: ValueFactory = SimpleValueFactory.getInstance()

private val log: Logger = LoggerFactory.getLogger("io.piveau.rdf.PreProcessing")

private class URIFixer(model: Model) : StatementCollector(model) {
    override fun handleStatement(st: Statement) {
        val fixedSubject = if (st.subject is IRI) st.subject.fix() else st.subject
        val fixedObject = if (st.`object` is IRI) (st.`object` as IRI).fix() else st.`object`
        val fixedStatement = factory.createStatement(fixedSubject, st.predicate.fix(), fixedObject)
        super.handleStatement(fixedStatement)
    }
}

private val encodingMap = mapOf(
    " " to "%20",
    "<" to "%3C",
    ">" to "%3E",
    "^" to "%5E",
    "|" to "%7C",
    "`" to "%60",
    "{" to "%7B",
    "}" to "%7D",
    "\"" to "%22",
    "\\" to "%5C",
    "\r" to "%0D",
    "\n" to "%0A",
    "\t" to "%09"
)

private fun Node_URI.fix(): Node = NodeFactory.createURI(fixUri(uri))
private fun Resource.fix(): IRI = factory.createIRI(fixUri(stringValue()))
private fun IRI.fix(): IRI = factory.createIRI(fixUri(stringValue()))

private fun fixUri(uri: String): String = uri.trim()
    .removeSuffix("%")
    .replace("&#9;", "")
    .replace("&#32;", "")
    .replace("&nbsp;", "")
    .replace("""[<>^{}|`"\s\n\r\t]""".toRegex()) {
        encodingMap[it.value] ?: ""
    }

/**
 * The pre-processing tries to fix URI problems of an rdf graph.
 *
 * @param content The rdf to pre-process
 * @param mimeType The mimeType of the input stream
 * @param url The base url for parsing relative URI refs
 * @return A [Pair]<[OutputStream], [String]> containing the final output stream and its mimeType
 */
@JvmOverloads
fun preProcess(content: ByteArray, mimeType: String, baseUri: String = ""): Pair<ByteArrayOutputStream, String> =
    preProcess(content.inputStream(), ByteArrayOutputStream(), mimeType, baseUri) as Pair<ByteArrayOutputStream, String>

/**
 * The pre-processing tries to fix URI problems of a rdf graph.
 *
 * @param input The input stream of the rdf
 * @param output The preferred output stream to use for the output. If used it will be part of the returned [Pair]
 * @param mimeType The mimeType of the input stream
 * @return A [Pair]<[OutputStream], [String]> containing the final output stream and its mimeType
 */
fun preProcess(
    input: InputStream,
    output: OutputStream,
    mimeType: String,
    baseUri: String
): Pair<OutputStream, String> = when (mimeType) {
    "application/ld+json" -> jenaProcessor(input, output, mimeType, baseUri)
    else -> rdf4jProcessor(input, output, mimeType, baseUri)
}

private class FilterSink(dest: StreamRDF) : StreamRDFWrapper(dest) {

    override fun triple(triple: Triple) {
        val fixedSubject = if (triple.subject.isURI) (triple.subject as Node_URI).fix() else triple.subject
        val fixedObject = if (triple.`object`.isURI) (triple.`object` as Node_URI).fix() else triple.`object`
        val fixedTriple = Triple.create(fixedSubject, triple.predicate, fixedObject)
        super.triple(fixedTriple)
    }

}

private fun jenaProcessor(
    inputStream: InputStream,
    outputStream: OutputStream,
    mimeType: String,
    baseUri: String
): Pair<OutputStream, String> {
    val streamOut: StreamRDF = StreamRDFLib.writer(outputStream)
    val filtered = FilterSink(streamOut)

    val bufferedInput = BufferedInputStream(inputStream)

    return try {
        RDFParser.source(bufferedInput)
            .lang(mimeType.asRdfLang())
            .checking(false)
            .strict(false)
            .resolveURIs(false)
//            .base(baseUri)
            .parse(filtered)
        Pair(outputStream, RDFMimeTypes.NTRIPLES)
    } catch (e: Exception) {
        log.error("Parsing {}", mimeType, e)
        bufferedInput.reset()
        bufferedInput.transferTo(outputStream)
        Pair(outputStream, mimeType)
    }
}

private fun rdf4jProcessor(
    inputStream: InputStream,
    outputStream: OutputStream,
    mimeType: String,
    baseUri: String
): Pair<OutputStream, String> {
    val model = LinkedHashModel()
    val format = Rio.getParserFormatForMIMEType(mimeType)

    if (!format.isPresent) {
        log.warn("MimeType {} not possible for pre-processing", mimeType)
        inputStream.transferTo(outputStream)
        inputStream.close()
        return Pair(outputStream, mimeType)
    }

    val parser = Rio.createParser(format.get()).apply {
        parserConfig.set(BasicParserSettings.VERIFY_URI_SYNTAX, false)
        parserConfig.set(BasicParserSettings.VERIFY_RELATIVE_URIS, false)
        parserConfig.addNonFatalError(BasicParserSettings.VERIFY_RELATIVE_URIS)
        parserConfig.addNonFatalError(BasicParserSettings.VERIFY_URI_SYNTAX)
        setRDFHandler(URIFixer(model))
    }
    BufferedInputStream(inputStream).use {
        try {
            parser.parse(it, baseUri)
        } catch (e: RDFParseException) {
            log.error("Parsing input as {} for pre-processing", mimeType, e)
            throw e
        }
    }
    Rio.write(model, outputStream, RDFFormat.NTRIPLES)
    return Pair(outputStream, RDFMimeTypes.NTRIPLES)
}
