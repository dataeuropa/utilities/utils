@file:JvmName("PiveauSemanticConventions")
package io.piveau.telemetry

import io.opentelemetry.api.common.AttributeKey
import io.opentelemetry.api.trace.Span
import io.piveau.pipe.model.Pipe
import io.vertx.core.json.JsonObject

object PiveauSemanticAttributes {
    val PIPE_ID = AttributeKey.stringKey("piveau.pipe.id")
    val PIPE_NAME = AttributeKey.stringKey("piveau.pipe.name")
    val PIPE_TITLE = AttributeKey.stringKey("piveau.pipe.title")
    val PIPE_VERSION = AttributeKey.stringKey("piveau.pipe.version")
    val PIPE_CONTEXT = AttributeKey.stringKey("piveau.pipe.context")
    val PIPE_RUN_ID = AttributeKey.stringKey("piveau.pipe.run.id")
    val PIPE_RUN_TOTAL = AttributeKey.longKey("piveau.pipe.run.total")
    val PIPE_RUN_CURRENT = AttributeKey.longKey("piveau.pipe.run.current")

    val PIVEAU_RESOURCE_ID = AttributeKey.stringKey("piveau.resource.id")
    val PIVEAU_RESOURCE_TARGET = AttributeKey.stringKey("piveau.resource.target")
    val PIVEAU_RESOURCE_CONTENT = AttributeKey.stringKey("piveau.resource.content")
}

fun Span.setPipeAttributes(pipe: Pipe) {
    setAttribute(PiveauSemanticAttributes.PIPE_ID, pipe.header.id)
    setAttribute(PiveauSemanticAttributes.PIPE_NAME, pipe.header.name)
    setAttribute(PiveauSemanticAttributes.PIPE_TITLE, pipe.header.title)
    setAttribute(PiveauSemanticAttributes.PIPE_VERSION, pipe.header.version)
    setAttribute(PiveauSemanticAttributes.PIPE_CONTEXT, pipe.header.context)
    setAttribute(PiveauSemanticAttributes.PIPE_RUN_ID, pipe.header.runId)
}

fun Span.setPipeResourceAttributes(info: JsonObject) {
    info.getString("id")?.let { setAttribute(PiveauSemanticAttributes.PIVEAU_RESOURCE_ID, it) }
    info.getString("catalogue")?.let { setAttribute(PiveauSemanticAttributes.PIVEAU_RESOURCE_TARGET, it) }
    info.getString("content")?.let { setAttribute(PiveauSemanticAttributes.PIVEAU_RESOURCE_CONTENT, it) }
    info.getLong("total")?.let { setAttribute(PiveauSemanticAttributes.PIPE_RUN_TOTAL, it) }
    info.getLong("current")?.let { setAttribute(PiveauSemanticAttributes.PIPE_RUN_CURRENT, it) }
}
