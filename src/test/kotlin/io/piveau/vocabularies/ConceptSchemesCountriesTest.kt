package io.piveau.vocabularies

import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.vocabulary.DCTerms
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Testing Concept Schemes Countries")
class ConceptSchemesCountriesTest {

    private val model = ModelFactory.createDefaultModel().apply {
        readTurtleResource("CataloguesWithSpatial.ttl")
    }

    @Test
    fun `load catalogue with spatial and test labels`() {
        val resource = model.getResource("https://europeandataportal.eu/set/catalogue/catalogue-deu")
        val spatial = resource.getPropertyResourceValue(DCTerms.spatial)
        val concept = Countries.getConcept(spatial)

        assertNotNull(concept)
    }

    @Test
    fun `load catalogue with spatial and test iso31661alpha2`() {
        val concept = Countries.getConcept(model.getResource("https://europeandataportal.eu/set/catalogue/catalogue-deu")
            .getPropertyResourceValue(DCTerms.spatial))

        concept?.let {
            assertTrue { Countries.iso31661alpha2(it) == "de" }
        } ?: assertFalse(true, "Concept not found.")
    }

}