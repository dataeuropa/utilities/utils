package io.piveau.vocabularies

import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@DisplayName("Testing indexing of vocabularies")
@ExtendWith(VertxExtension::class)
class IndexingVocabularyTest {

    @BeforeEach
    fun setup(vertx: Vertx, testContext: VertxTestContext) {
        initRemotes(vertx, prefetch = false)
        testContext.completeNow()
    }

    @Test
    @Disabled
    fun `Indexing vocabularies`(vertx: Vertx, testContext: VertxTestContext) {

        indexingSKOSVocabulary(DataTheme).also {
            println(it.encodePrettily())
        }

        val continentsIndex = indexingSKOSVocabulary(Continents)
        val countriesIndex = indexingSKOSVocabulary(Countries)
        val placesIndex = indexingSKOSVocabulary(Places)
        val corporateBodiesIndex = indexingSKOSVocabulary(CorporateBodies)

        val licencesIndex = indexingSKOSVocabulary(License)

        val languagesIndex = indexingSKOSVocabulary(Languages)

        val frequenciesIndex = indexingSKOSVocabulary(Frequency)

        testContext.completeNow()
    }
}