package io.piveau.vocabularies

import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Assertions.assertNotNull
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

@DisplayName("Testing vocabularies")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class VocabulariesTest {

    @BeforeAll
    fun `Initialize vocabularies`(vertx: Vertx, testContext: VertxTestContext) {
        initRemotes(vertx, prefetch = false)
            .onSuccess { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    @Test
    @Disabled
    fun `Load single rdf file into model`() {
        assertNotNull(License.exactMatch("http://www.opendefinition.org/licenses/cc-by"), "No exact match found.")
    }

    @Test
    fun `Get country by label`() {
        assertNotNull(Countries.conceptFromLabel("Germany"))
    }

}
