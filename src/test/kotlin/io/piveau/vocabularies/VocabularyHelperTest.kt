package io.piveau.vocabularies

import io.piveau.dcatap.TripleStore
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith

@Disabled
@DisplayName("Testing Vocabulary Helper")
@ExtendWith(VertxExtension::class)
class VocabularyHelperTest {

    private val config: JsonObject = JsonObject()
        .put("address", "https://piveau-virtuoso-data-europa-eu.apps.osc.fokus.fraunhofer.de")

    @Test
    fun `load SKOS identifier`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getIdentifier(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://publications.europa.eu/resource/authority/corporate-body/ESTAT"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals("ESTAT", it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load SKOS exactMatch`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getExactMatch(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://publications.europa.eu/resource/authority/corporate-body/ESTAT"
        ).onSuccess {
            testContext.verify {
                Assertions
                    .assertEquals(ModelFactory.createDefaultModel().createResource("http://eurovoc.europa.eu/2198"), it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load SKOS prefLabels`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getPrefLabels(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://publications.europa.eu/resource/authority/corporate-body/EURCOU_PRES"
        ).onSuccess {
            val prefLabels : List<Pair<String, String>> = listOf(
                Pair("cs", "Předseda Evropské rady"),
                Pair("fr", "Président du Conseil européen"),
                Pair("sk", "Predseda Európskej rady"),
                Pair("hr", "Predsjednik Europskog vijeća"),
                Pair("et", "Euroopa Ülemkogu eesistuja"),
                Pair("sl", "Predsednik Evropskega sveta"),
                Pair("sv", "Europeiska rådets ordförande"),
                Pair("da", "Formand for Det Europæiske Råd"),
                Pair("lv", "Eiropadomes priekšsēdētājs"),
                Pair("ga", "Uachtarán na Comhairle Eorpaí"),
                Pair("nl", "Voorzitter van de Europese Raad"),
                Pair("pl", "Przewodniczący Rady Europejskiej"),
                Pair("fi", "Eurooppa-neuvoston puheenjohtaja"),
                Pair("de", "Präsident des Europäischen Rates"),
                Pair("bg", "Председател на Европейския съвет"),
                Pair("es", "Presidente del Consejo Europeo"),
                Pair("hu", "Az Európai Tanács elnöke"),
                Pair("ro", "Președintele Consiliului European"),
                Pair("mt", "Il-President tal-Kunsill Ewropew"),
                Pair("en", "President of the European Council"),
                Pair("it", "Presidente del Consiglio europeo"),
                Pair("lt", "Europos Vadovų Tarybos pirmininkas"),
                Pair("el", "Πρόεδρος του Ευρωπαϊκού Συμβουλίου"),
                Pair("pt", "Presidente do Conselho Europeu")
            )

            testContext.verify {
                Assertions.assertTrue(prefLabels.containsAll(it))
                Assertions.assertEquals(prefLabels.size, it.size)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load SKOS prefLabel`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getPrefLabel(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://publications.europa.eu/resource/authority/corporate-body/EURCOU_PRES",
            "de"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals("Präsident des Europäischen Rates", it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load SKOS altLabels`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getAltLabels(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://publications.europa.eu/resource/authority/corporate-body/EURCOU_PRES"
        ).onSuccess {
            val altLabels : List<Pair<String, String>> = listOf(
                Pair("de", "Präsidentin des Europäischen Rates"),
                Pair("fr", "Présidente du Conseil européen"),
                Pair("lv", "Eiropadomes priekšsēdētāja"),
                Pair("pl", "Przewodnicząca Rady Europejskiej"),
                Pair("lt", "Europos Vadovų Tarybos pirmininkė"),
                Pair("sk", "Predsedníčka Európskej rady"),
                Pair("ro", "Președinta Consiliului European"),
                Pair("hr", "Predsjednica Europskog vijeća"),
                Pair("sl", "Predsednica Evropskega sveta"),
                Pair("cs", "Předsedkyně Evropské rady")
            )

            testContext.verify {
                assert(altLabels.containsAll(it))
                assert(altLabels.size == it.size)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load SKOS altLabel`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getAltLabel(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://publications.europa.eu/resource/authority/corporate-body/EURCOU_PRES",
            "de"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals("Präsidentin des Europäischen Rates", it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load SKOS label`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getLabel(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://publications.europa.eu/resource/authority/corporate-body/EURCOU_PRES",
            "de"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals("Präsident des Europäischen Rates", it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load SKOS concepts`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        val model : Model = ModelFactory.createDefaultModel()

        VocabularyHelper(tripleStore).getConcepts(
            "http://publications.europa.eu/resource/authority/access-right"
        ).onSuccess {
            val concepts : List<Resource> = listOf(
                model.createResource("http://publications.europa.eu/resource/authority/access-right/NON_PUBLIC"),
                model.createResource("http://publications.europa.eu/resource/authority/access-right/OP_DATPRO"),
                model.createResource("http://publications.europa.eu/resource/authority/access-right/PUBLIC"),
                model.createResource("http://publications.europa.eu/resource/authority/access-right/RESTRICTED"),
                model.createResource("http://publications.europa.eu/resource/authority/access-right/CONFIDENTIAL"),
                model.createResource("http://publications.europa.eu/resource/authority/access-right/SENSITIVE")
            )

            testContext.verify {
                Assertions.assertTrue(concepts.containsAll(it))
                Assertions.assertEquals(concepts.size, it.size)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `check is SKOS concept`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).isConcept(
            "http://publications.europa.eu/resource/authority/access-right",
            "http://publications.europa.eu/resource/authority/access-right/PUBLIC"
        ).onSuccess {
            testContext.verify {
                Assertions.assertTrue(it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load AT table-id`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getTableId(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://publications.europa.eu/resource/authority/corporate-body"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals("corporate-body", it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load iso-3166-1-alpha-2 notation code`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getNotationCode(
            "http://publications.europa.eu/resource/authority/country",
            "http://publications.europa.eu/resource/authority/country/DEU",
            "http://publications.europa.eu/resource/authority/notation-type/ISO_3166_1_ALPHA_2"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals("DE", it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load uri from exactMatch`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getFromExactMatch(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://eurovoc.europa.eu/2198"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(ModelFactory.createDefaultModel()
                    .createResource("http://publications.europa.eu/resource/authority/corporate-body/ESTAT"), it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load uri from prefLabel`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getFromPrefLabel(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "Präsident des Europäischen Rates",
            "de"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(ModelFactory.createDefaultModel()
                    .createResource("http://publications.europa.eu/resource/authority/corporate-body/EURCOU_PRES"), it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load uri from altLabel`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getFromAltLabel(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "Präsidentin des Europäischen Rates",
            "de"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(ModelFactory.createDefaultModel()
                    .createResource("http://publications.europa.eu/resource/authority/corporate-body/EURCOU_PRES"), it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load country of place`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getCountryOfConcept(
            "http://publications.europa.eu/resource/authority/place",
            "http://publications.europa.eu/resource/authority/place/1A0_PRN"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(ModelFactory.createDefaultModel()
                    .createResource("http://publications.europa.eu/resource/authority/country/1A0"), it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load iso-639-1 code`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getLegacyCode(
            "http://publications.europa.eu/resource/authority/language",
            "http://publications.europa.eu/resource/authority/language/BUL",
            "iso-639-1"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals("bg", it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load TED code`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getLegacyCode(
            "http://publications.europa.eu/resource/authority/language",
            "http://publications.europa.eu/resource/authority/language/BUL",
            "TED"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals("BG", it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `test indexing SKOS vocable`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        val expectedInScheme : JsonArray = JsonArray()
            .add("http://publications.europa.eu/resource/authority/country")

        val expectedPrefLabels : JsonObject = JsonObject()
            .put("no", "Tyskland")
            .put("de", "Deutschland")
            .put("fi", "Saksa")
            .put("be", "Германія")
            .put("ru", "Германия")
            .put("pt", "Alemanha")
            .put("bg", "Гepмaния")
            .put("lt", "Vokietija")
            .put("fo", "Týskland")
            .put("hr", "Njemačka")
            .put("lv", "Vācija")
            .put("fr", "Allemagne")
            .put("hu", "Németország")
            .put("bs", "Njemačka")
            .put("uk", "Німеччина")
            .put("sk", "Nemecko")
            .put("sl", "Nemčija")
            .put("ga", "An Ghearmáin")
            .put("mk", "Германија")
            .put("ca", "Alemanya")
            .put("sq", "Gjermania")
            .put("sr", "Немачка")
            .put("sv", "Tyskland")
            .put("gl", "Alemaña")
            .put("el", "Γερμανία")
            .put("mt", "Il-Ġermanja")
            .put("en", "Germany")
            .put("is", "Þyskaland")
            .put("it", "Germania")
            .put("es", "Alemania")
            .put("et", "Saksamaa")
            .put("cs", "Německo")
            .put("eu", "Alemania")
            .put("rm", "Germania")
            .put("pl", "Niemcy")
            .put("da", "Tyskland")
            .put("ro", "Germania")
            .put("nl", "Duitsland")
            .put("tr", "Almanya")

        val expected : JsonObject = JsonObject()
            .put("id", "DEU")
            .put("resource", "http://publications.europa.eu/resource/authority/country/DEU")
            .put("in_scheme", expectedInScheme)
            .put("pref_label", expectedPrefLabels)


        VocabularyHelper(tripleStore).indexingSKOSVocable(
            "http://publications.europa.eu/resource/authority/country",
            "http://publications.europa.eu/resource/authority/country/DEU"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(expected, it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `test indexing SKOS vocable with alt label`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        val expectedInScheme : JsonArray = JsonArray()
            .add("http://publications.europa.eu/resource/authority/corporate-body")

        val expectedPrefLabels : JsonObject = JsonObject()
            .put("cs", "Předseda Evropské rady")
            .put("fr", "Président du Conseil européen")
            .put("sk", "Predseda Európskej rady")
            .put("hr", "Predsjednik Europskog vijeća")
            .put("et", "Euroopa Ülemkogu eesistuja")
            .put("sl", "Predsednik Evropskega sveta")
            .put("sv", "Europeiska rådets ordförande")
            .put("da", "Formand for Det Europæiske Råd")
            .put("lv", "Eiropadomes priekšsēdētājs")
            .put("ga", "Uachtarán na Comhairle Eorpaí")
            .put("nl", "Voorzitter van de Europese Raad")
            .put("pl", "Przewodniczący Rady Europejskiej")
            .put("fi", "Eurooppa-neuvoston puheenjohtaja")
            .put("de", "Präsident des Europäischen Rates")
            .put("bg", "Председател на Европейския съвет")
            .put("es", "Presidente del Consejo Europeo")
            .put("hu", "Az Európai Tanács elnöke")
            .put("ro", "Președintele Consiliului European")
            .put("mt", "Il-President tal-Kunsill Ewropew")
            .put("en", "President of the European Council")
            .put("it", "Presidente del Consiglio europeo")
            .put("lt", "Europos Vadovų Tarybos pirmininkas")
            .put("el", "Πρόεδρος του Ευρωπαϊκού Συμβουλίου")
            .put("pt", "Presidente do Conselho Europeu");

        val expectedAltLabels : JsonObject = JsonObject()
            .put("de", "Präsidentin des Europäischen Rates")
            .put("fr", "Présidente du Conseil européen")
            .put("lv", "Eiropadomes priekšsēdētāja")
            .put("pl", "Przewodnicząca Rady Europejskiej")
            .put("lt", "Europos Vadovų Tarybos pirmininkė")
            .put("sk", "Predsedníčka Európskej rady")
            .put("ro", "Președinta Consiliului European")
            .put("hr", "Predsjednica Europskog vijeća")
            .put("sl", "Predsednica Evropskega sveta")
            .put("cs", "Předsedkyně Evropské rady");

        val expected : JsonObject = JsonObject()
            .put("id", "EURCOU_PRES")
            .put("resource", "http://publications.europa.eu/resource/authority/corporate-body/EURCOU_PRES")
            .put("in_scheme", expectedInScheme)
            .put("pref_label", expectedPrefLabels)
            .put("alt_label", expectedAltLabels)


        VocabularyHelper(tripleStore).indexingSKOSVocable(
            "http://publications.europa.eu/resource/authority/corporate-body",
            "http://publications.europa.eu/resource/authority/corporate-body/EURCOU_PRES"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(expected, it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `test indexing spare SKOS vocable`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        val expected : JsonObject = JsonObject()
            .put("id", "1509")
            .put("resource", "http://eurovoc.europa.eu/1509")

        VocabularyHelper(tripleStore).indexingSKOSVocable(
            "http://publications.europa.eu/resource/dataset/eurovoc_alignment_country",
            "http://eurovoc.europa.eu/1509"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(expected, it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `test indexing spare SKOS vocabulary`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        val expected : JsonObject = JsonObject()
            .put("id", "eurovoc_alignment_country")
            .put("resource", "http://publications.europa.eu/resource/dataset/eurovoc_alignment_country")
            .put("vocab", JsonArray())

        VocabularyHelper(tripleStore).indexingSKOSVocabulary(
            "http://publications.europa.eu/resource/dataset/eurovoc_alignment_country"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(expected, it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `test indexing SKOS vocabulary`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        val expectedInScheme : JsonArray = JsonArray()
            .add("http://publications.europa.eu/resource/authority/access-right")

        val expectedPrefLabelsOpDatpro : JsonObject = JsonObject()
           .put("pl", "Dane tymczasowe")
           .put("nl", "Voorlopige gegevens")
           .put("da", "Midlertidige data")
           .put("lt", "Laikinieji duomenys")
           .put("cs", "Předběžné údaje")
           .put("pt", "Dados provisórios")
           .put("fi", "Alustavat tiedot")
           .put("hu", "Ideiglenes adatok")
           .put("lv", "Provizoriski dati")
           .put("bg", "НЕОКОНЧАТЕЛНИ ДАННИ")
           .put("et", "Esialgsed andmed")
           .put("en", "Provisional data")
           .put("es", "Datos provisionales")
           .put("fr", "Données provisoires")
           .put("hr", "Privremeni podaci")
           .put("el", "Προσωρινά δεδομένα")
           .put("it", "Dati provvisori")
           .put("sl", "Začasni podatki")
           .put("sv", "Tillfälliga uppgifter")
           .put("mt", "Dejta provviżorja")
           .put("sk", "Predbežné údaje")
           .put("ro", "Date provizorii")
           .put("de", "Vorläufige Daten")
           .put("ga", "Sonraí sealadacha")

        val expectedOpDatpro : JsonObject = JsonObject()
            .put("id", "OP_DATPRO")
            .put("resource", "http://publications.europa.eu/resource/authority/access-right/OP_DATPRO")
            .put("in_scheme", expectedInScheme)
            .put("pref_label", expectedPrefLabelsOpDatpro)

        val expectedPrefLabelsPublic : JsonObject = JsonObject()
            .put("en", "public")
            .put("et", "avalik")

        val expectedPublic : JsonObject = JsonObject()
            .put("id", "PUBLIC")
            .put("resource", "http://publications.europa.eu/resource/authority/access-right/PUBLIC")
            .put("in_scheme", expectedInScheme)
            .put("pref_label", expectedPrefLabelsPublic)

        val expectedPrefLabelsNonPublic : JsonObject = JsonObject()
            .put("et", "mitteavalik")
            .put("en", "non-public")

        val expectedNonPublic : JsonObject = JsonObject()
            .put("id", "NON_PUBLIC")
            .put("resource", "http://publications.europa.eu/resource/authority/access-right/NON_PUBLIC")
            .put("in_scheme", expectedInScheme)
            .put("pref_label", expectedPrefLabelsNonPublic)

        val expectedPrefLabelsRestricted : JsonObject = JsonObject()
            .put("en", "restricted")
            .put("et", "piiratud")

        val expectedRestricted : JsonObject = JsonObject()
            .put("id", "RESTRICTED")
            .put("resource", "http://publications.europa.eu/resource/authority/access-right/RESTRICTED")
            .put("in_scheme", expectedInScheme)
            .put("pref_label", expectedPrefLabelsRestricted)

        val expectedPrefLabelsSensitive : JsonObject = JsonObject()
            .put("en", "sensitive")
            .put("et", "tundlik")
            .put("fr", "sensible")

        val expectedSensitive : JsonObject = JsonObject()
            .put("id", "SENSITIVE")
            .put("resource", "http://publications.europa.eu/resource/authority/access-right/SENSITIVE")
            .put("in_scheme", expectedInScheme)
            .put("pref_label", expectedPrefLabelsSensitive)

        val expectedPrefLabelsConfidential : JsonObject = JsonObject()
            .put("et", "konfidentsiaalne")
            .put("en", "confidential")

        val expectedConfidential : JsonObject = JsonObject()
            .put("id", "CONFIDENTIAL")
            .put("resource", "http://publications.europa.eu/resource/authority/access-right/CONFIDENTIAL")
            .put("in_scheme", expectedInScheme)
            .put("pref_label", expectedPrefLabelsConfidential)

        val expectedVocab : JsonArray = JsonArray()
            .add(expectedOpDatpro)
            .add(expectedPublic)
            .add(expectedNonPublic)
            .add(expectedRestricted)
            .add(expectedSensitive)
            .add(expectedConfidential)

        val expected : JsonObject = JsonObject()
            .put("id", "access-right")
            .put("resource", "http://publications.europa.eu/resource/authority/access-right")
            .put("vocab", expectedVocab)

        VocabularyHelper(tripleStore).indexingSKOSVocabulary(
            "http://publications.europa.eu/resource/authority/access-right"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(expected.getString("id"), it.getString("id"))
                Assertions.assertEquals(expected.getString("resource"), it.getString("resource"))
                Assertions.assertTrue(
                    expected.getJsonArray("vocab").toList().containsAll(it.getJsonArray("vocab").toList())
                )
                Assertions.assertEquals(expected.getJsonArray("vocab").size(), it.getJsonArray("vocab").size())
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load concept id public from access-right`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getConceptId(
            "http://publications.europa.eu/resource/authority/access-right",
            "http://publications.europa.eu/resource/authority/access-right/PUBLIC"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals("PUBLIC", it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `check is machine readable`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).isMachineReadable(
            "http://publications.europa.eu/resource/authority/file-type",
            "http://publications.europa.eu/resource/authority/file-type/XML"
        ).onSuccess {
            testContext.verify {
                Assertions.assertTrue(it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `check is non proprietary`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).isNonProprietary(
            "http://publications.europa.eu/resource/authority/file-type",
            "http://publications.europa.eu/resource/authority/file-type/CSV"
        ).onSuccess {
            testContext.verify {
                Assertions.assertTrue(it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `check is open`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).isOpen(
            "http://publications.europa.eu/resource/authority/licence",
            "http://europeandataportal.eu/ontologies/od-licenses#Apache-2.0"
        ).onSuccess {
            testContext.verify {
                Assertions.assertTrue(it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load licensing assistant`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).getLicensingAssistant(
            "http://publications.europa.eu/resource/authority/licence",
            "http://europeandataportal.eu/ontologies/od-licenses#CC-BY-SA3.0NL"
        ).onSuccess {
            testContext.verify {
                Assertions.assertEquals(ModelFactory.createDefaultModel()
                    .createResource("https://data.europa.eu/en/content/show-license?license_id=CC-BY-SA3.0NL"), it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `check has concept, true`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).hasConcepts(
            "http://publications.europa.eu/resource/authority/licence"
        ).onSuccess {
            testContext.verify {
                Assertions.assertTrue(it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `check has concept, false`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        VocabularyHelper(tripleStore).hasConcepts(
            "http://publications.europa.eu/resource/authority/nuts-gisco-links"
        ).onSuccess {
            testContext.verify {
                Assertions.assertFalse(it)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load concept scheme of csv in file-type`(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        val expectedInScheme : JsonArray = JsonArray()
            .add(ModelFactory.createDefaultModel()
                .createResource("http://publications.europa.eu/resource/authority/file-type"))

        VocabularyHelper(tripleStore).getConceptScheme(
            "http://publications.europa.eu/resource/authority/file-type",
            "http://publications.europa.eu/resource/authority/file-type/CSV"
        ).onSuccess {
            testContext.verify {
                Assertions.assertTrue(expectedInScheme.toList().containsAll(it))
                Assertions.assertEquals(expectedInScheme.size(), it.size)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `load concept scheme `(vertx: Vertx, testContext: VertxTestContext) {
        val tripleStore = TripleStore(vertx, config)

        val expectedInScheme : JsonArray = JsonArray()
            .add(ModelFactory.createDefaultModel().createResource("http://eurovoc.europa.eu/100186"))
            .add(ModelFactory.createDefaultModel().createResource("http://eurovoc.europa.eu/100141"))

        VocabularyHelper(tripleStore).getConceptScheme(
            "http://publications.europa.eu/resource/dataset/eurovoc",
            "http://eurovoc.europa.eu/427"
        ).onSuccess {
            testContext.verify {
                Assertions.assertTrue(expectedInScheme.toList().containsAll(it))
                Assertions.assertEquals(expectedInScheme.size(), it.size)
            }
            testContext.completeNow()
        }.onFailure(testContext::failNow)
    }

    @Test
    fun `test vocable id normalizer`(vertx: Vertx, testContext: VertxTestContext) {
        val result = normalize("Some---identifier-/-with-#-SYMBOLS")
        testContext.verify {
            Assertions.assertEquals("Some-identifier-with-SYMBOLS", result)
        }
        testContext.completeNow()
    }

}
