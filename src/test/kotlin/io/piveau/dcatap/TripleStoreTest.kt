package io.piveau.dcatap

import io.piveau.rdf.toModel
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.jena.riot.Lang
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.testcontainers.containers.GenericContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName


@DisplayName("Testing triple store")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Testcontainers(disabledWithoutDocker = true)
class TripleStoreTest {

    companion object {
        @Container
        val virtuoso = GenericContainer(DockerImageName.parse("openlink/virtuoso-opensource-7:7.2.13"))
            .withExposedPorts(8890)
            .withEnv("DBA_PASSWORD", "test")
    }

    private lateinit var tripleStore: TripleStore

    @BeforeAll
    fun setUp(vertx: Vertx, testContext: VertxTestContext) {
        val config = JsonObject()
            .put("address", "http://${virtuoso.host}:${virtuoso.firstMappedPort}")
            .put("password", "test")

        tripleStore = TripleStore(vertx, config)

        testContext.completeNow()
    }

    @Test
    @Order(1)
    fun `Testing put graph`(vertx: Vertx, testContext: VertxTestContext) {
        vertx.fileSystem().readFile("test.rdf")
            .map(Buffer::toString)
            .map { data -> data.encodeToByteArray().toModel(Lang.RDFXML) }
            .compose { model -> tripleStore.putGraph("http://example.com", model) }
            .onSuccess {
                testContext.verify {
                    Assertions.assertEquals("created", it)
                }
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

    @Test
    @Order(2)
    fun `Testing ask`(vertx: Vertx, testContext: VertxTestContext) {
        tripleStore.datasetManager.existGraph("http://example.com")
            .onSuccess {
                testContext.verify {
                    Assertions.assertTrue(it)
                }
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

    @Test
    @Order(3)
    fun `Testing equals function`(vertx: Vertx, testContext: VertxTestContext) {

        val test = TripleStore(vertx, JsonObject())
        Assertions.assertNotEquals(tripleStore, test)

        val test2 = tripleStore
        Assertions.assertEquals(tripleStore, test2)

        val test3 = TripleStore(
            vertx,
            JsonObject().put("address", "http://${virtuoso.host}:${virtuoso.firstMappedPort}").put("password", "test"),
            null,
            "test"
        )
        val test4 = TripleStore(
            vertx,
            JsonObject().put("address", "http://${virtuoso.host}:${virtuoso.firstMappedPort}").put("password", "test"),
            null,
            "test"
        )
        Assertions.assertEquals(test3, test4)
        Assertions.assertEquals(test4, test4)
        Assertions.assertEquals(test4, test3)
        Assertions.assertNotEquals(test3, tripleStore)
        Assertions.assertNotEquals(tripleStore, test3)
        Assertions.assertNotEquals(tripleStore, null)
        Assertions.assertNotEquals(tripleStore, "test")


        testContext.completeNow()
    }

}