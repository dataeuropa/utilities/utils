package io.piveau.dcatap

import io.piveau.vocabularies.vocabulary.DCATAP
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.jena.query.DatasetFactory
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.vocabulary.DCTerms
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith

@DisplayName("Testing Prefixes")
@ExtendWith(VertxExtension::class)
class PrefixesTest {

    @Test
    fun `test set namespace prefixes in models`(testContext: VertxTestContext) {
        val model = ModelFactory.createDefaultModel()

        val resource = model.createResource("https://data.europa.eu.eu/set/distribution/test-distribution")

        // KNOWN NAMESPACES
        val knownStatement1 = model.createStatement(resource, DCTerms.format, "test")
        model.add(knownStatement1)

        val knownStatement2 = model.createStatement(resource, DCATAP.availability, "test")
        model.add(knownStatement2)

        // UNKNOWN NAMESPACE
        val unknownProperty = model.createProperty("http://example.org/terms/format")
        val unknownStatement = model.createStatement(resource, unknownProperty,
            "http://publications.europa.eu/resource/authority/file-type/XLS")
        model.add(unknownStatement)

        model.setNsPrefixesFiltered()

        val prefixes = mapOf(
            "dct" to DCTerms.NS,
            "dcatap" to DCATAP.NS
        )

        testContext.verify {
            Assertions.assertEquals(prefixes, model.nsPrefixMap)
        }

        testContext.completeNow()
    }

    @Test
    fun `test set namespace prefixes in datasets`(testContext: VertxTestContext) {
        val model = ModelFactory.createDefaultModel()
        val resource = model.createResource("https://data.europa.eu.eu/set/distribution/test-distribution")

        // KNOWN NAMESPACES
        val knownStatement1 = model.createStatement(resource, DCTerms.format, "test")
        model.add(knownStatement1)

        val knownStatement2 = model.createStatement(resource, DCATAP.availability, "test")
        model.add(knownStatement2)

        // UNKNOWN NAMESPACE
        val unknownProperty = model.createProperty("http://example.org/terms/format")
        val unknownStatement = model.createStatement(resource, unknownProperty,
            "http://publications.europa.eu/resource/authority/file-type/XLS")
        model.add(unknownStatement)

        val dataset = DatasetFactory.create(model)
        dataset.setNsPrefixesFiltered()

        val prefixes = mapOf(
            "dct" to DCTerms.NS,
            "dcatap" to DCATAP.NS
        )

        testContext.verify {
            Assertions.assertEquals(prefixes, dataset.prefixMapping.nsPrefixMap)
        }

        testContext.completeNow()
    }

}

