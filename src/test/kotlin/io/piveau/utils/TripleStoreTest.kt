package io.piveau.utils

import io.piveau.dcatap.TripleStore
import io.piveau.dcatap.URIRef
import io.piveau.rdf.asString
import io.piveau.rdf.toModel
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import io.vertx.junit5.Timeout
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.jena.query.ResultSetFormatter
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.SKOS
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import java.util.concurrent.TimeUnit
import kotlin.streams.toList

@DisplayName("Testing triple store")
@ExtendWith(VertxExtension::class)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class TripleStoreTest {

    val testGraphName: URIRef = "http://piveau.io/graph/test1"

    val testGraph = """
        @prefix dc:         <http://purl.org/dc/terms/> .
        @prefix skos:       <http://www.w3.org/2004/02/skos/core#> .
        @prefix piveau:     <http://piveau.io/ns/test#> .

        piveau:test
            a                   skos:Concept ;
            skos:prefLabel      "Test"@en ;
            dc:description      "Just simple test."@en ;
            dc:identifier       "TEST" .
    """.trimIndent().toByteArray().toModel(Lang.TURTLE)

    val config: JsonObject = JsonObject()
        .put("address", "https://www.europeandataportal.eu")
        .put("username", "")
        .put("password", "")

    //    @Test
    @Order(1)
    fun `Put a graph`(vertx: Vertx, testContext: VertxTestContext) {
        TripleStore(vertx, config).putGraph(testGraphName, testGraph)
            .onSuccess { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    //    @Test
    @Order(2)
    fun `Query a select`(vertx: Vertx, testContext: VertxTestContext) {
        TripleStore(vertx, config)
            .select("SELECT ?test WHERE { graph <$testGraphName> { ?test a <${SKOS.Concept}> } }")
            .onSuccess {
                print(ResultSetFormatter.asText(it))
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

    //    @Test
    @Order(3)
    fun `Update query`(vertx: Vertx, testContext: VertxTestContext) {
        TripleStore(vertx, config)
            .update("INSERT DATA { GRAPH <$testGraphName> { <http://piveau.io/ns/test#test> <${DCTerms.title}> \"Testing TripleStore\"@en . } }")
            .onSuccess { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    //    @Test
    @Order(4)
    fun `Get a graph`(vertx: Vertx, testContext: VertxTestContext) {
        TripleStore(vertx, config)
            .getGraph(testGraphName)
            .onSuccess {
                print(it.asString(Lang.TURTLE))
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

    //    @Test
    @Order(5)
    fun `Delete a graph`(vertx: Vertx, testContext: VertxTestContext) {
        TripleStore(vertx, config)
            .deleteGraph(testGraphName)
            .onSuccess { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    @Test
    @Disabled
    @Timeout(10, timeUnit = TimeUnit.MINUTES)
    fun `Set huge graph`(vertx: Vertx, testContext: VertxTestContext) {
        val model = JenaUtils.read(vertx.fileSystem().readFileBlocking("corporatebodies-skos-ap-act.rdf").bytes, Lang.RDFXML, "corporatebodies-skos-ap-act.rdf")
        val config = JsonObject().put("address", "http://localhost:8890").put("useVirtuosoPragmas", true)

        val tripleStore = TripleStore(vertx, config, WebClient.create(vertx, WebClientOptions().setKeepAlive(true)))
        tripleStore.setGraph("urn:test", model, false)
            .onSuccess {
                testContext.completeNow()
            }
            .onFailure { testContext.failNow(it) }
    }

}
