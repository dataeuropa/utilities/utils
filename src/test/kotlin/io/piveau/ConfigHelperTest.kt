package io.piveau

import io.piveau.json.ConfigHelper
import io.piveau.json.asJsonObject
import io.vertx.core.json.JsonObject
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ConfigHelperTest {

    companion object {
        val config = JsonObject("{ \"keyString\": \"{ }\", \"keyJson\": {} }")
    }

    @Test
    fun getStringJson() {
        val json = ConfigHelper.forConfig(config).forceJsonObject("keyString")
        assert(json == JsonObject("{}"))
        val json2 = config.asJsonObject("keyString")
        assert(json2 == JsonObject("{}"))
    }

    @Test
    fun getJsonJson() {
        val json = ConfigHelper.forConfig(config).forceJsonObject("keyJson")
        assert(json == JsonObject("{}"))
        val json2 = config.asJsonObject("keyJson")
        assert(json2 == JsonObject("{}"))
    }

    @Test
    fun getNullJson() {
        val json = ConfigHelper.forConfig(config).forceJsonObject("anyKey")
        assert(json == JsonObject())
        val json2 = config.asJsonObject("anyKey")
        assert(json2 == JsonObject())
    }

}