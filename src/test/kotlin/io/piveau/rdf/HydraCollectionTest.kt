package io.piveau.rdf

import io.piveau.vocabularies.vocabulary.HYDRA
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ResIterator
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.RDF
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import java.net.URL

class HydraCollectionTest {
    private lateinit var model: Model
    private lateinit var resource: Resource

    @Before
    fun setUp() {
        model = mock(Model::class.java)
        resource = mock(Resource::class.java)
        val resIterator = mock(ResIterator::class.java)
        `when`(resIterator.hasNext()).thenReturn(true)
        `when`(resIterator.next()).thenReturn(resource)
        `when`(model.listResourcesWithProperty(RDF.type, HYDRA.PartialCollectionView))
            .thenReturn(resIterator)
        `when`(resource.uri).thenReturn("http://example.com/datasets?offset=0&limit=100")
    }

    // testcase for default limit
    @Test
    fun testDefaultLimit() {
        val base = "http://example.com/datasets"
        val totalItems = 500
        val offset = 0
        val hydraCollection = HydraCollection.build(base, totalItems, offset)
        val expectedUri = "http://example.com/datasets?offset=0&limit=100"
        compareUrlsIgnoringOrder(expectedUri, hydraCollection.id)
    }

    // testcase for custom limit
    @Test
    fun testCustomLimit() {
        val base = "http://example.com/datasets"
        val totalItems = 500
        val offset = 0
        val customLimit = 50
        val hydraCollection = HydraCollection.build(base, totalItems, offset, customLimit)
        val expectedUri = "http://example.com/datasets?offset=0&limit=$customLimit"
        compareUrlsIgnoringOrder(expectedUri, hydraCollection.id)
    }

    // testcase for Old And New Hydra Type
    @Test
    fun testOldAndNewHydraType() {
        val base = "http://example.com/datasets"
        val totalItems = 500
        val limit = 100
        val offset = 0

        // Test with usePagedCollection = true
        var hydraCollection = HydraCollection.build(base, totalItems, offset, limit, usePagedCollection = true)
        var expectedUri = "http://example.com/datasets?offset=0&limit=100&usePagedCollection=true"
        compareUrlsIgnoringOrder(expectedUri, hydraCollection.id)
        assertEquals(
            HYDRA.PagedCollection,
            hydraCollection.model.getResource(hydraCollection.id).getProperty(RDF.type).`object`.asResource()
        )

        // Test with usePagedCollection = false
        hydraCollection = HydraCollection.build(base, totalItems, offset, limit, usePagedCollection = false)
        expectedUri = "http://example.com/datasets?offset=0&limit=100"
        compareUrlsIgnoringOrder(expectedUri, hydraCollection.id)
        assertEquals(
            HYDRA.PartialCollectionView,
            hydraCollection.model.getResource(hydraCollection.id).getProperty(RDF.type).`object`.asResource()
        )
    }

    // other methods
    // method for ignoring the query parameter order
    private fun compareUrlsIgnoringOrder(expected: String, actual: String) {
        val expectedUrl = URL(expected)
        val actualUrl = URL(actual)

        assertEquals(expectedUrl.protocol, actualUrl.protocol)
        assertEquals(expectedUrl.host, actualUrl.host)
        assertEquals(expectedUrl.path, actualUrl.path)

        val expectedParams = parseQuery(expectedUrl.query)
        val actualParams = parseQuery(actualUrl.query)

        assertEquals(expectedParams, actualParams)
    }

    // method for parse query
    private fun parseQuery(query: String?): Map<String, String> {
        return query?.split("&")
            ?.map { param -> param.split("=") }
            ?.associate { it[0] to it[1] }
            ?: emptyMap()
    }
}