package io.piveau.utils;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.apache.commons.io.input.XmlStreamReader;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.DCTerms;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLInputFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Testing piveau utils class")
@ExtendWith(VertxExtension.class)
public class PiveauUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(PiveauUtilsTest.class);

    @Test
    void testWrite(Vertx vertx, VertxTestContext testContext) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(vertx.fileSystem().readFileBlocking("test.rdf").getBytes());
        Model model = JenaUtils.read(inputStream, "application/rdf+xml");
        log.info(JenaUtils.write(model, Lang.RDFXML));
        testContext.completeNow();
    }

    @Test
    @Disabled("XML declaration is now always prepended")
    void testXmlDeclaration(Vertx vertx, VertxTestContext testContext) {
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(vertx.fileSystem().readFileBlocking("test.rdf").getBytes())) {
            Model model = JenaUtils.read(inputStream, "application/rdf+xml");

            String xmlDeclarationPresent = JenaUtils.write(model, Lang.RDFXML, true);
            assertNotNull(XMLInputFactory.newInstance()
                    .createXMLStreamReader(
                            new StringReader(xmlDeclarationPresent)
                    )
                    .getVersion()
            );

            String xmlDeclarationMissing = JenaUtils.write(model, Lang.RDFXML, false);
            assertNull(XMLInputFactory.newInstance()
                    .createXMLStreamReader(
                            new StringReader(xmlDeclarationMissing)
                    )
                    .getVersion()
            );

            testContext.completeNow();
        } catch (Exception e) {
            testContext.failNow(e);
        }
    }

    @Test
    void testDataset(Vertx vertx, VertxTestContext testContext) {
        Dataset dataset = DatasetFactory.create();
        testContext.completeNow();
    }

    @Test
    void testNormalize() {
        log.debug(JenaUtils.normalize("Съд на публичната   служба"));
    }

}
