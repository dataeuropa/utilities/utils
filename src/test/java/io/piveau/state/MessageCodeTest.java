package io.piveau.state;

import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Testing message codecs & event bus")
@ExtendWith(VertxExtension.class)


public class MessageCodeTest {
    @Test
    @DisplayName("Send a state object")
    public void sendState(Vertx vertx, VertxTestContext testContext) {

        EventBus ev = vertx.eventBus();
        ev.registerDefaultCodec(State.class, new StateMessageCodec());
        String userid = "userid";
        State s = new State(userid);


        MessageConsumer<State> mc = ev.consumer("testing");

        mc.handler(h -> {
            State s2 = h.body();

            assertNotNull(s2);
            assertEquals(s.getUserID(), s2.getUserID());


            assertNotNull(s2.children());

            assertNull(s2.getChild("test"));


            assertEquals(s, s2);
            assertEquals(s.getID(), s2.getID());


            testContext.completeNow();
        });


        ev.send("testing", s);

    }


}
