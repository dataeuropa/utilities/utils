package io.piveau.state;

import io.piveau.state.sub.SubState;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.authorization.Authorization;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Testing state object")
@ExtendWith(VertxExtension.class)
public class StateTest {
    @Test
    @DisplayName("Create a state object")
    public void createState(VertxTestContext testContext) {

        String userid = "userid";
        State s = new State(userid);

        assertNotNull(s);
        assertEquals(userid, s.getUserID());

        assertNotNull(s.children());

        assertNull(s.getChild("test"));

        s.addChild("test", new SubState() {

            State parent;

            @Override
            public State getParent() {
                return parent;
            }

            @Override
            public SubState setParent(State parent) {
                this.parent = parent;
                return this;
            }
        });


        assertNotNull(s.getChild("test"));

        assertEquals(s.getChild("test").getParent(), s);


        assertNotNull(s.getID());


        s.setUser(new User() {
            @Override
            public JsonObject attributes() {
                return null;
            }

            @Override
            public User isAuthorized(Authorization authorization, Handler<AsyncResult<Boolean>> handler) {
                return null;
            }

            @Override
            public JsonObject principal() {
                return null;
            }

            @Override
            public void setAuthProvider(AuthProvider authProvider) {

            }

            @Override
            public User merge(User user) {
                return null;
            }
        });
        assertNotNull(s.user());

        s.clearUser();
        assertNull(s.user());

        testContext.completeNow();

    }

}
