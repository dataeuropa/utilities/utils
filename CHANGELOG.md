# ChangeLog

## Unreleased

## 11.1.9 (2024-10-08)

**Added:**

* Property isOpenDataPresence to PV vocabulary
* Functionality to the Keycloak helper class to modify the valid redirect uri of a client

## 11.1.8 (2024-10-08)

Release process fix

## 11.1.7 (2024-10-08)

**Fixed:**

* Improved Security Features for Advanced Access Control
* Updated HVD Categories Local Vocabulary

**Added:**

* backwards compatibility for hydra `PagedCollection` users

## 11.1.6 (2024-08-27)

**Fixed:**

* Correct accept header for sparql results

## 11.1.5 (2024-08-22)

**Added:**

* Class `CatalogueExtendManager` for working with access restricted Datasets
* ACCESS_LEVEL enum to work with public and restricted datasets
* access management properties and resources into PV vocabulary

**Added:**

* Fixed pagination bug by setting the default limit to 100.
* Added test cases in `HydraCollectionTest` to verify the correctness of default and custom limits in the API.

## 11.1.4 (2024-08-15)

**Added:**

* initial versions of State and SubState objects for better context/state management.
* Added additional prefixed for RDF
* Class `DatasetExtendManager` for working with access restricted Datasets
* Initial implementation of piveau state objects
* Initial implementation of piveau access mangement solution

## 11.1.3 (2024-08-08)

**Fixed:**

* Hydra pagination without `totalItems` set

**Added:**

* Convenience methods `CataloguesManager.ensureExists` and `DatasetsManager.ensureExists`

## 11.1.2 (2024-07-16)

**Changed:**

* Flexible Hydra `next` and `nextPage` property usage

## 11.1.1 (2024-07-15)

**Fixed:**

* presentAs of model lists

## 11.1.0 (2024-07-15)

**Added:**

* New method `CatalogueManager.datasetsCount()` for counting number of datasets in a catalogue

**Changed:**

* Mark old Hydra elements in the `HYDRA` vocabulary as deprecated
* Use `PartialCollectionView` as Hydra class now
* Add Hydra class to the default graph

**Removed:**

* `List<Model>.presentAs` with hydraCollection as parameter

## 11.0.11 (2024-07-12)

**Fixed:**

* Exist query for datasets

## 11.0.10 (2024-07-12)

**Added:**

* `DCATAPUriSchema.inModel(model)` method
* `DatasetManager.exist()` method

**Changed:**

* `DatasetManager.datasetsAsFlow()` filter only models with dcat:Dataset resource

## 11.0.9 (2024-07-09)

**Added:**

* `TIME` vocabulary
* Stop predicate `isPartOf` and `hasPart` for datasets

**Changed:**

* TripleStore use no auth request on non auth endpoint

## 11.0.8 (2024-06-20)

**Added:**

* New Keycloak method to retrieve permissions for a user

## 11.0.7 (2024-06-12)

**Fixed:**

* Now really, the geo parsing problem...

**Changed:**

* Timeout for resolving geonames is decreased

## 11.0.6 (2024-06-11)

**Fixed:**

* Prevent crash in StandardVisitor for invalid xsd:durations

**Fixed:**

* Geo parsing problem with null values

## 11.0.5 (2024-06-06)

**Fixed:**

* Dependency chaos with jackson library

## 11.0.4 (2024-06-01)

**Added:**

* `equals` and `hashCode` functions in `TripleStore` class
* ability to configure chunk size via `chunksize`. Default is 5000
* `PV:openDataPresence`, `PV:visualisationSelection` and `PV:visualisationData`

**Changed:**

* all models that are greater than `chunksize` will be skolemized before being stored into the triplestore.
* Switch to Apache Jena 5, but had to safe model extraction utils from 4

**Added:**

* `equals` and `hashCode` functions in `TripleStore` class
* ability to configure chunk size via `chunksize`. Default is 5000
* `PV:openDataPresence`, `PV:visualisationSelection` and `PV:visualisationData`

** Changed:**

* all models that are greater than `chunksize` will be skolemized before being stored into the triplestore.

## 11.0.3 (2024-05-03)

**Changed:**

* Further improvements for the ModelExtractor classes

## 11.0.2 (2024-04-26)

**Changed:**

* Extract as model for specific "Classes" with specific stop conditions

## 11.0.1 (2024-04-21)

**Fixed:**

* Vocabularies pre-fetching

**Changed:**

* Ontology containsConcept parameter to resource type instead of individual

## 11.0.0 (2024-04-15)

**Changed:**

* `ConceptScheme` class based now on ontology model, supporting reasoner and inference
* `ADMS` class reflects transition to SEMIC
* Vocabularies API
* Lib dependency updates

**Added:**

* `AuthorityTable` class

**Fixed:**

* Resource.identify() now utilizes the removePrefix parameter to remove all contents of a string before the last /
* hydra next element is not included in the last page anymore
* `Resource.identify()` now utilizes the removePrefix parameter to remove all contents of a string before the last /

## 10.9.1 (2024-01-24)

**Fixed:**

* Offset & Count Calculation for Hydra Paging *

**Added:**

* HVD Category Vocabulary
* SkosVisitor for generic SKOS Properties

## 10.9.0 (2024-01-11)

**Added:**

* Filtered namespace prefixes also for class `Dataset`
* Convenient `Model` extensions for finding several DCAT-AP resource types

## 10.8.2 (2024-01-10)

**Add:**

* Missing fixes/commits from develop

## 10.8.1 (2024-01-10)

**Changed:**

* Exceptional response handling

**Added:**

* TSV as machine-readable file type
* Configurable transfer format for the triple store class

## 10.8.0 (2023-12-20)

**Fixed:**

* Better error-handling on invalid keycloak configuration

**Added:**

* Keycloak helper functions to create Groups, Resources or GroupPolicies if they do not exist
* Telemetry (tracing) (see `PiveauTelemetry`) based on OpenTelemetry
* A sub-classed vertx launcher `PiveauVertxLauncher`, especially to support telemetry auto initialization
* Dataset `count()` function in `DatasetManager`
* Hydra Collection object in `Hydra`

## 10.7.2 (2023-11-10)

**Fixed:**

* Triple store manager now exclude uriRefs that are not valid

## 10.7.1 (2023-11-09)

**Fixed:**

* `MockupTripleStore` request parsing

## 10.7.0 (2023-11-07)

**Added:**

* `Path`to `Dataset` extension functions

**Changed:**

* Delegated all `JenaUtils` read and write operations to the corresponding kotlin extension functions
* Exclude countries vocabularies from remote list
* Include corporate bodies to remote list

## 10.6.2 (2023-10-11)

**Fixed:**

* GML parsing

## 10.6.1 (2023-10-02)

**Added:**

* Piveau Properties for the Catalogue Page Feature (interesting datasets list)

**Changed:**

* Default anytime value to 180000 (zero seems not working properly)

## 10.6.0 (2023-07-17)

**Changed:**

* Increased max http header size of triple store web client
* Reduce extensive future and promise use
* Remove some use of `Void` in kotlin code

## 10.5.13 (2023-05-12)

**Changed:**

* Install and internal use of vocabularies aligned

**Added:**

* Piveau Properties for the Catalogue Page Feature

## 10.5.12 (2023-05-10)

**Added:**

* English label for 'Stadt Jena' in local corporate bodies vocabulary (intermediate hack)

## 10.5.11 (2023-05-10)

**Added:**

* 'Stadt Jena' to local corporate bodies vocabulary (intermediate hack)

## 10.5.10 (2023-05-08)

**Added:**

* DCAT-AP.de vocabularies as local copies

## 10.5.9 (2023-05-03)

**Fixed:**

* Filename for contributors vocabulary

## 10.5.8 (2023-05-02)

**Added:**

* DCAT-AP.de contributors vocabulary
* `Path` to model extension

## 10.5.7 (2023-04-19)

**Added:**

* notation-type skos vocabulary

## 10.5.6 (2023-04-17)

**Fixed**

* datasets Endpoint will now only return datasets that are not part of a hidden catalogue

**Changed:**

* `setGraph` and `getGraph` can now handle huge graphs

## 10.5.5 (2023-02-27)

**Changed:**

* Removed 'auth/' from keycloak path

## 10.5.4 (2023-02-21)

**Added:**

* `Model.presentAs` and `Dataset.presentAs`
* `Model.findTypedSubject(subjectType)`

**Changed:**

* Mark `Model.asString` and `Dataset.asString` as deprecated

## 10.5.3 (2023-02-15)

**Changed**

* Special characters are removed in vocable ids

## 10.5.2 (2023-02-15)

**Changed**

* Vocable ids are normalized

## 10.5.1 (2023-02-13)

**Added**

* Indexing of alt labels for vocabularies

**Fixed:**

* Use of `anytime` in triple store requests

## 10.5.0 (2023-02-12)

**Added**

* Dataset manager and catalogue manager can return list of full dataset metadata
* Equal and hash function for `DCATAPUriRef`
* GeoSPARQL format `geo:geoJSONLiteral`

## 10.4.0 (2023-02-01)

**Added:**

* Catalogue `dct:modified` property update when dataset is created or deleted
* `MockTripleStore` additional load model and graph methods
* Configurable `anytime` SPARQL query parameter
* SPARQL partial result and query timeout handling
* Dataset manager 'list distributions' implementation
* `TripleStoreException` integer code and query property

**Changed:**

* `TripleStore` and managers returning lists of `DCATAPUriRef`

## 10.3.0 (2022-12-29)

**Changed:**

* `TripleStore` API to use type alias `URIRef`
* `CatalogueManager`, `DatasetManager`, `MetricsManager` APIs to use type alias `URIRef`

**Added:**

* A `URIRef` type alias
* Convenient creation methods for `DCATAPUriRef`
* `planned-availability` vocabulary

## 10.2.1 (2022-11-20)

**Changed:**

* Use ResourceFactory for dcat-ap schema class, instead of default model

## 10.2.0 (2022-11-13)

**Added:**

* A name for triple store instances
* Handling list of API keys associated with resources

## 10.1.0 (2022-10-07)

**Changed:**

* Capitalized GeoName feature resource name
* `SpatialVisitor` returns now `Future<JsonObject>`
* `GeoNameFeature` refactoring
* Vocabularies remote default to false
* Vocabularies prefetch default to true

**Added:**

* Timeout when fetching geo names
* `GeoSpatialVisitor` parses now also `dcat:centroid` properties

## 10.0.1 (2022-09-15)

**Added**

* Add DEXT ("https://data.europa.eu/ns/ext#") namespace for prefixes

## 10.0.0 (2022-08-31)

**Added:**

* Add gzip function to compress files as complementary method to gunzip

**Changed:**

* multilingual ThemeVisitor
* Removed `method` parameter in `sendDigestAuth` and `sendBufferDigestAuth`

## 9.2.0 (2022-07-24)

**Fixed:**

* `catalogueManager.datasetsAsFlow` continuing despite of exceptions

**Added:**

* `isQuadFormat` rdf helper
* csv validation metrics

## 9.1.3 (2022-07-21)

**Added:**

* A convenient method `gunzip` to decompress gzip files (issue #2177)
* `catalogueManager.listUris()` returning a list of catalogue uris

**Changed:**

* `catalogueManager.list()` method returns now a list of models

## 9.0.1 (2022-07-19)

**Fixed:**

* Triple store handles 503 responses correctly

## 9.0.0 (2022-07-07)

**Fixed:**

* `DCATAPUriRef.resource` set empty when context is blank
* `parseGeoLiteral` catches parseDouble exceptions

**Changed:**

* IanaType vocabulary to be performant (caused out of memory issue)
* Ticket: https://gitlab.fokus.fraunhofer.de/piveau/organisation/piveau-scrum-board/-/issues/2057;
  SPDX NS changed from https://spdx.org/rdf/terms/# to http://spdx.org/rdf/terms#

## 8.10.0 (2022-06-27)

**Added:**

* RDF mime types completed

**Changed:**

* Fine-tuning of `DCATAPUriSchema` and `DCATAPUriRef`

**Fixed:**

* Accept header parsing in content negotiation, honor lists and weights

## 8.9.0 (2022-06-02)

**Added:**

* Add `fromLabel` to ConceptScheme base class

**Fixed:**

* Use `createFor` instead of `applyFor` to prevent normalization of catalogue ids

## 8.8.2 (2022-05-12)

**Changed:**

* Set JWT option ignoring expiration to false

## 8.8.1 (2022-04-26)

**Changed:**

* Use corporate bodies ac act version including homepage

## 8.8.0 (2022-04-25)

**Changed:**

* Updated all authority vocabularies

## 8.7.1 (2022-04-19)

**Fixed:**

* Check for existing `WWW-Authenticate` header

## 8.7.0 (2022-04-01)

**Added:**

* Prefixes of `osi`

## 8.7.0 (2022-04-01)

**Added:**

* Set namespace prefixes filtered utils function in Prefixes.kt
* Prefixes of `edp`, `dcatapde`, `euvoc`, `dqv`, `lemon`, `oa`, `at`, `shacl`, `sis` and `stat`

**Changed:**

* Prefix of vcard from `v` to `vcard`

## 8.6.1 (2022-03-09)

**Added:**

* Keycloak existsGroup method

## 8.6.0 (2022-02-14)

**Added:**

* Utility extension functions for http request digest auth, with and without buffer
* `DEXT` namespace and elements
* Issue #1922: Methods for adding and putting only no blank strings into JsonObject and JsonArray
* Ability to use digest auth even for read operations

**Changed**

* Issue #1922: Consequently add no blank strings in visitors
* Moved optimized http request extension functions for digest auth to `Piveau` utilities
* Use POST for sparql update queries (follow specification)

## 8.5.0 (2022-01-27)

**Added:**

* Method in `DCATAPUriSchema` for creating a `DCATAPUriRef` without normalization
* Flow methods for `CatalogueManager`

## 8.4.0 (2022-01-09)

**Changed:**

* EDP property names

**Added:**

* `KeycloakResourceHelper` for adding keycloak resources, groups and policies/permissions

## 8.3.1 (2021-12-14)

**Added:**

* `VocabularyHelper` for vocabulary resolution via triplestore

**Fixed**

* select: handle exception if status code is 200 but the data stream contains incomplete JSON data (Fuseki timeout)

## 8.3.0 (2021-12-02)

**Added:**

* `toModel` with custom `ErrorHandler`
* Identify dataset from distribution id
* `SparqlQueryPool` for externalize SPARQL queries
* Catalogue stripped only from records

**Fixed:**

* Use MIME Type `application/sparql-results+json` for ASK queries
* Fix mock triple store to honour the changed mime type for ASK queries
* `PiveauAuthConfig` default constructor `tokenServerConfig` initialising

**Changed:**

* `PreProcessing` throws exception

## 8.2.0 (2021-10-27)

**Added:**

* Helper for CLI completion

**Updated:**

* Vocabulary: corporate bodies
* Replacement mechanism for geo data types in triple store

## 8.1.0 (2021-10-18)

**Added:**

* `TripleStore.headGraph`
* `Countries.fromLabel`
* StatDCATAP vocabulary

## 8.0.1 (2021-10-08)

**Changed:**

* `TripleStore.setGraph` will not chunk the model anymore. Chunking needs to be done on a higher level.

## 8.0.0 (2021-10-07)

**Added:**

* DEFAULT_BASE_URI
* Missing vocabularies:
    * dataset-status
    * dataset-type
    * distribution-type
    * documentation-type
    * a stripped and extended version of eurovoc
    * ODRL.kt

**Changed:**

* Various RDF visitor, using label instead of title
* PrefLabel of vocabulary indexing is reduced to ISO 639-1 codes only

## 7.3.0 (2021-09-17)

**Added:**

* Filtering and removing geo statements

**Changed:**

* Normalize datetime will now normalize a date only to a date and not a datetime

## 7.2.1 (2021-09-01)

**Added:**

* Method to normalize unicode strings
* Extented EDP vocabulary with edp:visibility and edp:hidden

**Fixed:**

* Empty result when writing XML w/o declaration
* Parse errors in tests

## 7.2.0 (2021-07-05)

**Changed:**

* Piveau auth token server config
* Model extractor stops also at foaf:isPrimaryTopicOf
* Optionally clearing any geo data before send to triple store

## 7.1.0 (2021-06-23)

**Added:**

* `setGraph` uses optionally virtuoso pragma for speed up graph cleanup
* Configuration for enabling virtuoso pragmas

**Fixed:**

* `setGraph` for big models by wrote chunks sequentially

## 7.0.5 (2021-06-15)

**Fixed:**

* Stringify rdf extensions
* Update Jena libs with important bug fixes

## 7.0.4 (2021-06-11)

**Changed:**

* RDF read write in kotlin extensions

## 7.0.3 (2021-06-11)

**Changed:**

* Indexing missing multi-language fields with empty object

## 7.0.2 (2021-06-06)

**Fixed:**

* Triple store read model catch exceptions

## 7.0.1 (2021-06-05)

**Changed:**

* RDF read method

## 7.0.0 (2021-06-02)

**Added:**

* Flag to add XML declaration when writing RDF/XML

**Changed:**

* Licence assistant URLs in licence vocabulary
* Use `chunked` for partitioning triple list

**Fixed:**

* Get dct:identifier as lexical string
* Extract model boundary

**Removed:**

* Api key handler and credentials

## 6.3.2 (2021-05-03)

**Changed:**

* Consider prefLabel when identifying concept from literal
* Change writing of models to use `OutputStream` instead of `StringWriter`

## 6.3.1 (2021-04-30)

**Changed:**

* Uppercase literals when looking for concept ids

## 6.3.0 (2021-04-29)

**Fixed:**

* Test `isRDF` for RDF mime type

**Added:**

* Helper Class for creating and using a CORS Handler
* Visitor for indexing foaf:Document

## 6.2.0 (2021-03-03)

**Added:**

* Log output when sending asset failed
* ApiKey security classes: `ApiKeyCredentials`, `ApiKeyAuthProvider`, `ApiKeyAuthHandler`

## 6.1.0 (2021-02-19)

**Added:**

* New class `ConfigurableAssetHandler` for serving configurable assets like logos or favicons

**Changed:**

* Failure of token server creation throws exception

## 6.0.0 (2021-02-06)

**Changed:**

* Switched to Vert.x 4.0.0
* `TripleStore.deleteGraph` return logic
* Updated all dependencies

**Added:**

* `ADMS.Identifier` resource definition

**Fixed:**

* Pre-processing use buffered input stream for allowing reset

## 5.1.0 (2021-01-01)

**Added:**

* Implementation for `CatalogueManager::removeDatasetEntry`
* Methods for getting catalogue of a dataset `DatasetManager::catalogue`
* Security package `PiveauAuth` (JWT)
* `TripleStore::recursiveSelect` method
* Manager methods for recursive selects

**Changed:**

* Make constructor for `DCATAPUriRef` class internal
* Delete graph not found breaks circuit

## 5.0.0 (2020-11-18)

**Added:**

* Functions for indexing vocabularies
* Property `AT.table_id` for AT vocabulary
* Piveau log level checks
* Dimension scoring measurements

**Changed:**

* ConceptScheme constructor requires now the uriRef of the scheme

**Fixed:**

* Convert extracted format to upper case for better concept detection
* Frequency vocabulary initialization
* Typos in piveau dqv vocabulary

## 4.4.0 (2020-10-23)

**Added:**

* Define overloads for extension functions
* `SemVer` class
* SenIAS RDF object
* `EDP.MetricsLatest` and `EDP.MetricsHistory`

**Changed:**

* Use `UInt` in `SemVer`

## 4.3.1 (2020-09-21)

**Removed:**

* println of every triple parsed

**Changed:**

* CI JDK update to 14

## 4.3.0 (2020-09-19)

**Added:**

* `String.isRDF` extension property to check for RDF mime type
* preProcessing for N3 and JSON-LD
* preProcessing with Jena library

**Fixed:**

* preProcessing without base uri for jena parser
* Trim URIs during preProcessing

**Changed:**

* Use Jena preProcessing for JSON-LD
  **Changed**
* Added DCATAPDE Object and properties

## 4.2.2 (2020-09-09)

**Changed:**

* Jena lib for DCAT 2 extensions

**Added:**

* Historic Metrics in DCATAP schema

**Removed:**

* `DCAT2` helper class(obsolete due to Jena update)

**Fixed:**

* 500 response from triple store

## 4.2.0 (2020-07-13)

**Changed:**

* GeoNames reworked
* Use more lexical strings in visitors
* Blocking block dispatcher for loading vocabularies

**Added:**

* IANA namespace `http(s)://www.iana.org/assignments/media-types/`
* `JsonObject.asJsonObject(key)` and `JsonObject.asJsonArray(key)` extension functions
* ADMS vocabulary
* Frequency vocabulary

**Removed:**

* Normalization of ids in visitors

## 4.1.0 (2020-06-11)

**Added:**

* `TripleStore` constructor overloads
* DCAT-AP Visitors
* RDF geo parser
* Method for converting from mime type to `RDFFormat`

**Changed:**

* `Model` and `Dataset` write converts mime type to `RDFFormat`

## 4.0.1 (2020-05-30)

**Fixed:**

* Basic resource uri for IANA media types

## 4.0.0 (2020-05-27)

**Changed:**

* Complete refactoring of package structure
* IANA media type URIref is now as defined [here](https://www.w3.org/ns/iana/media-types/)

**Added:**

* Quality check job in gitlab ci
* Integrated vocabulary lib
* Method for retrieving TED language code
* Added Geonames class

**Fixed:**

* Typo in piveau license vocabulary extension
* `TripleStore.getGraph()` fails immediately when a 404 received

## 3.3.0 (2020-04-22)

**Changed:**

* Update vocabulary library for JSON_LD extensions

## 3.2.1 (2020-04-15)

**Fixed:**

* Pre-processing passes input to output stream when doing nothing

## 3.2.0 (2020-04-02)

**Fixed:**

* Language tag parsing
* CHeck for 404 responses from triple store

**Added:**

* Pre-processing with input streams and output streams
* Partition size configuration
* Read models from input stream

**Changed:**

* Removing annotation method

## 3.1.0 (2020-03-13)

**Fixed:**

* Circuit breaker conditions for retries

## 3.0.6 (2020-03-10)

**Changed:**

* Refactored put/post for setting graphs

## 3.0.5 (2020-03-10)

**Added:**

* Post graph
* Put metrics with slicing

**Fixed:**

* Check for empty model when get graph
* Check for 200 select result

## 3.0.4 (2020-03-08)

**Added:**

* Annotation methods for metrics API

## 3.0.3 (2020-03-06)

**Added:**

* More convenient metrics API

**Fixed:**

* Metrics add measurement method

**Changed:**

* Configurable circuit breaker timeout with default to no timeout
* Increased triple store web client pool size

## 3.0.2 (2020-03-04)

**Changed:**

* TripleStore breaker timeout increased

## 3.0.1 (2020-03-04)

**Fixed:**

* Metrics API: Create metrics graph returns named graph from dataset

## 3.0.0 (2020-02-28)

**Added:**

* Convenient classes `DCATGraph`, `Dataset` and `Distribution` to generate DCAT conform RDF
* Embedded TripleStore Mock for easier testing
* Method `externalIdentifier` in `DatasetManager`
* MockTripleStore provides ONE default config
* Convenient API to deploy and load data into mock triple store
* Convenient API `Metrics` to deal with metric datasets

**Removed:**

* Old hub config

**Changed:**

* Response handling in DigestAuth methods
* Use turtle for triple store communication
* Use circuit breaker when put a graph

## 2.3.0 (2020-01-24)

**Added:**

* Triple hash for statements
* `Piveau` static namespace
* `PiveauContext` for logging

**Changed:**

* `CatalogueManager` API
* `TripleStore` API

**Fixed:**

* `PreProcessing` now also encodes '"' and '\\'

## 2.2.0 (2019-12-10)

**Added:**

* RDF Helper for renaming resources containing self references
* First draft implementation for a triple store connector `TripleStore` with appropriate dcat-ap managers

**Fixed:**

* Pre-processor handling unsupported parser mime types types

## 2.1.0 (2019-11-17)

**Added:**

* Util function for parsing language tags (formerly part of indexing)
* PiveauContext
* rdf4j based pre-processing to fix not allowed characters in URIRefs

## 2.0.0 (2019-11-08)

**Added:**

* DCAT-AP context for metrics in `DCATAPUriSchema`

**Removed:**

* Vocabularies code (separated to piveau-vocabularies)
* Indexing code (separated to piveau-indexing)

## 1.1.2 (2019-10-18)

**Fixed:**

* Typo in DQV property `computedOn`

## 1.1.1 (2019-10-02)

**Added:**

* reading and writing rdf datasets
* piveau dqv vocabulary
* prov vocabulary
* `RDFMimeTypes`
* piveau ConceptScheme scoring
* DQV metric for scoring
* Array parser for `ConfigHelper`

**Removed:**

* URL decoding when normalizing id

**Changed:**

* Hash now implemented in class `TripleHash` and in Kotlin

**Fixed:**

* typo in RDF mime type

## 1.1.0 (2019-08-23)

**Added:**

* `DQV` and `SHACL` namespaces
* Code coverage (surefire, jacoco)

**Changed:**

* Requires now latest LTS Java 11

## 1.0.0 (2019-08-08)

**Added:**

* ConfigHelper helps reading json config values either as string or as json object
* Canonical hash function for jena models

**Changed:**

* Morph to kotlin

## 0.0.3 (2019-05-17)

**Added:**

* Model read method with baseUri parameter

## 0.0.2 (2019-05-11)

**Added:**

* findIdentifier with flexible configuration

**Changed:**

* findIdentifier algorithm refined

## 0.0.1 (2019-04-16)

Initial release
