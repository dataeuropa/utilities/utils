# piveau utils library
Providing convenient helper classes for the piveau context

## Table of Contents
1. [Build & Install](#build-&-install)
1. [Use](#use)
1. [License](#license)

## Build & Install
Requirements:
 * Git
 * Maven 3
 * Java 17

```bash
$ git clone <gitrepouri>
$ cd piveau-utils
$ mvn install
```

## Use

Add repository to your project pom file:
```xml
<repository>
    <id>paca</id>
    <name>paca</name>
    <url>https://paca.fokus.fraunhofer.de/repository/maven-public/</url>
</repository>
```

Add dependency to your project pom file:
```xml
<dependency>
    <groupId>io.piveau.utils</groupId>
    <artifactId>piveau-utils</artifactId>
    <version>10.8.1</version>
</dependency>
```

## License

[Apache License, Version 2.0](LICENSE.md)

## Important Notes

- This library uses Apache Jena, please refer to the Jena documentation for configuring your Maven config: https://jena.apache.org/documentation/notes/jena-repack.html



